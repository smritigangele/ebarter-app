//
//  AppDelegate.swift
//  Ebarter
//
//  Created by Hocrox Infotech Pvt Ltd1 on 21/03/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import UIKit
import CoreData
import Material
import GooglePlaces
import GoogleMaps
import IQKeyboardManagerSwift
import Stripe
import UserNotifications
import Fabric
import Crashlytics
import XCGLogger

let appDelegate = UIApplication.shared.delegate as! AppDelegate
let log: XCGLogger = {
    
    #if USE_NSLOG // Set via Build Settings, under Other Swift Flags
        // Setup XCGLogger
        let log = XCGLogger.default
        log.remove(destinationWithIdentifier: XCGLogger.Constants.baseConsoleDestinationIdentifier)
        log.add(destination: AppleSystemLogDestination(identifier: XCGLogger.Constants.systemLogDestinationIdentifier))
        log.logAppDetails()
        
    #elseif USE_OLD_SETUP_STYLE
        // This old style where we us a single setup method isn't recommended for production apps.
        // It's mostly useful for when you need a quick and dirty log, in a sample project, or unit test etc
        let log = XCGLogger.default
        let logPath: URL = appDelegate.cacheDirectory.appendingPathComponent("XCGLogger_Log.txt")
        log.setup(level: .debug, showThreadName: true, showLevel: true, showFileNames: true, showLineNumbers: true, writeToFile: logPath)
        
        // Add colour (using the ANSI format) to our file log, you can see the colour when `cat`ing or `tail`ing the file in Terminal on macOS
        // This is mostly useful when testing in the simulator, or if you have the app sending you log files remotely
        // This is also an example of how you can pull a log destination out of the logger later to alter it's setup
        if let fileDestination: FileDestination = log.destination(withIdentifier: XCGLogger.Constants.fileDestinationIdentifier) as? FileDestination {
            let ansiColorLogFormatter: ANSIColorLogFormatter = ANSIColorLogFormatter()
            ansiColorLogFormatter.colorize(level: .verbose, with: .colorIndex(number: 244), options: [.faint])
            ansiColorLogFormatter.colorize(level: .debug, with: .black)
            ansiColorLogFormatter.colorize(level: .info, with: .blue, options: [.underline])
            ansiColorLogFormatter.colorize(level: .warning, with: .red, options: [.faint])
            ansiColorLogFormatter.colorize(level: .error, with: .red, options: [.bold])
            ansiColorLogFormatter.colorize(level: .severe, with: .white, on: .red)
            fileDestination.formatters = [ansiColorLogFormatter]
        }
        
    #else
        // Setup XCGLogger (Advanced/Recommended Usage)
        // Create a logger object with no destinations
        let log = XCGLogger(identifier: "advancedLogger", includeDefaultDestinations: false)
        
        // Create a destination for the system console log (via NSLog)
        let systemDestination = AppleSystemLogDestination(identifier: "advancedLogger.appleSystemLogDestination")
        
        // Optionally set some configuration options
        systemDestination.outputLevel = .debug
        systemDestination.showLogIdentifier = false
        systemDestination.showFunctionName = true
        systemDestination.showThreadName = true
        systemDestination.showLevel = true
        systemDestination.showFileName = true
        systemDestination.showLineNumber = true
        
        // Add the destination to the logger
        log.add(destination: systemDestination)
        
        // Create a file log destination
        let logPath: URL = appDelegate.cacheDirectory.appendingPathComponent("XCGLogger_Log.txt")
//        let autoRotatingFileDestination = AutoRotatingFileDestination(writeToFile: logPath, identifier: "advancedLogger.fileDestination", shouldAppend: true,
//                                                                      attributes: [.pro: FileProtectionType.completeUntilFirstUserAuthentication], // Set file attributes on the log file
//            maxFileSize: 1024 * 5, // 5k, not a good size for production (default is 1 megabyte)
//            maxTimeInterval: 60) // 1 minute, also not good for production (default is 10 minutes)
//
//        // Optionally set some configuration options
//        autoRotatingFileDestination.outputLevel = .debug
//        autoRotatingFileDestination.showLogIdentifier = false
//        autoRotatingFileDestination.showFunctionName = true
//        autoRotatingFileDestination.showThreadName = true
//        autoRotatingFileDestination.showLevel = true
//        autoRotatingFileDestination.showFileName = true
//        autoRotatingFileDestination.showLineNumber = true
//        autoRotatingFileDestination.showDate = true
//        autoRotatingFileDestination.targetMaxLogFiles = 10 // probably good for this demo and production, (default is 10, max is 255)
        systemDestination.logQueue = XCGLogger.logQueue
        // Process this destination in the background
        //autoRotatingFileDestination.logQueue = XCGLogger.logQueue
        
        // Add colour (using the ANSI format) to our file log, you can see the colour when `cat`ing or `tail`ing the file in Terminal on macOS
        let ansiColorLogFormatter: ANSIColorLogFormatter = ANSIColorLogFormatter()
        ansiColorLogFormatter.colorize(level: .verbose, with: .colorIndex(number: 244), options: [.faint])
        ansiColorLogFormatter.colorize(level: .debug, with: .black)
        ansiColorLogFormatter.colorize(level: .info, with: .blue, options: [.underline])
        ansiColorLogFormatter.colorize(level: .warning, with: .red, options: [.faint])
        ansiColorLogFormatter.colorize(level: .error, with: .red, options: [.bold])
        ansiColorLogFormatter.colorize(level: .severe, with: .white, on: .red)
        systemDestination.formatters = [ansiColorLogFormatter]
        //autoRotatingFileDestination.formatters = [ansiColorLogFormatter]
        
        // Add the destination to the logger
        log.add(destination: systemDestination)
        
        // Add basic app info, version info etc, to the start of the logs
        log.logAppDetails()
        
    #endif
    
    // You can also change the labels for each log level, most useful for alternate languages, French, German etc, but Emoji's are more fun
    //    log.levelDescriptions[.verbose] = "🗯"
    //    log.levelDescriptions[.debug] = "🔹"
    //    log.levelDescriptions[.info] = "ℹ️"
    //    log.levelDescriptions[.warning] = "⚠️"
    //    log.levelDescriptions[.error] = "‼️"
    //    log.levelDescriptions[.severe] = "💣"
    
    // Alternatively, you can use emoji to highlight log levels (you probably just want to use one of these methods at a time).
    let emojiLogFormatter = PrePostFixLogFormatter()
    emojiLogFormatter.apply(prefix: "🗯🗯🗯 ", postfix: " 🗯🗯🗯", to: .verbose)
    emojiLogFormatter.apply(prefix: "🔹🔹🔹 ", postfix: " 🔹🔹🔹", to: .debug)
    emojiLogFormatter.apply(prefix: "ℹ️ℹ️ℹ️ ", postfix: " ℹ️ℹ️ℹ️", to: .info)
    emojiLogFormatter.apply(prefix: "⚠️⚠️⚠️ ", postfix: " ⚠️⚠️⚠️", to: .warning)
    emojiLogFormatter.apply(prefix: "‼️‼️‼️ ", postfix: " ‼️‼️‼️", to: .error)
    emojiLogFormatter.apply(prefix: "💣💣💣 ", postfix: " 💣💣💣", to: .severe)
    log.formatters = [emojiLogFormatter]
    
    return log
}()

// Create custom tags for your logs
//extension  {
//    static let sensitive = Tag("sensitive")
//    static let ui = Tag("ui")
//    static let data = Tag("data")
//}
//
//// Create custom developers for your logs
//extension Dev {
//    static let dave = Dev("dave")
//    static let sabby = Dev("sabby")
//}

var deviceTokenApp = "User Canceled to register"
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate {
    static let name = "name"
    static let user_image = "user_image"
    static let longitude = "longitude"
    static let latitude = "latitude"
    static let chatId = "chatId"
    static let messagingId = "messagingId"
    static let NegotiationType = "notifictionType"
//     static let NegotiationRejected = "NEG_R"
//     static let NegotiationStarted = "NEG_S"
//    static let chatStarted = "CHAT_M"
    var window: UIWindow?
    var deviceTokenString: String!
    var device_token: String? = nil
    var chatId: String!
    var appNavigationDrawerController: AppNavigationDrawerController!
    
    let documentsDirectory: URL = {
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.endIndex - 1]
    }()
    
    let cacheDirectory: URL = {
        let urls = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask)
        return urls[urls.endIndex - 1]
    }()
    
    // Life Cycle Methods:-
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        registerForRemoteNotification()
         IQKeyboardManager.sharedManager().enable = true
        application.applicationIconBadgeNumber = 0
        Fabric.with([Crashlytics.self])
        // TODO: Move this to where you establish a user session
        self.logUser()
       
//        let notificationSettings = UNNotificationSettings(types: [.badge,.alert,.sound], categories: nil)
//        UIApplication.shared.registerForRemoteNotifications(matching: notificationSettings)
//       // self.createLocalNotification()
//        application.registerForRemoteNotifications()
    
        Service.loadTokenOnAppLoad()
       
        GMSServices.provideAPIKey("AIzaSyAfqlUM919qU8xQcuCDwv09JgjFEEqsYbw")
        GMSPlacesClient.provideAPIKey("AIzaSyAfqlUM919qU8xQcuCDwv09JgjFEEqsYbw")
        //window = UIWindow(frame: Screen.bounds)
        
        STPPaymentConfiguration.shared().publishableKey = "pk_test_gfempIxKU4B0YP26ISjFs0LQ"
//        STPPaymentConfiguration.shared().smsAutofillDisabled = false
        
        manageView()
        
//        if let remoteNotificationInfo = launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] as? [AnyHashable : Any]{
//            
//                        let vc = {return UIStoryboard.viewController(identifier: "NegotiateViewController") as! NegotiateViewController}()
//                        self.window?.rootViewController = vc
//                        self.window?.makeKeyAndVisible()
//            self.application(application, didReceiveRemoteNotification: remoteNotificationInfo)
//        }

        GMSServices.provideAPIKey(Constants.googleMapsApiKey)
        GMSPlacesClient.provideAPIKey(Constants.googleMapsApiKey)
        return true
    }

    func logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        Crashlytics.sharedInstance().setUserEmail("user@fabric.io")
        Crashlytics.sharedInstance().setUserIdentifier("12345")
        Crashlytics.sharedInstance().setUserName("Test User")
    }
    
    
    func presentNextView(_ chatId: Int,_ data: String) -> Void {
        
    print("presenting next view")
    let alert = UIAlertController(title: "Alert", message: "Cannot connect to :", preferredStyle: UIAlertControllerStyle.alert)
    alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: nil))
    self.window?.rootViewController?.present(alert, animated: true, completion: nil)
    let vc = {return UIStoryboard.viewController(identifier: "NegotiateViewController") as! NegotiateViewController}()
        vc.bck = true
        vc.NEG_Type = data
        vc.ChatID = chatId
        vc.isFromNotification = true
    openNewWindow = UIWindow(frame: UIScreen.main.bounds)
    openNewWindow.backgroundColor = UIColor.red
    openNewWindow.windowLevel = UIWindowLevelAlert + 1
    openNewWindow.rootViewController = vc
    openNewWindow.makeKeyAndVisible()
    
        
    }

       func manageView() {
        let sideBarViewController = {
            return UIStoryboard.viewController(identifier: "SideBarViewController") as! SideBarViewController
        }()
      
        if (Service.userToken as String!) != nil {
            let viewController: UIViewController = UIViewController()
            //Service.getService(view: viewController)
            
//            static func getService(view: UIViewController){
                var userProfile: UserProfileDTO = UserProfileDTO()
                UserService.getCurrentUser(view: viewController , paginate: HomeScreenVM()){
                    (completion: UserProfileDTO) in
                    print(completion)
                    userProfile = completion
                    print(userProfile)
                    UserDefaults.standard.setValue(userProfile.name, forKey: AppDelegate.name)
                    UserDefaults.standard.synchronize()
                    UserDefaults.standard.setValue(userProfile.user_image, forKey: AppDelegate.user_image)
                    UserDefaults.standard.synchronize()
                    UserDefaults.standard.setValue(userProfile.user_longitude, forKey: AppDelegate.longitude)
                    UserDefaults.standard.synchronize()
                    UserDefaults.standard.setValue(userProfile.user_latitude, forKey: AppDelegate.latitude)
                    UserDefaults.standard.synchronize()
//                    (UIApplication.shared.delegate as! AppDelegate).manageView()
                    print("result printed for user profile")
            
            print("user image......\(UserDefaults.standard.string(forKey: AppDelegate.name)! as String)")
//            if (UserDefaults.standard.string(forKey: AppDelegate.user_image) as String!) != nil {
            if(userProfile.user_image != ""){
                print("user Profile ::: \(userProfile.user_image)")
                let homePageViewController: HomePageViewController = {
                    return UIStoryboard.viewController(identifier: "HomePageViewController") as! HomePageViewController
                }()
                let appToolbarController = AppToolbarController(rootViewController: homePageViewController)
                self.window!.rootViewController = AppNavigationDrawerController(rootViewController: appToolbarController, leftViewController: sideBarViewController)
            }
                else{
                
                let vc = { return UIStoryboard.viewController(identifier: "EditProfileViewController") as! EditProfileViewController
                }()
                //                    self.presentNextView("EditProfileViewController")
                self.window!.rootViewController = vc
              UIApplication.shared.statusBarStyle = .lightContent
               }
        }
        }
            else {
            let loginController: LoginSignUpViewController = {
                return UIStoryboard.viewController(identifier: "LoginSignUpViewController") as! LoginSignUpViewController
            }()
            let appToolbarController = AppToolbarController(rootViewController: loginController)
             let appNavigationDrawerController = AppNavigationDrawerController(rootViewController: appToolbarController, leftViewController: sideBarViewController)
              appNavigationDrawerController.isLeftViewEnabled = false
            window!.rootViewController = appNavigationDrawerController
                //AppNavigationDrawerController(rootViewController: appToolbarController, leftViewController: sideBarViewController)
          
          
            //window!.rootViewController = appNavigationDrawerController
        }
        
      window!.makeKeyAndVisible()
    UIApplication.shared.statusBarStyle = .lightContent
    }
    
    // For device Token : -
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        print("Device  messaging id ",deviceTokenString)
        
        forwardTokenToServer(deviceTokenString)
        deviceTokenApp = deviceTokenString
    }
    func forwardTokenToServer(_ token: String){
        
         UserDefaults.standard.setValue(token, forKey: "Token")
            print("Device Token  : \(token)")
        
        }
        
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("remote notification support is unavailable.....\(error.localizedDescription)")
       // print("simulator id ")
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "Ebarter")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    

    
    // MARK: Class Methods
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("User Info = ",notification.request.content.userInfo)
        //presentNextView(notification)
        

            completionHandler([.alert, .badge, .sound])
        
        
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        //print("User Info = ",response.notification.request.content.userInfo)
        print(response)
        print("notification tapped")
        print(response)
        let responseString = response.notification.request.content.userInfo["completeMessage"] as! String
        print(responseString)
        // let contents = str.split(separator: ":")
        let contents = responseString.components(separatedBy:":" )
        print(contents)
        let type =  contents[0]
        print(type)
        let chatID = contents[1]
        print(chatID)
        let title = contents[2]
        print(title)
      //  UserDefaults.standard.setValue(chatID, forKey: AppDelegate.chatId)
        
        switch type {
            
        case "NEG_A":
            print("accepted")
        case "NEG_R":
            print("rejected")
        case "NEG_S":
            print("negotiation started")
          case "CHAT_M":
            print("chat initiated")
    UserDefaults.standard.setValue(chatID, forKey: AppDelegate.messagingId)
            UserDefaults.standard.synchronize()
            
        default:
            print("no such type is there")
        }
         presentNextView(Int(chatID)!,type)
    }
    
    func registerForRemoteNotification() {
        if #available(iOS 10.0, *) {
            let center  = UNUserNotificationCenter.current()
            center.delegate = self
            center.requestAuthorization(options: [.sound, .alert, .badge]) { (granted, error) in
                if error == nil{
                    UIApplication.shared.registerForRemoteNotifications()
                }
            }
        }
        else {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.sound, .alert, .badge], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
    }


}

