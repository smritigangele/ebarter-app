import UIKit
import Material

class AppToolbarController: ToolbarController {
    static let blue = UIColor(red: 33/255,green: 119/255, blue: 232/255, alpha: 1)
    fileprivate var menuButton: IconButton!
    fileprivate var switchControl: Switch!
    fileprivate var moreButton: IconButton!
    public var backButton: IconButton!
    public var notificationButton: IconButton!
    
    override func prepare() {
        super.prepare()
        prepareMenuButton()
        prepareStatusBar()
        prepareToolbar()
    }
}

extension AppToolbarController {
    public func prepareMenuButton() {
        menuButton = IconButton(image: Icon.cm.menu, tintColor: .white)
        menuButton.addTarget(self, action: #selector(handleMenuButton), for: .touchUpInside)
         backButton = IconButton(image: Icon.cm.arrowBack, tintColor: .white)
         notificationButton = IconButton(image: Icon.cm.bell, tintColor: .white)
    }

    fileprivate func prepareStatusBar() {
        //statusBar.backgroundColor = Color.appRed
        statusBar.backgroundColor = AppToolbarController.blue
    }
    public func itemsOnToolbar(_ sender: Bool){
        if (sender == false){
            toolbar.backgroundColor = AppToolbarController.blue
            toolbar.titleLabel.textColor = Color.white
            toolbar.leftViews = [backButton]
        }
        else{
            toolbar.leftViews = []
        }
        
        //backButton.addTarget(self,action: #selector(handleBackButton), for: .touchUpInside)
    }
    
    fileprivate func prepareToolbar() {
       // toolbar.backgroundColor = Color.appRed
         toolbar.backgroundColor = AppToolbarController.blue
         toolbar.titleLabel.textColor = Color.white
        
//         toolbar.leftViews = [menuButton]
        //toolbar.leftViews = [backButton]
         }
    
    func toggleMenus(left: Bool, right: Bool) {
        setLeftMenuItem(left)
        setRightMenuItem(right)
    }
    
    func setLeftMenuItem(_ show: Bool) {
        if show == true {
            toolbar.leftViews = [menuButton]
        } else {
            toolbar.leftViews = []
        }
    }
    
    
    func setRightMenuItem(_ show: Bool) {
        if show {
            toolbar.rightViews = [notificationButton,backButton]
            
        } else {
            toolbar.rightViews = []
        }
    }
    
}

extension AppToolbarController {
    @objc
    fileprivate func handleMenuButton() {
        navigationDrawerController?.toggleLeftView()
    }
}
