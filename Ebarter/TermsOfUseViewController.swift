//
//  TermsOfUseViewController.swift
//  Ebarterr
//
//  Created by Hocrox Infotech Pvt Ltd1 on 23/06/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import UIKit

class TermsOfUseViewController: UIViewController {

    
    
    @IBOutlet weak var webViewForTerms: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
      webViewForTerms.loadRequest(NSURLRequest(url: NSURL(string: "http://ebarternow.com/toc.html")! as URL) as URLRequest)
        
    }
    
    @IBAction func clickedOnBack(_ sender: Any) {
        
        
        dismiss(animated: true, completion: nil)
    }
    
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
