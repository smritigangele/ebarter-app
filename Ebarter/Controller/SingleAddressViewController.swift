//
//  SingleAddressViewController.swift
//  Ebarter
//
//  Created by Hocrox Infotech Pvt Ltd1 on 01/04/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import UIKit

class SingleAddressViewController: UIViewController {
    
    @IBOutlet weak var product_image: UIImageView!
    
    @IBOutlet weak var product_name: UILabel!
    
    @IBOutlet weak var product_points: UILabel!
    
    @IBOutlet weak var category_name: UILabel!
    
    @IBOutlet weak var user_name: UILabel!
    
    @IBOutlet weak var user_mobile: UILabel!
    
    @IBOutlet weak var user_address: UILabel!
    var product_id: [String] = []
    var address_id: String!
    var addressDTO: AddressDTO = AddressDTO()
    var productDTO: ProductDTO = ProductDTO()
    var chatList: ChatMessageListVM = ChatMessageListVM()
    var buyViaNegotiation: Buy_Via_NegotiationVM = Buy_Via_NegotiationVM()
    var buyViaNegotiationDTO: Buy_Via_NegotiationDTO = Buy_Via_NegotiationDTO()
    var buyWithoutNegotiation: Buy_Without_NegotiationVM = Buy_Without_NegotiationVM()
    var buyWithoutNegotiationDTO: Buy_Without_NegotiationDTO = Buy_Without_NegotiationDTO()
    var chatId: Int!
    var isFromNegotiation: Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
                get_URL()

    }
    func get_URL(){
        
        UserService.getAddressById(view: self, id:address_id!,paginate: AddressVM()){
            (completion: AddressDTO) in
            
            print("completion:::: + \(completion)")
            self.addressDTO = completion
            self.product_name.text! = self.productDTO.name
            self.product_points.text! = self.productDTO.points
            self.category_name.text! = self.productDTO.categoryName
            let url = NSURL(string: self.productDTO.sellerImage)
            
            let request1: NSURLRequest = NSURLRequest(url: url as! URL)
            let mainQueue1 = OperationQueue.main
            NSURLConnection.sendAsynchronousRequest(request1 as URLRequest, queue: mainQueue1, completionHandler: { (response, data, error) -> Void in
                if error == nil {
                    DispatchQueue.main.async(execute: {
                        self.product_image.image = UIImage(data: data! as Data)
                    })
                }
                else{
                    self.product_image.image=UIImage(cgImage: #imageLiteral(resourceName: "placeholderr") as! CGImage)
                }
            })
//            let url = NSURL(string: self.productDTO.image)
//            
//            let request1: NSURLRequest = NSURLRequest(url: url as! URL)
//            let mainQueue1 = OperationQueue.main
//            NSURLConnection.sendAsynchronousRequest(request1 as URLRequest, queue: mainQueue1, completionHandler: { (response, data, error) -> Void in
//                if error == nil {
//                    DispatchQueue.main.async(execute: {
//                        self..image = UIImage(data: data! as Data)
//                    })
//                }
//                else{
//                    self.product_image.image=UIImage(cgImage: #imageLiteral(resourceName: "placeholderr") as! CGImage)
//                }
//            })

            self.user_name.text! = self.addressDTO.address_name
            self.user_address.text! = self.addressDTO.address_permanent_address
            self.user_mobile.text! = self.addressDTO.address_mobile
        }

    }
    
    
    @IBAction func BckBtn(_ sender: Any) {
      dismiss(animated: true, completion: nil)
    }
  
    
    @IBAction func Place_Order_Btn(_ sender: Any) {
        
        if isFromNegotiation {
            buyViaNegotiation.chat_id = chatList.chat_id
            buyViaNegotiation.address_id = addressDTO.address_id
            
            AccountService.buy_Via_Negotiation(view: self, address: buyViaNegotiation){
                (completion: Buy_Via_NegotiationDTO) in
                print(completion)
                self.buyViaNegotiationDTO = completion
                print(self.buyViaNegotiationDTO)
                self.createAlert(title: "success", body: "your order has been placed")
            }
        }
    
        else{
    
     print(self.productDTO.product_id)
     buyWithoutNegotiation.product_id = self.productDTO.product_id
     buyWithoutNegotiation.address_id = self.addressDTO.address_id
     AccountService.buy_Without_Negotiation(view: self, address: buyWithoutNegotiation){
    (completion: Buy_Without_NegotiationDTO) in
     self.buyWithoutNegotiationDTO = completion
     self.createAlert(title: "success", body: "your order has been placed")
        
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
