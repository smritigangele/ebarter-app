//
//  AddAddressViewController.swift
//  
//
//  Created by Hocrox Infotech Pvt Ltd1 on 23/03/17.
//
//

import UIKit
import GoogleMaps
import GooglePlaces

class AddAddressViewController: UIViewController {

    @IBOutlet weak var nameField: UITextField!
    
    @IBOutlet weak var nameOfCity: UITextField!
    
    @IBOutlet weak var nameOfState: UITextField!
    
    @IBOutlet weak var nameOfCountry: UITextField!
    
    @IBOutlet weak var postalCodeField: UITextField!
    
    @IBOutlet weak var mobile_No: UITextField!
    
    @IBOutlet weak var permanent_Address: UITextField!
    
    let address : AddressVM = AddressVM()
    var selectedString: [String] = [String]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
      mobile_No.keyboardType = UIKeyboardType.numberPad
      postalCodeField.keyboardType = UIKeyboardType.numberPad
        
    }

    @IBAction func bckBtn(_ sender: Any) {
        dismiss(animated: true, completion: nil)

        }
    
    @IBAction func actionOnCityField(_ sender: Any, forEvent event: UIEvent) {
        
    let autoCompleteController = GMSAutocompleteViewController()
    autoCompleteController.delegate = self
    self.present(autoCompleteController, animated: true, completion: nil)
        
    }
    @IBAction func saveAddressBtn(_ sender: Any) {
        
        address.name = nameField.text!
        address.mobile = mobile_No.text!
        address.city = nameOfCity.text!
        address.state = nameOfState.text!
        address.country = nameOfCountry.text!
        address.postalcode = postalCodeField.text!
        address.permanentAddress = permanent_Address.text!
        AccountService.addAddress(view: self, address : address) {
            (completion: AddressDTO) in
            print(completion)
        self.createAlert(title: "Success", body: "Address Added successfully")
       
    }
         self.dismiss(animated: true, completion: nil)
       
}
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
    }
}
extension AddAddressViewController: GMSAutocompleteViewControllerDelegate {
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        self.address.address = place.formattedAddress!
        self.address.latitude = place.coordinate.latitude
        self.address.longitude = place.coordinate.longitude
        self.address.placeId = place.placeID
        self.nameOfCity.text = place.formattedAddress!
        
        for address: GMSAddressComponent in (place.addressComponents)! {
            if(address.type == "administrative_area_level_2") {
                self.address.city = address.name
                self.nameOfCity.text = address.name
            }
            if(address.type == "administrative_area_level_1") {
                self.address.state = address.name
                self.nameOfState.text = address.name
            }
            if(address.type == "country") {
                self.address.country = address.name
                self.nameOfCountry.text = address.name
            }
            if(address.type == "postal_code") {
                self.postalCodeField.text = address.name
                self.address.postalcode = address.name
            }
            print(address.type + " - " + address.name)
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}

