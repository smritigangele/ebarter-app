//
//  FAQViewController.swift
//  Ebarterr
//
//  Created by Hocrox Infotech Pvt Ltd1 on 02/05/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import UIKit

class FAQViewController: UIViewController {

    
    @IBOutlet weak var webView: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
    
        webView.loadRequest(NSURLRequest(url: NSURL(string: "http://ebarternow.com/faq.html")! as URL) as URLRequest)
        
    }

    
    @IBAction func back_Btn(_ sender: Any) {
       (UIApplication.shared.delegate as! AppDelegate).manageView()
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
