//
//  BuyerViewController.swift
//  Ebarter
//
//  Created by Hocrox Infotech Pvt Ltd1 on 30/03/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import UIKit
import Material

class BuyerViewController: UIViewController {
    
        static let blue = UIColor(red: 33/255,green: 119/255, blue: 232/255, alpha: 1)
        var buyerPaginateDTO: BuyerPaginateDTO = BuyerPaginateDTO()
    @IBOutlet weak var tableViewForBuyer: TableView!
    var type:String = "BUYER"
    
        required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
            preparePageTabBarItem()
            
        }
        
        init() {
            super.init(nibName: nil, bundle: nil)
            preparePageTabBarItem()
        }
        
        override func viewDidLoad() {
            super.viewDidLoad()
            self.hideKeyboardWhenTappedAround()
            preparePageTabBarItem()
            
            tableViewForBuyer.delegate = self
            tableViewForBuyer.dataSource = self
                self.view.layoutIfNeeded()
            self.tableViewForBuyer.setNeedsLayout()
            self.tableViewForBuyer.layoutIfNeeded()
//            self.tableViewForBuyer.estimatedRowHeight = 100.0;
//            self.tableViewForBuyer.rowHeight = UITableViewAutomaticDimension
            self.tableViewForBuyer.setNeedsLayout()
            self.tableViewForBuyer.layoutIfNeeded()
    }
    
    func get_URL(){
            Service.loadTokenOnAppLoad()
            UserService.getChatListOfBuyerSeller(view: self, type: type, paginate: HomeScreenVM()){
            (completion: BuyerPaginateDTO) in
            print(completion)
            self.buyerPaginateDTO = completion
            self.tableViewForBuyer.reloadData()
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        get_URL()
    }
        private func preparePageTabBarItem() {
            pageTabBarItem.title = "As a Buyer"//"Buyer"
            pageTabBarItem.titleColor = UIColor.white
           
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
extension BuyerViewController: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return buyerPaginateDTO.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableViewForBuyer.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! BuyerTableViewCell
        cell.styling()
        cell.updateView(text: self.buyerPaginateDTO.data[indexPath.row])
        cell.actionBtn.addTarget(self, action: #selector(handlecells(_:)), for: .touchUpInside)
        return cell
    }
    func handlecells(_ sender:UIButton){
        print("something selected.......")
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "NegotiateViewController") as! NegotiateViewController
        vc.products = buyerPaginateDTO.data[(tableViewForBuyer.indexPath(for:(sender.superview?.superview?.superview) as! BuyerTableViewCell)?.row)!].product
        
        vc.isFromBuyer = true
        vc.sendMsgFromBuyer = true
        vc.buyerDTO = buyerPaginateDTO.data[(tableViewForBuyer.indexPath(for:(sender.superview?.superview?.superview) as! BuyerTableViewCell)?.row)!]
//        vc.buyer_name = buyerPaginateDTO.data[(tableViewForBuyer.indexPath(for:(sender.superview?.superview) as! BuyerTableViewCell)?.row)!].buyerName
        present(vc, animated: true, completion: nil)
        
    }

    }


