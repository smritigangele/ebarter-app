//
//  AddProductViewController.swift
//  Ebarter
//
//  Created by Hocrox Infotech Pvt Ltd1 on 27/03/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import UIKit
import DropDown
import Alamofire
import XCGLogger
import Kingfisher

class AddProductViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    let imagePicker = UIImagePickerController()
    
    @IBOutlet weak var viewForImage: UIView!
    
    @IBOutlet weak var addProductImage: UIImageView!
    @IBOutlet weak var selectedCategory: UIButton!
    
    @IBOutlet weak var product_name: UITextField!
    
    @IBOutlet weak var points_Lbl: UITextField!
    
    @IBOutlet weak var Qty_Lbl: UITextField!

    @IBOutlet weak var description_Lbl: UITextView!
    
     var array2 = [String]()
    var addNewProductDTO: AddNewProductDTO = AddNewProductDTO()
    var homeScreenPaginateDTO: HomeScreenPaginateDTO = HomeScreenPaginateDTO()
    static let product_name = "product_name"
    static let category_name = "category_name"
    static let points = "points"
    static let quantity = "Quantity"
    static let time = "time"
    static let image = "image"
    static let newId = "id"
    static let addNewProduct = "addNewProduct"
    var imageUrl: String!
    var thumbnailUrl: String! = nil
    var isFrmNotification = false
    var editProduct = EditProductDTO()
    let newView = UIView()
    let tableView = UITableView()
    let reuseIdentifier = "cell"
    let dropdown = DropDown()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDropDown()
        imagePicker.delegate = self
        let tapImage = UITapGestureRecognizer(target: self, action: #selector(handleImage(_:)))
        addProductImage.addGestureRecognizer(tapImage)
        addProductImage.isUserInteractionEnabled = true
        points_Lbl.delegate = self
        points_Lbl.keyboardType = UIKeyboardType.numberPad
        Qty_Lbl.delegate = self
        Qty_Lbl.keyboardType = UIKeyboardType.numberPad
        
        if isFrmNotification == true{
            
        let url = URL(string: self.editProduct.thumbnail)
         self.addProductImage.kf.setImage(with: url)
         self.product_name.text! = self.editProduct.product_name
         self.selectedCategory.setTitle(self.editProduct.category_name, for: .normal)
         self.points_Lbl.text! = "\(self.editProduct.product_points)"
        self.Qty_Lbl.text! = "\(self.editProduct.product_quantity)"
        self.description_Lbl.text! = self.editProduct.product_description
        }
       // addProductImage.addTarget(self, action: #selector(handleImage), for: .touchUpInside)
        
    }
    func handleImage(_ gesture:UITapGestureRecognizer){
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)

//        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
//            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
//            present(imagePicker, animated: true, completion: nil)
//        }
        
    }
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            //            picker!.allowsEditing = false
            //            picker!.sourceType = UIImagePickerControllerSourceType.Camera
            //            picker!.cameraCaptureMode = .Photo
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = false
            imagePicker.cameraCaptureMode = .photo
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController,didFinishPickingMediaWithInfo info: [String: Any]){
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage{
            addProductImage.contentMode = .scaleToFill
            addProductImage.image = pickedImage
            print("picked image....\(addProductImage.image)")
            dismiss(animated:true, completion: nil)
            
            //UserDefaults.standard.setValue(pickedImage, forKey: AddProductViewController.image)
            //UserDefaults.standard.synchronize()
        }
        
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
   
    @IBAction func selectCategoryBtn(_ sender: Any) {
           dropdown.show()
        }
    
    func setupDropDown() {
        dropdown.anchorView = selectedCategory
        UserService.getAllImages(view: self, paginate: HomeScreenVM()){
            (completion: HomeScreenPaginateDTO)in
            self.homeScreenPaginateDTO = completion
            //self.homeScreenPaginateDTO.data
          //  array.append(contentsOf: self.homeScreenPaginateDTO.data)
            var arry = [String]()
            for categoryName in self.homeScreenPaginateDTO.data{
                        arry.append(categoryName.category_name)
             }
            self.dropdown.dataSource = arry
            //self.dropdown.dataSource = [self.homeScreenDTO.id]
           // ["selectCategory","Electronics","Mobiles","Bikes","Furniture"]
           self.dropdown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.selectedCategory.setTitle(item, for: .normal)
            //self.selectedCategory.titleLabel?.text
            let variable = self.homeScreenPaginateDTO.data.filter({$0.category_name == item}).first!.id
            UserDefaults.standard.setValue(variable, forKey: AddProductViewController.newId)
            print("no.of ids......\(variable)")
        }
      }
    }
    @IBAction func bckBtn(_ sender: Any) {
        (UIApplication.shared.delegate as! AppDelegate).manageView()
        //dismiss(animated: true, completion: nil)
    }
    
    func uploadImage(_ imageName: String){
        var logLevel: XCGLogger.Level = .verbose
           logLevel = .error
           log.outputLevel = logLevel
        print("there is some error:- \(log.outputLevel)")
        
        let addProduct: AddProductVM = AddProductVM()
        
                addProduct.product_image = imageName
                print("here is product image....\(addProduct.product_image)")
               addProduct.category_id = UserDefaults.standard.integer(forKey: AddProductViewController.newId) as Int
                print("here is the category_id....\(addProduct.category_id)")
                addProduct.title = self.product_name.text!
                print("title is this...\(addProduct.title)")
                addProduct.product_points = self.points_Lbl.text!
                addProduct.product_quantity = "\(self.Qty_Lbl.text!)"
                addProduct.body = self.description_Lbl.text!
                print("body......\(self.description_Lbl.text!)")
    
        UserService.getCurrentUser(view: self, paginate: HomeScreenVM()){
            (completion: UserProfileDTO) in
            print(completion)
            addProduct.author_id = completion.id//UserDefaults.standard.integer(forKey: EditProfileViewController.id) as Int
            addProduct.product_latitude = completion.user_latitude
            addProduct.product_longitude = completion.user_longitude
            addProduct.product_state = completion.user_state
                addProduct.product_city = completion.user_city
            addProduct.product_country = completion.user_country
//          addProduct.product_latitude = UserDefaults.standard.double(forKey: EditProfileViewController.latitude1) as Double
//
//          addProduct.product_longitude = UserDefaults.standard.double(forKey: EditProfileViewController.longitude1) as Double
                AccountService.addNewProduct(view: self, address: addProduct){
                    (completion: AddNewProductDTO) in
                    print(completion)
                    self.addNewProductDTO = completion
                    print(self.addNewProductDTO)
                    log.verbose()
                    log.debug()
                    log.debug {
                        // add expensive code required only for logging, then return an optional String
                        return "Executed debug code block" // or nil
                    }
                    log.error()
                    log.error {
                        // add expensive code required only for logging, then return an optional String
                        return "Executed error code block" // or nil
                    }
                    log.warning()
                    log.warning {
                        // add expensive code required only for logging, then return an optional String
                        return "Executed warning code block" // or nil
                    }
                   print("after added product city field is.... \(self.addNewProductDTO.category_name)")
                    UserDefaults.standard.setValue(self.addNewProductDTO.category_name, forKey: AddProductViewController.category_name)
                    UserDefaults.standard.setValue(self.addNewProductDTO.product_name, forKey: AddProductViewController.product_name)
                    UserDefaults.standard.setValue(self.addNewProductDTO.product_quantity, forKey: AddProductViewController.quantity)
                    UserDefaults.standard.setValue(self.addNewProductDTO.product_points, forKey: AddProductViewController.points)
                    UserDefaults.standard.setValue(self.addNewProductDTO.image, forKey: AddProductViewController.image)
                    UserDefaults.standard.synchronize()
                   self.createAlert(title: "Product added", body: "successfully")
                    (UIApplication.shared.delegate as! AppDelegate).manageView()
                    
                     }
                }
           }
    
    @IBAction func post_Product(_ sender: Any) {
        Loader.startLoading()
    
        let imgData = UIImageJPEGRepresentation(addProductImage.image!, 0.2)!
        
        let parameters = ["image": "file.jpeg"]
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(imgData, withName: "image",fileName: "file.jpg", mimeType: "image/jpg")
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
        },
                         to: Constants.baseURL + "image/upload", method: .post, headers: Service.authHeader , encodingCompletion: {
                            encodingResult in
                            switch encodingResult {
                                
                            case .success(let upload, _, _):
                                print("dxfcgvhbj")
                                upload.responseJSON { response in
                                    print(response.request ?? "original url")  // original URL request
                                    print(response.response ?? "url response ") // URL response
                                    print(response.data ?? "server data")     // server data
                                    print("uploaded successfully\(response.result)")   // result of response serialization
                                    Loader.stopLoading()
                                    if String(describing: response.result) == "SUCCESS" {
                                        print("this is token")
                                        
                                        if let JSON = response.result.value {
                                            
                                            print("JSON: \(JSON)")
                                            let response = JSON as! NSDictionary
                                            if ((response["image_name"] as? String) != nil) {
                                                
                                                self.imageUrl = response["image_name"] as! String
                                                print("image url.....\((self.imageUrl)! as String)")
                                                self.uploadImage(self.imageUrl)
                                                
                                            }else{
                                                if (response["description"] as? String) == nil {
                                                    self.createAlert(title: "Alert", body: ((response["fieldErrors"] as! NSArray)[0] as! NSDictionary)["message"] as! String)
                                                }else{
                                                    self.createAlert(title: "Alert", body: response["description"] as! String)
                                                }
                                            }
                                        }
                                    }
                                    else{
                                        
                                        self.createAlert(title: "Alert", body: (response.error?.localizedDescription)!)
                                    }
                                }
                                
                            case .failure(let encodingError):
                                print(encodingError)
                }
         })
    }
    
}
extension AddProductViewController: UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let invalidCharacters = CharacterSet(charactersIn: "0123456789").inverted
        return string.rangeOfCharacter(from: invalidCharacters, options: [], range: string.startIndex ..< string.endIndex) == nil
    }
    
}

