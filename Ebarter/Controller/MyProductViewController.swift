//
//  MyProductViewController.swift
//  Ebarterr
//
//  Created by Hocrox Infotech Pvt Ltd1 on 23/04/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import UIKit

class MyProductViewController: UIViewController {
    
    
    @IBOutlet weak var tableView: UITableView!

//    let addnewProduct = UserDefaults.standard.array(forKey: AddProductViewController.addNewProduct) as Array!
    
    var deleteProduct: DeleteProductResult = DeleteProductResult()
    var editProduct: EditProductDTO = EditProductDTO()
    var getAllProductPaginateDTO: GetAllProductsPaginateDTO = GetAllProductsPaginateDTO()
        var parentVC: UIViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       tableView.delegate = self
        tableView.dataSource = self
        self.tableView.setNeedsLayout()
        self.tableView.layoutIfNeeded()
        get_Url()
    }
    
    func get_Url()
    {
        UserService.getAllProducts(view: self, paginate: HomeScreenVM()){
            (completion: GetAllProductsPaginateDTO) in
            self.getAllProductPaginateDTO = completion
            print(self.getAllProductPaginateDTO)
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }

        }
    }

    
    @IBAction func bck_Btn(_ sender: Any) {
        self.dismiss(animated: true, completion: {
            self.parentVC.dismiss(animated: false, completion: {})
        })
        //(UIApplication.shared.delegate as! AppDelegate).manageView()
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
extension MyProductViewController: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getAllProductPaginateDTO.data.count
        //return (UserDefaults.standard.array(forKey: AddProductViewController.addNewProduct)! as Array).count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: MyProductViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MyProductViewCell
        
       // cell.styling()
        cell.updateView(data: self.getAllProductPaginateDTO.data[indexPath.row])
        cell.edit_Btn.addTarget(self, action: #selector(editBtn(_:)), for: .touchUpInside)
   // cell.delete_Btn.addTarget(self, action: #selector(deleteBtn), for: .touchUpInside)
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableViewAutomaticDimension
//    }
//
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableViewAutomaticDimension
//    }
    
    func editBtn(_ sender: UIButton){
        //"\(UserDefaults.standard.integer(forKey: EditProfileViewController.id) as Int)
        let productId = self.getAllProductPaginateDTO.data[(tableView.indexPath(for:(sender.superview?.superview?.superview) as! MyProductViewCell)?.row)!].product_id
        
        UserService.editProduct(view: self, id: "\(productId)"){
            (completion: EditProductDTO) in
            print(completion)
            self.editProduct = completion
            print(self.editProduct)
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddProductViewController") as! AddProductViewController
             vc.isFrmNotification = true
             vc.editProduct = self.editProduct
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    func deleteBtn(){
        UserService.deleteProduct(view: self, id: "\(UserDefaults.standard.integer(forKey: EditProfileViewController.id) as Int)"){ (completion: DeleteProductResult) in
            print("deleted product...\(completion)")
            self.deleteProduct = completion
            print(self.deleteProduct)
        }
        
    }
    
    
}

