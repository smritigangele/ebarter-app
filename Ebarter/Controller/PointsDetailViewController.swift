//
//  PointsDetailViewController.swift
//  Ebarterr
//
//  Created by Hocrox Infotech Pvt Ltd1 on 14/04/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import UIKit
import Kingfisher

class PointsDetailViewController: UIViewController {

    @IBOutlet weak var buyer_image: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var product_name: UILabel!
    @IBOutlet weak var username_Lbl: UILabel!
    @IBOutlet weak var sellerImage_Lbl: UIImageView!
    @IBOutlet weak var imageOfProduct: UIImageView!
    @IBOutlet weak var descriptionOfProduct: UILabel!
    @IBOutlet weak var transactionId: UILabel!
    @IBOutlet weak var points_Lbl: UILabel!
    @IBOutlet weak var buyer_username: UILabel!
    @IBOutlet weak var buyer_name_Lbl: UILabel!
    @IBOutlet weak var transactionDate: UILabel!
    @IBOutlet weak var transactionPoints: UILabel!
    var pointsTransactionId:Int!
    //PointTransactionDTO = PointTransactionDTO()
    
  var transaction_id : String = ""
    var pointTransactionDTO: PointTransactionDTO = PointTransactionDTO()
   var pointsPaginateResults: ProductDetailsDTO = ProductDetailsDTO()
    var sellerDTO: SellerDetailsDTO = SellerDetailsDTO()
    var buyerDTO: SellerDetailsDTO = SellerDetailsDTO()
    var dateTransacDTO: DateTransactionDTO = DateTransactionDTO()
//    var pointsTransacId = UserDefaults.standard.string(forKey: AddPointViewController.pointsTransactionId) as String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
     get_URL()
       
    }
    
    @IBAction func back_Btn(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func get_URL(){
     // UserDefaults.standard.string(forKey: AddPointViewController.pointsTransactionId)! as String
//       let demo =  UserDefaults.standard.object(forKey:AddPointViewController.pointsTransactionId ) as! Int
        UserService.getSingleTransaction(view: self, id: "\(pointsTransactionId!)" , paginate: HomeScreenVM()){
            (completion: PointTransactionDTO) in
            print(completion)
            self.pointTransactionDTO = completion
            
            let url1 = URL(string: self.pointsPaginateResults.image)
//
//             self.imageOfProduct.kf.setImage(with: url1)
           
            
            //self.imageOfProduct.image?.resizingMode
            let request1: NSURLRequest = NSURLRequest(url: url1 as! URL)
            let mainQueue1 = OperationQueue.main
            NSURLConnection.sendAsynchronousRequest(request1 as URLRequest, queue: mainQueue1, completionHandler: { (response, data, error) -> Void in
                if error == nil {
                    DispatchQueue.main.async(execute: {
                        self.imageOfProduct.image = UIImage(data: data! as Data)!
                        let image = UIImage(data: data! as Data)!
                        self.imageOrientation(_:image)
                        //self.fixOrientation(img: self.imageOfProduct.image!)
                            //self.imageWithImage(image: UIImage(data: data! as Data)!, scaledToSize: CGSize(width: self.view.frame.width, height: 128))
                       // self.fixOrientation(img: self.imageOfProduct.image!)
                        //UIImage(data: data! as Data)
                    })
                }
            })
            let url2 = NSURL(string: self.sellerDTO.image)
            let request2: NSURLRequest = NSURLRequest(url: url2 as! URL)
            let mainQueue2 = OperationQueue.main
            NSURLConnection.sendAsynchronousRequest(request2 as URLRequest, queue: mainQueue2, completionHandler: { (response, data, error) -> Void in
                if error == nil {
                    DispatchQueue.main.async(execute: {
                        self.sellerImage_Lbl.image = UIImage(data: data! as Data)
                    })
                }
            })

            let url = NSURL(string: self.buyerDTO.image)
            
            let request: NSURLRequest = NSURLRequest(url: url as! URL)
            let mainQueue = OperationQueue.main
            NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: mainQueue, completionHandler: { (response, data, error) -> Void in
                if error == nil {
                    DispatchQueue.main.async(execute: {
                        self.buyer_image.image = UIImage(data: data! as Data)
                    })
                }
            })

            
            self.nameLabel.text! = self.sellerDTO.name
            self.buyer_name_Lbl.text! = self.buyerDTO.username
            self.buyer_username.text! = self.buyerDTO.email
            self.descriptionOfProduct.text = self.pointsPaginateResults.product_description
           // self.transactionId.text = "\(self.pointsPaginateResults.data[0].points_transaction_id)"
           self.transactionPoints.text = "\(self.pointTransactionDTO.points)"
//            let resultOfTimeStamp =
//            let date = Date(timeIntervalSince1970: TimeInterval(resultOfTimeStamp)!)
//            let dateFormatter = DateFormatter()
//            dateFormatter.timeStyle = .short
//            dateFormatter.doesRelativeDateFormatting = true
//            let stringTimestampResponse = dateFormatter.string(from: date as Date)
            self.transactionDate.text = self.dateTransacDTO.date
            //self.transactionDate.text = "\(self.pointTransactionDTO.seller.sentOn)"
            self.username_Lbl.text! = self.sellerDTO.email
            self.product_name.text! = self.pointsPaginateResults.product_name
            self.points_Lbl.text! = self.pointsPaginateResults.product_points
            
          
    }
    }

    func fixOrientation(img: UIImage) -> UIImage {
        if (img.imageOrientation == .up) {
            return img
        }
        
        UIGraphicsBeginImageContextWithOptions(img.size, false, img.scale)
        let rect = CGRect(x: 0, y: 0, width: img.size.width, height: img.size.height)
        img.draw(in: rect)
        
        let normalizedImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return normalizedImage
    }
    
    func imageOrientation(_ src: UIImage) -> UIImage {
        if src.imageOrientation == UIImageOrientation.up {
            return src
        }
        var transform: CGAffineTransform = CGAffineTransform.identity
        switch src.imageOrientation {
        case UIImageOrientation.down, UIImageOrientation.downMirrored:
            transform = transform.translatedBy(x: src.size.width, y: src.size.height)
            transform = transform.rotated(by: CGFloat(M_PI))
            break
        case UIImageOrientation.left, UIImageOrientation.leftMirrored:
            transform = transform.translatedBy(x: src.size.width, y: 0)
            transform = transform.rotated(by: CGFloat(M_PI_2))
            break
        case UIImageOrientation.right, UIImageOrientation.rightMirrored:
            transform = transform.translatedBy(x: 0, y: src.size.height)
            transform = transform.rotated(by: CGFloat(-M_PI_2))
            break
        case UIImageOrientation.up, UIImageOrientation.upMirrored:
            break
        }
        
        switch src.imageOrientation {
        case UIImageOrientation.upMirrored, UIImageOrientation.downMirrored:
            transform.translatedBy(x: src.size.width, y: 0)
            transform.scaledBy(x: -1, y: 1)
            break
        case UIImageOrientation.leftMirrored, UIImageOrientation.rightMirrored:
            transform.translatedBy(x: src.size.height, y: 0)
            transform.scaledBy(x: -1, y: 1)
        case UIImageOrientation.up, UIImageOrientation.down, UIImageOrientation.left, UIImageOrientation.right:
            break
        }
        
        let ctx:CGContext = CGContext(data: nil, width: Int(src.size.width), height: Int(src.size.height), bitsPerComponent: (src.cgImage)!.bitsPerComponent, bytesPerRow: 0, space: (src.cgImage)!.colorSpace!, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue)!
        
        ctx.concatenate(transform)
        
        switch src.imageOrientation {
        case UIImageOrientation.left, UIImageOrientation.leftMirrored, UIImageOrientation.right, UIImageOrientation.rightMirrored:
            ctx.draw(src.cgImage!, in: CGRect(x: 0, y: 0, width: src.size.height, height: src.size.width))
            break
        default:
            ctx.draw(src.cgImage!, in: CGRect(x: 0, y: 0, width: src.size.width, height: src.size.height))
            break
        }
        
        let cgimg:CGImage = ctx.makeImage()!
        let img:UIImage = UIImage(cgImage: cgimg)
        
        return img
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func imageWithImage(image:UIImage, scaledToSize newSize:CGSize) -> UIImage{
        print("image of marker:- \(image)  &  \(newSize)")
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0);
        // image.draw(in: CGRectMake(0, 0, newSize.width, newSize.height))
        
        image.draw(in: CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: newSize.width, height: newSize.height)))
        //        image.draw(CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: newSize.width, height: newSize.height))  )
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        //        print(newImage)
        return newImage
    }
    
}
extension UIImage {
    func resized(withPercentage percentage: CGFloat) -> UIImage? {
        let canvasSize = CGSize(width: size.width * percentage, height: size.height * percentage)
        UIGraphicsBeginImageContextWithOptions(canvasSize, false, scale)
        defer { UIGraphicsEndImageContext() }
        draw(in: CGRect(origin: .zero, size: canvasSize))
        return UIGraphicsGetImageFromCurrentImageContext()
    }
    func resized(toWidth width: CGFloat) -> UIImage? {
        let canvasSize = CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))
        UIGraphicsBeginImageContextWithOptions(canvasSize, false, scale)
        defer { UIGraphicsEndImageContext() }
        draw(in: CGRect(origin: .zero, size: canvasSize))
        return UIGraphicsGetImageFromCurrentImageContext()
    }
}
