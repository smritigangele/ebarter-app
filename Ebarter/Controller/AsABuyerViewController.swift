//
//  AsABuyerViewController.swift
//  Ebarter
//
//  Created by Hocrox Infotech Pvt Ltd1 on 23/03/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import UIKit
import Material

class AsABuyerViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var transactionPaginate: TransactionPaginate = TransactionPaginate()
    var pointsPaginateResults: PointsPaginateResults = PointsPaginateResults()
    static let blue = UIColor(red: 0/255,green: 122/255, blue: 255/255, alpha: 1)
        required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
            preparePageTabBarItem()
            //pageTabBar()
        }
        
        init() {
            super.init(nibName: nil, bundle: nil)
            preparePageTabBarItem()
            //pageTabBar()
        }
        
        open override func viewDidLoad() {
            super.viewDidLoad()
            self.hideKeyboardWhenTappedAround()
            preparePageTabBarItem()
            //pageTabBar()
            tableView.delegate = self
            tableView.dataSource = self
            self.tableView.setNeedsLayout()
            self.tableView.layoutIfNeeded()
           
        }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         get_URL()
    }
    func get_URL()
    {
        UserService.getTransactionsByBuyer(view: self){
            (completion: TransactionPaginate) in
            print(completion)
            self.transactionPaginate = completion
                DispatchQueue.main.async {
            self.tableView.reloadData()
            }
        }

    }
        private func preparePageTabBarItem() {
            pageTabBarItem.title = "Buyer"//"As A Buyer"
            pageTabBarItem.titleColor = UIColor.white
    }
//         private func pageTabBar() {
//          pageTabBar().lineAlignment = .bottom
//    
//        }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
extension AsABuyerViewController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return transactionPaginate.data.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: OrdersViewCell! = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? OrdersViewCell
//        let url = NSURL(string: transactionPaginate.data[indexPath.row].image)
//
//        let request: NSURLRequest = NSURLRequest(url: url as! URL)
//        let mainQueue = OperationQueue.main
//        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: mainQueue, completionHandler: { (response, data, error) -> Void in
//            if error == nil {
//                DispatchQueue.main.async(execute: {
//                    cell.imageOfProduct.image = UIImage(data: data! as Data)
//                })
//            }
//     })
        cell.styling()
        cell.updateView(data: transactionPaginate.data[indexPath.row])
        
//        cell.product_name.text = self.transactionPaginate.data[indexPath.row].productName
//        cell.category_name.text = self.transactionPaginate.data[indexPath.row].product.category_name
////        cell.quantityOfProduct.text = self.transactionPaginate.data[indexPath.row].product.
//        cell.points_Lbl.text = "\(self.transactionPaginate.data[indexPath.row].productPoints)"
        return cell
    }
}
