//
//  PrivacyViewController.swift
//  Ebarterr
//
//  Created by Hocrox Infotech Pvt Ltd1 on 02/05/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import UIKit

class PrivacyViewController: UIViewController {

    
    @IBOutlet weak var webViewForPrivacy: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        webViewForPrivacy.loadRequest(NSURLRequest(url: NSURL(string: "http://ebarternow.com/privacy.html")! as URL) as URLRequest)
    }

    @IBAction func back_Btn(_ sender: Any) {
        dismiss(animated: true, completion: nil)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
