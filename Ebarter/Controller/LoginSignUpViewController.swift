//
//  LoginSignUpViewController.swift
//  Ebarter
//
//  Created by Hocrox Infotech Pvt Ltd1 on 21/03/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import UIKit
import Material

class LoginSignUpViewController: UIViewController {
    
static let blue = UIColor(red: 33/255,green: 119/255, blue: 232/255, alpha: 1)
    
    @IBOutlet weak var tabbedView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
         prepareToolbar()
        self.navigationDrawerController?.isLeftViewEnabled = false
        let loginController: LoginViewController = {
            return self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        }()
        
        let signUpController: SignUpViewController = {
            return self.storyboard?.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
        }()
        let item: LoginTabBarViewController = LoginTabBarViewController(viewControllers: [loginController, signUpController], selectedIndex: 0)
        add(asChildViewController: item)
    }
    
    fileprivate func prepareToolbar() {
       guard let tc = toolbarController as! AppToolbarController! else {
             return
            }
        
        tc.toolbar.title = "SignIn/SignUp"
        tc.toolbar.detail = ""
        tc.toggleMenus(left: false, right: false)
        
    }
    
    private func add(asChildViewController viewController: UIViewController) {
        addChildViewController(viewController)
        
        tabbedView.addSubview(viewController.view)
        viewController.view.frame = tabbedView.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        viewController.didMove(toParentViewController: self)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
