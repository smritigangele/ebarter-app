//
//  BarterProductsViewController.swift
//  Ebarter
//
//  Created by Hocrox Infotech Pvt Ltd1 on 30/03/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import UIKit
import Material

class BarterProductsViewController: UIViewController {
    
    static let blue = UIColor(red: 0/255,green: 122/255, blue: 255/255, alpha: 1)
    
    @IBOutlet weak var tabbedView: UIView!
    
    @IBOutlet weak var viewForBarterProduct: UIView!
    var parentVC: UIViewController!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        prepareToolbar()
        viewForBarterProduct.backgroundColor = BarterProductsViewController.blue
        let buyerController: AsABuyerViewController = {
            return self.storyboard?.instantiateViewController(withIdentifier: "AsABuyerViewController") as! AsABuyerViewController
        }()
        
        let sellerController: AsASellerViewController = {
            return self.storyboard?.instantiateViewController(withIdentifier: "AsASellerViewController") as! AsASellerViewController
        }()
        let item: LoginTabBarViewController = LoginTabBarViewController(viewControllers:  [buyerController, sellerController], selectedIndex: 0)
        add(asChildViewController: item)

    }
    
   
    @IBAction func back_Btn(_ sender: Any) {
        self.dismiss(animated: true, completion: {
            self.parentVC.dismiss(animated: false, completion: {})
        })

     // (UIApplication.shared.delegate as! AppDelegate).manageView()
    }
    
    fileprivate func prepareToolbar() {
        guard let tc = toolbarController else {
            return
        }
        
        tc.toolbar.title = "Orders"//"Barter"
        tc.toolbar.detail = ""
        // tc.toolbar.leftViews =
    }
    
    
    
    private func add(asChildViewController viewController: UIViewController) {
        addChildViewController(viewController)
        tabbedView.addSubview(viewController.view)
        viewController.view.frame = tabbedView.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        viewController.didMove(toParentViewController: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
