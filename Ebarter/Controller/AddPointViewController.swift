//
//  AddPointViewController.swift
//  Ebarterr
//
//  Created by Hocrox Infotech Pvt Ltd1 on 06/04/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import UIKit

class AddPointViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var totalPoints_Lbl: UILabel!
    static let pointsTransactionId = "pointsTransactionId"
    var pointsPaginateResults: PointsPaginateResults = PointsPaginateResults()
    var parentVC: UIViewController!
    //var pointsTransactionDTO:
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         get_URL()
    }
    
    func get_URL()
    {
        UserService.getPointsTransaction(view: self, paginate: HomeScreenVM())
        {
            (completion: PointsPaginateResults) in
            print(completion)
            self.pointsPaginateResults = completion
            print(self.pointsPaginateResults)
            
            if self.pointsPaginateResults.data.count == 0{
                self.totalPoints_Lbl.text! = ""
            }
            else{
            
            self.totalPoints_Lbl.text! = "\(self.pointsPaginateResults.data[0].totalPoints)"
            }
           // print(" get all data:::::\(self.pointsPaginateResults.data[0].points_transaction_id)")
            //let x = self.pointsPaginateResults.data[0].points_transaction_id
           // print("x....\(x)")
//            let transacId = self.pointsPaginateResults.data[0].points_transaction_id
//        UserDefaults.standard.setValue( x, forKey: AddPointViewController.pointsTransactionId)
//            UserDefaults.standard.synchronize()
            DispatchQueue.main.async {
                if self.pointsPaginateResults.data.count == 0 {
                if self.pointsPaginateResults.data[0].seller.email == "ebarter@ebarternow.com"{
                    print("sdfghj")
                 }
                }
                else{
                self.tableView.reloadData()
               }
            }
        }
    }

    @IBAction func add_Points_Btn(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "BuyPointsViewController") as! BuyPointsViewController
        
        present(vc, animated: true, completion: nil)
    }
    @IBAction func back_Btn(_ sender: Any) {
        self.dismiss(animated: true, completion: {
            self.parentVC.dismiss(animated: false, completion: {})
        })

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
extension AddPointViewController: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if pointsPaginateResults.data[0].seller.email == "ebarter@ebarternow.com"{
//            return 1
//        }
//        else{
            return pointsPaginateResults.data.count
       // }

    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

//        if pointsPaginateResults.data[indexPath.row].seller.email == "ebarter@ebarternow.com"{
//            
//            
//            let cell: AddPointsCell! = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? AddPointsCell
//            
//            return cell
//        }
//        else{
        
        let cell: AddPointsCell! = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? AddPointsCell
        //cell.imageOfProduct.layer.cornerRadius = 35/2
        
        cell.styling()
        cell.updateView(text: self.pointsPaginateResults.data[indexPath.row])
        let inputFormatter = DateFormatter()
        //let showDate = inputFormatter.date(from: self.pointsPaginateResults.data[indexPath.row].dateOfTransaction.date)
        inputFormatter.dateStyle = .short
        inputFormatter.dateFormat = "dd-MM"
//        let resultString = inputFormatter.string(from: showDate!)
//        print("date of transaction....\(resultString)")
        
        //dateFormatter.doesRelativeDateFormatting = true
//        let stringTimestampResponse = dateFormatter.string(from: resultOfTimeStamp as Date)
        
     
        cell.details_Btn.addTarget(self, action: #selector(showDetailView(_:)), for: .touchUpInside)
    return cell
       // }
    }
    func showDetailView(_ sender: UIButton){
        let vc = storyboard?.instantiateViewController(withIdentifier: "PointsDetailViewController") as! PointsDetailViewController
        
        vc.pointsPaginateResults = pointsPaginateResults.data[(tableView.indexPath(for:(sender.superview?.superview) as! AddPointsCell)?.row)!].product
        vc.pointsTransactionId = pointsPaginateResults.data[(tableView.indexPath(for:(sender.superview?.superview) as! AddPointsCell)?.row)!].points_transaction_id
        vc.sellerDTO = pointsPaginateResults.data[(tableView.indexPath(for:(sender.superview?.superview) as! AddPointsCell)?.row)!].seller
         vc.buyerDTO = pointsPaginateResults.data[(tableView.indexPath(for:(sender.superview?.superview) as! AddPointsCell)?.row)!].buyer
         vc.dateTransacDTO = pointsPaginateResults.data[(tableView.indexPath(for:(sender.superview?.superview) as! AddPointsCell)?.row)!].dateOfTransaction
       // vc.pointsPaginateResults = self.pointsPaginateResults
        present(vc, animated: true, completion: nil)
        
    }
    
}
