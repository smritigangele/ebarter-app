//
//  SignUpViewController.swift
//  Ebarter
//
//  Created by Hocrox Infotech Pvt Ltd1 on 21/03/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import UIKit
import Material

class SignUpViewController: UIViewController {
    static let blue = UIColor(red: 33/255,green: 119/255, blue: 232/255, alpha: 1)
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var fullnameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    static  let username = "username"
        required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        preparePageTabBarItem()
        //        styling()
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)
        preparePageTabBarItem()
        //        styling()
    }
    
   override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        preparePageTabBarItem()
       }
    private func preparePageTabBarItem() {
        pageTabBarItem.title = "SignUp"
        pageTabBarItem.titleColor = UIColor.white  //SignUpViewController.blue
    }

    @IBAction func signupBtn(_ sender: Any) {
            let signUp: SignupVM = SignupVM();
            signUp.username = usernameTextField.text!;
            signUp.email = emailTextField.text!;
        let isEmailAddressValid = isValidEmail(signUp.email)
        
        if isEmailAddressValid
        {
            print("Email address is valid")
        } else {
            print("Email address is not valid")
            self.createAlert(title: "", body: "Email is not valid")
        }
            signUp.fullname = fullnameTextField.text!;
            signUp.password = passwordTextField.text!;
            AccountService.signup(view: self, signup: signUp) {
                (completion: TokenDTO) in
                print(completion.token)
                self.createAlert(title: "Success", body: "Account created successfully")
                (UIApplication.shared.delegate as! AppDelegate).manageView()
//                let homePageViewController: HomePageViewController = {
//                    return UIStoryboard.viewController(identifier: "HomePageViewController") as! HomePageViewController
//                }()
//                homePageViewController.username = signUp
//                (self.navigationDrawerController?.rootViewController as? ToolbarController)?.transition(to: homePageViewController, completion: nil)
//                let editProfileViewController: EditProfileViewController = {
//                    return UIStoryboard.viewController(identifier: "EditProfileViewController") as! EditProfileViewController
//                }()
//                self.present(editProfileViewController, animated: true, completion: nil)
//                }()
//                (self.navigationDrawerController?.rootViewController as? ToolbarController)?.transition(to: editProfileViewController, completion: nil);
        }
    }
    
    func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "^(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?(?:(?:(?:[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+(?:\\.[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+)*)|(?:\"(?:(?:(?:(?: )*(?:(?:[!#-Z^-~]|\\[|\\])|(?:\\\\(?:\\t|[ -~]))))+(?: )*)|(?: )+)\"))(?:@)(?:(?:(?:[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)(?:\\.[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)*)|(?:\\[(?:(?:(?:(?:(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))\\.){3}(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))))|(?:(?:(?: )*[!-Z^-~])*(?: )*)|(?:[Vv][0-9A-Fa-f]+\\.[-A-Za-z0-9._~!$&'()*+,;=:]+))\\])))(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES[c] %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
