//
//  LoginViewController.swift
//  Ebarter
//
//  Created by Hocrox Infotech Pvt Ltd1 on 21/03/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import UIKit
import Material

class LoginViewController: UIViewController {
    static let blue = UIColor(red: 33/255,green: 119/255, blue: 232/255, alpha: 1)
     static  let username = "username"
 @IBOutlet weak var usernameTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    var userProfile = UserProfileDTO()
    
    //var appNavigationDrawerController: AppNavigationDrawerController  = AppNavigationDrawerController()
    var deviceRegisterDTO: DeviceRegisterDTO = DeviceRegisterDTO()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        preparePageTabBarItem()
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)
        preparePageTabBarItem()
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        preparePageTabBarItem()
    }

    @IBAction func loginBtn(_ sender: Any) {
       
        let login: LoginVM = LoginVM()
        login.loginType = "NORMAL"
        login.password = passwordTextField.text!
        login.username = usernameTextField.text!
    
        UserDefaults.standard.setValue(login.username, forKey: LoginViewController.username)
        let isEmailAddressValid = isValidEmail(login.username)
        
        if isEmailAddressValid
        {
            print("Email address is valid")
        } else {
            print("Email address is not valid")
            self.createAlert(title: "", body: "Email is not valid")
        }
        AccountService.login(view: self, login: login) {
            (completion: TokenDTO) in
            print(completion.token)
            self.register_Device()
            if completion.token != nil{
        UserService.getCurrentUser(view: self , paginate: HomeScreenVM()){
                (completion: UserProfileDTO) in
                print(completion)
            self.userProfile = completion
            let sideBarViewController = {
                return UIStoryboard.viewController(identifier: "SideBarViewController") as! SideBarViewController
            }()
            if self.userProfile.user_image != nil{
                
                let homePageViewController: HomePageViewController = {
                    return UIStoryboard.viewController(identifier: "HomePageViewController") as! HomePageViewController
                }()
                self.navigationDrawerController?.isLeftViewEnabled = true
                (self.navigationDrawerController?.rootViewController as? ToolbarController)?.transition(to: homePageViewController, completion: nil);
            }
            else{
                
                let vc = { return UIStoryboard.viewController(identifier: "EditProfileViewController") as! EditProfileViewController
                }()
                //                    self.presentNextView("EditProfileViewController")
//                self.window!.rootViewController = vc
//                UIApplication.shared.statusBarStyle = .lightContent
                self.present(vc, animated: true, completion: nil)
            }
            }
            }
//            else{
//           let homePageViewController: HomePageViewController = {
//                return UIStoryboard.viewController(identifier: "HomePageViewController") as! HomePageViewController
//            }()
//            self.navigationDrawerController?.isLeftViewEnabled = true
//            (self.navigationDrawerController?.rootViewController as? ToolbarController)?.transition(to: homePageViewController, completion: nil);
//            }
        }
    }
    func register_Device(){
        let uuid = UIDevice.current.identifierForVendor?.uuidString
        print(uuid ?? "device type::::::")
        let  deviceName = UIDevice.current.name
        let deviceRegister: DeviceRegisterVM = DeviceRegisterVM()
        deviceRegister.device_name = deviceName
        deviceRegister.messaging_id = deviceTokenApp
        deviceRegister.device_type = "IOS"
      AccountService.deviceRegister(view: self, address: deviceRegister){
            (completion: DeviceRegisterDTO) in
            print(completion)
          self.deviceRegisterDTO = completion
        
     //8B5A7B6419976155AD7E80C0571F90F05E981E8A748378946DD4883629E31148
        
        }
    }
    
    private func preparePageTabBarItem() {
        pageTabBarItem.title = "SignIn"
        pageTabBarItem.titleColor = UIColor.white
       // pageTabBarItem.titleColor = LoginViewController.blue
    }
    
    func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "^(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?(?:(?:(?:[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+(?:\\.[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+)*)|(?:\"(?:(?:(?:(?: )*(?:(?:[!#-Z^-~]|\\[|\\])|(?:\\\\(?:\\t|[ -~]))))+(?: )*)|(?: )+)\"))(?:@)(?:(?:(?:[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)(?:\\.[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)*)|(?:\\[(?:(?:(?:(?:(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))\\.){3}(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))))|(?:(?:(?: )*[!-Z^-~])*(?: )*)|(?:[Vv][0-9A-Fa-f]+\\.[-A-Za-z0-9._~!$&'()*+,;=:]+))\\])))(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES[c] %@", emailRegEx)
        return emailTest.evaluate(with: email)

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
}
