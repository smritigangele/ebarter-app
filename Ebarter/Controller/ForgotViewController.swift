//
//  ForgotViewController.swift
//  Ebarter
//
//  Created by Hocrox Infotech Pvt Ltd1 on 30/03/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import UIKit

class ForgotViewController: UIViewController {

    var forgotPassword: ForgotPasswordDTO = ForgotPasswordDTO()
    
    @IBOutlet weak var emailTextField: UITextField!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"+"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"+"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"+"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"+"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"+"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"+"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
        
        let emailTest = NSPredicate(format:"SELF MATCHES[c] %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    @IBAction func Done_Btn(_ sender: Any) {
        
        let forgotPassword: ForgotPasswordVM = ForgotPasswordVM()
        forgotPassword.email = self.emailTextField.text!
        let isEmailAddressValid = isValidEmail(forgotPassword.email)
        
        if isEmailAddressValid
        {
            print("Email address is valid")
        } else {
            print("Email address is not valid")
           self.createAlert(title: "", body: "Email is not valid")
        }
        AccountService.forgotPassword(view: self, address: forgotPassword)
        {
            (completion: ForgotPasswordDTO) in
            print(completion)
            self.forgotPassword = completion
            let vc: ResetViewController = {
                return UIStoryboard.viewController(identifier: "ResetViewController") as! ResetViewController
            }()
           self.present(vc, animated: true, completion: nil)
         }
    }
    
    @IBAction func actionOnBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
