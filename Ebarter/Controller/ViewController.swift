//
//  ViewController.swift
//  Ebarter
//
//  Created by Hocrox Infotech Pvt Ltd1 on 21/03/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var textField: UITextField!
    var arry = ["A","B","C","D","E","F"]
    var selectedData: [String] = [String]()
    var searchedText: [String] = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()
        textField.delegate = self
        tableView.delegate = self
        tableView.dataSource = self
      
        self.tableView.reloadData()
           }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension ViewController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if textField.isFirstResponder{
             searchedText = arry.filter({$0.range(of: textField.text!) != nil})
            return searchedText.count
        }
        return arry.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        var textname: String!
        if textField.isFirstResponder{
        textname = searchedText[indexPath.row]
       }
        else{
            textname = arry[indexPath.row]
        }
         cell.textLabel?.text = textname
            return cell
        }
}

extension ViewController: UITextFieldDelegate {
        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("is first responder...")
        selectedData = arry
        print(selectedData)
        searchedText = selectedData.filter({$0.range(of: textField.text!) != nil})
        print(searchedText)
        self.tableView.reloadData()
        return true
    }
}

