//
//  AsASellerViewController.swift
//  Ebarter
//
//  Created by Hocrox Infotech Pvt Ltd1 on 23/03/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import UIKit
import Material

class AsASellerViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var transactionPaginate: TransactionPaginate = TransactionPaginate()

    var pointsPaginateResult: PointsPaginateResults = PointsPaginateResults()
    static let blue = UIColor(red: 0/255,green: 122/255, blue: 255/255, alpha: 1)
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        preparePageTabBarItem()
        
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)
        preparePageTabBarItem()
          }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        preparePageTabBarItem()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 100.0
        tableView.rowHeight = UITableViewAutomaticDimension
    
//        self.tableView.setNeedsLayout()
//        self.tableView.layoutIfNeeded()
       
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        get_URL()
    }
    func get_URL()
    {
        UserService.getTransactionsBySeller(view: self){
            (completion: TransactionPaginate) in
            print(completion)
            self.transactionPaginate = completion
            print(self.pointsPaginateResult)
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
        
    }
    
    private func preparePageTabBarItem() {
        pageTabBarItem.title = "Seller"//"As a Seller"
        pageTabBarItem.titleColor = UIColor.white
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
extension AsASellerViewController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return transactionPaginate.data.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: OrdersViewCell! = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? OrdersViewCell
        cell.styling()
        cell.updateSellerView(data: transactionPaginate.data[indexPath.row])
//        let url = NSURL(string: pointsPaginateResult.data[indexPath.row].product.image)
//        let request: NSURLRequest = NSURLRequest(url: url as! URL)
//        let mainQueue = OperationQueue.main
//        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: mainQueue, completionHandler: { (response, data, error) -> Void in
//            if error == nil {
//                DispatchQueue.main.async(execute: {
//                    cell.imageOfSellerProduct.image = UIImage(data: data! as Data)
//                })
//            }
//        })
        
//        cell.product_Of_Seller.text = self.transactionPaginate.data[indexPath.row].productName
//       // cell.categoryOfProduct.text = self.transactionPaginate.data[indexPath.row]
////        cell.productQuantity.text = self.transactionPaginate.data[indexPath.row].product_quantity
//        cell.product_Points.text = "\(self.transactionPaginate.data[indexPath.row].productPoints)"
        return cell
        
}
}

