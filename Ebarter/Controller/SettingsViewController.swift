//
//  SettingsViewController.swift
//  Ebarter
//
//  Created by Hocrox Infotech Pvt Ltd1 on 22/03/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import UIKit
import Material
import Kingfisher

class SettingsViewController: UIViewController {

    @IBOutlet weak var settingsView: UIView!
    @IBOutlet weak var tableViewForAccounts: UITableView!
    @IBOutlet weak var tableViewForAbout: UITableView!
    var accountSettingsItems = ["Edit Profile","Change Password","Logout"]
    var aboutSettingsItems = ["Privacy Policy","Terms Of Use","FAQ"]
     static var blue = UIColor(red: 40/255, green: 116/255, blue: 240/255, alpha: 1)
    var userProfileDTO: UserProfileDTO = UserProfileDTO()
    var parentVC: UIViewController!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewForAbout.delegate=self
        tableViewForAbout.dataSource=self
        tableViewForAccounts.delegate=self
        tableViewForAccounts.dataSource=self
        settingsView.backgroundColor = SettingsViewController.blue
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    @IBAction func actionOnBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: {
            self.parentVC.dismiss(animated: false, completion: {})
        })
       // (UIApplication.shared.delegate as! AppDelegate).manageView()

    }

}
extension SettingsViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count: Int!
        if tableView == tableViewForAccounts {
            count = accountSettingsItems.count
        }
        if tableView == tableViewForAbout{
            count = aboutSettingsItems.count
        }
        return count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell!
        if tableView == tableViewForAccounts{
            cell = tableViewForAccounts.dequeueReusableCell(withIdentifier: "cellForAccounts", for: indexPath)
            cell.textLabel?.text = accountSettingsItems[indexPath.row]
             cell.textLabel?.font = UIFont(name: (cell.textLabel?.font.fontName)!, size:15)
        }
        if tableView == tableViewForAbout{
            cell = tableViewForAbout.dequeueReusableCell(withIdentifier: "cellForAbout", for: indexPath)
            cell.textLabel?.text = aboutSettingsItems[indexPath.row]
             cell.textLabel?.font = UIFont(name: (cell.textLabel?.font.fontName)!, size:15)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tableViewForAccounts{
        actionOnCell(indexPath.row)
        }
        if tableView == tableViewForAbout{
            actionOnAboutCell(indexPath.row)
        }
    }
    func actionOnCell(_ index: Int)
    {
        
        switch index
        {
        case 0:
           
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
            
            UserService.getUserProfile(view: self){
                //UserService.getAllImages(view: self, paginate: HomeScreenVM()){
                (completion: UserProfileDTO) in
                print(completion)
                
                self.userProfileDTO = completion
            let url = URL(string: self.userProfileDTO.user_image)
             vc.uploadImage.kf.setImage(with: url!)
                print("image of user:- \(url!)")
                vc.mobileNoField.text! = "\(self.userProfileDTO.user_mobile)"
                vc.houseNoField.text! = self.userProfileDTO.user_house_number
                vc.cityTextField.text! = self.userProfileDTO.user_city
                vc.colonyTextField.text! = self.userProfileDTO.user_locality
                vc.pinTextField.text! =  "\(self.userProfileDTO.user_pincode)"
                vc.stateTextField.text! = self.userProfileDTO.user_state
                vc.descriptionTextField.text! = self.userProfileDTO.user_description
            }
            present(vc, animated: true, completion: nil)
            
        case 1:
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordViewController") as! ChangePasswordViewController
            present(vc, animated: true, completion: nil)
        case 2:
            Service.logout(view: self)
            (UIApplication.shared.delegate as! AppDelegate).manageView()
//             UserDefaults.standard.set(nil, forKey: Service.ebarterToken)
//            let loginSignUpViewController: LoginSignUpViewController = {
//                return UIStoryboard.viewController(identifier: "LoginSignUpViewController") as! LoginSignUpViewController
//            }()
//            (self.navigationDrawerController?.rootViewController as? ToolbarController)?.transition(to: loginSignUpViewController, completion: nil);
//            let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginSignUpViewController") as! LoginSignUpViewController
//            present(vc, animated: true, completion: nil)
        default:
            break
        }
    }
    func actionOnAboutCell(_ index: Int){
        switch index{
        case 0:
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PrivacyViewController") as! PrivacyViewController
            present(vc, animated: true, completion: nil)
        case 1:
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "TermsOfUseViewController") as! TermsOfUseViewController
            present(vc, animated: true, completion: nil)
        case 2:
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "FAQViewController") as! FAQViewController
            present(vc, animated: true,completion: nil)
        default:
            print("nothing is thr")
        }
    }
}
