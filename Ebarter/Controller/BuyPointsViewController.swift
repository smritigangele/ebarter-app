//
//  BuyPointsViewController.swift
//  Ebarterr
//
//  Created by Hocrox Infotech Pvt Ltd1 on 12/04/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import UIKit
import Stripe

class BuyPointsViewController: UIViewController{

    @IBOutlet weak var textFieldForPoints: UITextField!
    
    @IBOutlet weak var cardNoTextField: UITextField!
    
    @IBOutlet weak var expYrTxtField: UITextField!
    @IBOutlet weak var monthTextField: UITextField!
    
    @IBOutlet weak var cvvField: UITextField!
    @IBOutlet weak var saveDetailsBtn: UIButton!
    @IBOutlet weak var cardImage: UIImageView!
    
   // @IBOutlet weak var payNow_Btn: UIButton!
    
    var purchase_Points: PurchasePointsVM = PurchasePointsVM()
    var purchase_Points_DTO: PurchasePointsDTO = PurchasePointsDTO()
    var buyPoints: BuyPointsDTO = BuyPointsDTO()
    @IBOutlet weak var pay_now: UIButton!
    var tap: Bool = false
    var toggleBtn = false {
        didSet {
            if (toggleBtn == true) {
                saveDetailsBtn.setBackgroundImage(UIImage(named: "checked"), for: UIControlState.normal)
            } else {
                saveDetailsBtn.setBackgroundImage(UIImage(named: ""), for: UIControlState.normal)
            }
            
        }
    }
    
  let addCardViewController = STPAddCardViewController()
    
    
    
     override func viewDidLoad() {
        super.viewDidLoad()
        addCardViewController.delegate = self
     cardNoTextField.isEnabled = false
        expYrTxtField.isEnabled = false
        monthTextField.isEnabled = false
        cvvField.isEnabled = false
       cardImage.image = #imageLiteral(resourceName: "cardno")
            }
    
    @IBAction func bck_Btn(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
        @IBAction func check(_ sender: Any) {
//            if tap == false{
//                tap = true
//                
//            }
//            else{
//                tap = false
//            saveDetailsBtn.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
//                print("card details saved")
//            }
             self.toggleBtn =  !self.toggleBtn;
        
            
    }

    @IBAction func buy_Points(_ sender: Any) {
        let points: BuyPointsVM = BuyPointsVM()
        if textFieldForPoints.text != ""{
            points.points = self.textFieldForPoints.text!
            AccountService.addPoints(view: self, points: points){
                (completion: BuyPointsDTO) in
                print(completion)
                 self.buyPoints = completion
                self.createAlert(title: "Successfully Added", body: "you are now able to pay")
                self.cardNoTextField.isEnabled = true
                self.monthTextField.isEnabled = true
                self.expYrTxtField.isEnabled = true
                self.cvvField.isEnabled = true
                self.cardImage.image = #imageLiteral(resourceName: "mastercard")
            
//                self.cardNoTextField.text = ""
//                self.monthTextField.text = ""
//                self.expYrTxtField.text = ""
//                self.cvvField.text = ""
                self.pay_now.setImage(#imageLiteral(resourceName: "point"), for: .normal)
                self.pay_now.setTitle("Pay", for: .normal)
                //self.pay_now.titleLabel = .center
        }
  }
        else
        {
            self.createAlert(title:"", body: "Please enter points")
        }
    }
    func submitTokenToBackend(_ token: STPToken?, completion: (Error)->Void){
        print("token passed",token?.tokenId ?? "this ")
        self.purchase_Points.stripeToken =  (token!.tokenId)
        self.purchase_Points.amount = "\(buyPoints.amount)"
        print(purchase_Points.amount)
        AccountService.purchasePoints(view: self, points: purchase_Points){
            (completion: PurchasePointsDTO) in
            print(completion)
            self.purchase_Points_DTO = completion
            print(self.purchase_Points)
             self.createAlert(title: "Successfull", body: "purchase done")
                }
    }
    @IBAction func pay_now_Btn(_ sender: Any) {
        let cardParams = STPCardParams()
        cardParams.number = cardNoTextField.text
        cardParams.expMonth = 10
        cardParams.expYear = 2018
        cardParams.cvc = cvvField.text!
        STPAPIClient.shared().createToken(withCard: cardParams) { (token, error) in
            if error != nil {
                // show the error to the user
            } else if let token = token {
                self.submitTokenToBackend(token, completion: { (error: Error?) in
                    if error != nil {
                        // show the error to the user
                    } else {
                        // show a receipt page
                    }
                })
            }
        }
        
        }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension BuyPointsViewController: STPAddCardViewControllerDelegate{
    
    func addCardViewControllerDidCancel(_ addCardViewController: STPAddCardViewController)
    {
        
    }
 func addCardViewController(_ addCardViewController: STPAddCardViewController, didCreateToken token: STPToken, completion: @escaping STPErrorBlock) {
        self.submitTokenToBackend(token, completion: { (error: Error?) in
            if let error = error {
                completion(error)
            } else {
                self.dismiss(animated: true, completion: {
                    self.showReceiptPage()
                    completion(nil)
                })
            }
        })
    }
    
 func showReceiptPage(){
        print("successfull")
    }
    

   }


       
