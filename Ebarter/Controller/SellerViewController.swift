//
//  SellerViewController.swift
//  Ebarter
//
//  Created by Hocrox Infotech Pvt Ltd1 on 30/03/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import UIKit
import Material

class SellerViewController: UIViewController{
    
  
    @IBOutlet weak var tableView: TableView!
    var sellerPaginateDTO: SellerPaginateDTO = SellerPaginateDTO()
    let type = "SELLER"

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        preparePageTabBarItem()
        
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)
        preparePageTabBarItem()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        preparePageTabBarItem()
       tableView.delegate = self
        tableView.dataSource = self
         self.view.layoutIfNeeded()
//        tableView.rowHeight = UITableViewAutomaticDimension
//        tableView.estimatedRowHeight = 100
//         //self.tableView.layoutSubviews()
        self.tableView.setNeedsLayout()
        self.tableView.layoutIfNeeded()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        get_Url()
    }
    
    func get_Url()
    {
        UserService.getChatListOfBuyerSeller(view: self, type: type, paginate: HomeScreenVM())
        {
            (completion: SellerPaginateDTO) in
           print(completion)
           self.sellerPaginateDTO = completion
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    private func preparePageTabBarItem() {
        pageTabBarItem.title = "As a Seller"//"Seller"
        pageTabBarItem.titleColor = UIColor.white
        
    }

}
extension SellerViewController: UITableViewDataSource, UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sellerPaginateDTO.data.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! SellerTableViewCell
        cell.styling()
        cell.updateView(text: self.sellerPaginateDTO.data[indexPath.row])
        cell.action_Btn.addTarget(self, action: #selector(handleCells(_:)), for: .touchUpInside)
        return cell
    }
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableViewAutomaticDimension
//    }
//
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableViewAutomaticDimension
//    }
    func handleCells(_ sender: UIButton)
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "NegotiateViewController") as! NegotiateViewController
        vc.products = sellerPaginateDTO.data[(tableView.indexPath(for:(sender.superview?.superview?.superview) as! SellerTableViewCell)?.row)!].product
        vc.isFromSeller = true
        vc.sendMsgFromBuyer = true
        vc.buyerDTO = sellerPaginateDTO.data[(tableView.indexPath(for:(sender.superview?.superview?.superview) as! SellerTableViewCell)?.row)!]
//    vc.buyer_name = sellerPaginateDTO.data[(tableView.indexPath(for:(sender.superview?.superview) as! SellerTableViewCell)?.row)!].sellerName
        present(vc, animated: true, completion: nil)
    }
    }
