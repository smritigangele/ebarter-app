//
//  ResetViewController.swift
//  Ebarterr
//
//  Created by Hocrox Infotech Pvt Ltd1 on 02/05/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import UIKit

class ResetViewController: UIViewController {

    
    @IBOutlet weak var otpField: UITextField!
    
    
    @IBOutlet weak var newPassword: UITextField!
    
    var resetPasswordDTO: ForgotPasswordUserDTO = ForgotPasswordUserDTO()
    override func viewDidLoad() {
        super.viewDidLoad()
     
    }
    
    @IBAction func reset_Password_Btn(_ sender: Any) {
        
        let resetPassword: ResetPasswordVM = ResetPasswordVM()
        resetPassword.reset_token = self.otpField.text!
        resetPassword.new_password = self.newPassword.text!
        AccountService.resetPassword(view: self, address: resetPassword){
            (completion: ForgotPasswordUserDTO) in
            print(completion)
              self.resetPasswordDTO = completion
       (UIApplication.shared.delegate as! AppDelegate).manageView()
            
        }
    }
    
    @IBAction func actionOnBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
