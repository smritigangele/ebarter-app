//
//  SideBarViewController.swift
//  Ebarter
//
//  Created by Hocrox Infotech Pvt Ltd1 on 22/03/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import UIKit
import Material

class SideBarViewController: UIViewController {
    
    static let yellow = UIColor(red: 236/255, green: 171/255, blue: 38/255, alpha: 1)
    
    @IBOutlet weak var postProductBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    var items = ["Orders","Points","Barter","My Products","Settings","Share"]
    var images = ["orders","points","barter","myproduct","settings","share"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        postProductBtn.backgroundColor = SideBarViewController.yellow
    }
    
    @IBAction func actionOnPostBtn(_ sender: Any)
    {
     self.navigationDrawerController?.navigationDrawerController?.closeLeftView()
        let vc = storyboard?.instantiateViewController(withIdentifier: "AddProductViewController") as! AddProductViewController
        
        present(vc, animated: true, completion: nil)
    }
  }
extension SideBarViewController: UITableViewDelegate,UITableViewDataSource{
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SideMenuTableViewCell
        
        let image = images[indexPath.row]
        
        //image.size = CGSize(width: 21, height: 21)
        cell.headerLabel?.text = self.items[indexPath.row]
        cell.headerLabel?.font = UIFont.systemFont(ofSize: 15)
        
  
       
        cell.imageVW.image = UIImage(named: image)
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("....")
        actionOnCell(indexPath.row)
        
    }
    func actionOnCell(_ index: Int){
        switch index{
        case 0:
            self.navigationDrawerController?.navigationDrawerController?.closeLeftView()
        let vc = storyboard?.instantiateViewController(withIdentifier: "OrdersViewController") as! OrdersViewController
            vc.parentVC = self
            present(vc, animated: true, completion: nil)
            
            //(self.navigationDrawerController?.rootViewController as? ToolbarController)?.transition(to: vc)
        case 1:
            self.navigationDrawerController?.navigationDrawerController?.closeLeftView()
            let vc = storyboard?.instantiateViewController(withIdentifier: "AddPointViewController") as! AddPointViewController
            vc.parentVC = self
            present(vc, animated: true, completion: nil)
            
        case 2:
            
           self.navigationDrawerController?.navigationDrawerController?.closeLeftView()
            let vc =  storyboard?.instantiateViewController(withIdentifier: "BarterProductsViewController") as! BarterProductsViewController
            vc.parentVC = self
            present(vc, animated: true, completion: nil)
//            (self.navigationDrawerController?.rootViewController as? ToolbarController)?.transition(to: vc)
      case 3:
        self.navigationDrawerController?.navigationDrawerController?.closeLeftView()
            let vc = storyboard?.instantiateViewController(withIdentifier: "MyProductViewController") as! MyProductViewController
            vc.parentVC = self
           present(vc, animated: true, completion: nil)
            
        case 4:
            self.navigationDrawerController?.navigationDrawerController?.closeLeftView()
          let vc = storyboard?.instantiateViewController(withIdentifier: "SettingsViewController")as! SettingsViewController
           vc.parentVC = self
            present(vc,animated: true, completion: nil)
            
        case 5:
            self.navigationDrawerController?.navigationDrawerController?.closeLeftView()
            let textToShare = "This would be ebarter app url"
            let text = [textToShare]
            let activityViewController = UIActivityViewController(activityItems: text, applicationActivities: nil)
                present(activityViewController, animated: true, completion: {})
        
//            let settingsUrl = NSURL(string:UIAPplicat) as! URL
//            UIApplication.shared.open(settingsUrl, options: [:], completionHandler: nil)
            
        default:
            print("something...")

    }
}
}
