//
//  Product_DetailViewController.swift
//  Ebarter
//
//  Created by Hocrox Infotech Pvt Ltd1 on 24/03/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import UIKit

class Product_DetailViewController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var category_name: UILabel!
    @IBOutlet weak var product_name: UILabel!
    @IBOutlet weak var sent_On_Label: UILabel!
    @IBOutlet weak var description_Label: UILabel!
    @IBOutlet weak var negotiateBtn: UIButton!
    @IBOutlet weak var buyNowBtn: UIButton!
    @IBOutlet weak var imageOfSeller: UIImageView!
    @IBOutlet weak var sellerName: UILabel!
    @IBOutlet weak var sellerNumber: UILabel!
    @IBOutlet weak var sellerEmail: UILabel!
    @IBOutlet weak var product_Points: UILabel!
    var productDTO: ProductDTO = ProductDTO()
    var productPaginateDTO: ProductPaginateDTO = ProductPaginateDTO()
    var homeScreenPaginateDTO: HomeScreenPaginateDTO = HomeScreenPaginateDTO()
    var username:String!
    override func viewDidLoad() {
        super.viewDidLoad()
        let url = NSURL(string: self.productDTO.image)
        
        let request: NSURLRequest = NSURLRequest(url: url as! URL)
        let mainQueue = OperationQueue.main
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: mainQueue, completionHandler: { (response, data, error) -> Void in
            if error == nil {
                DispatchQueue.main.async(execute: {
                    self.imageView.image = UIImage(data: data! as Data)
                })
            }
        })
            let url1 = NSURL(string: self.productDTO.sellerImage)
            
            let request1: NSURLRequest = NSURLRequest(url: url1 as! URL)
            let mainQueue1 = OperationQueue.main
            NSURLConnection.sendAsynchronousRequest(request1 as URLRequest, queue: mainQueue1, completionHandler: { (response, data, error) -> Void in
                if error == nil {
                    DispatchQueue.main.async(execute: {
                      self.imageOfSeller.image = UIImage(data: data! as Data)
                    })
                }
                else{
                 self.imageView.image=UIImage(cgImage: #imageLiteral(resourceName: "placeholderr") as! CGImage)
                }
            })
            self.category_name.text! = self.productDTO.categoryName
        self.product_name.text! = self.productDTO.name
         let resultOfTimeStamp = self.productDTO.sentOn
        
        let date = NSDate(timeIntervalSince1970: TimeInterval(resultOfTimeStamp))  
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .short
        dateFormatter.dateFormat = "dd-MM"
        let stringTimestampResponse = dateFormatter.string(from: date as Date)
        self.sent_On_Label.text! = stringTimestampResponse
        self.sellerName.text! = self.productDTO.sellerName
        self.sellerNumber.text! = self.productDTO.sellerMobile
        self.sellerEmail.text! = self.productDTO.sellerEmail
        self.product_Points.text! = self.productDTO.points
        self.description_Label.text! = self.productDTO.description1
        
    }
    @IBAction func bckBtn(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func chat_Btn(_ sender: Any) {
        
        let chatInitiate: ChatInitiateVM = ChatInitiateVM()
         chatInitiate.product_id = productDTO.product_id
        
       var chatPaginateDTO: ChatInitiatePaginate = ChatInitiatePaginate()
       //print("\(UserDefaults.standard.string(forKey: AppDelegate.name)! as String) && \(self.productDTO.sellerName)")
        
// if ((UserDefaults.standard.string(forKey: AppDelegate.name)! as String) != self.productDTO.sellerName){
        
        AccountService.chatInitiate(view: self, chatInitiate: chatInitiate)
        {
            (completion: ChatInitiatePaginate) in
            print(completion)
           chatPaginateDTO = completion
            print(chatPaginateDTO)
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "NegotiateViewController") as! NegotiateViewController
            vc.productDTO = self.productDTO
           vc.chatid = chatPaginateDTO.chat_id
            vc.buyername = chatPaginateDTO.buyerName
            vc.chatPaginateDTO = chatPaginateDTO
            vc.isBuyer = self.productDTO.sellerEmail
            vc.isFromBuyer = true
            vc.isFormProducDetails = true
            //vc.username = self.username
            vc.sold_price = chatPaginateDTO.preTrans.pre_transaction_sold_price
            vc.negotiationStatus = chatPaginateDTO.negotiationStatus
            self.present(vc, animated: true, completion: nil)
            
            }
            //  }
//            else {
//                self.createAlert(title: "", body: "No chats found")
//            }
    }
    @IBAction func buyNow(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "ShippingViewController") as! ShippingViewController
         vc.isFromNegotiation = false
        vc.product_id = [String(describing: self.productDTO)]
        vc.productDTO = self.productDTO
        present(vc, animated: true, completion: nil)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    }
