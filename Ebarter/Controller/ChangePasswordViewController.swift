//
//  ChangePasswordViewController.swift
//  
//
//  Created by Hocrox Infotech Pvt Ltd1 on 23/03/17.
//
//

import UIKit

class ChangePasswordViewController: UIViewController {
    
    @IBOutlet weak var oldPasswordField: HTextField!
    @IBOutlet weak var confirmTextField: UITextField!
    @IBOutlet weak var newPasswordTextField: HTextField!
    var changePassword: ForgotPasswordUserDTO = ForgotPasswordUserDTO()
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    @IBAction func actionOnBckBtn(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

    @IBAction func clickedOnchangeAction(_ sender: Any) {
        let changePassword: ChangePasswordVM = ChangePasswordVM();
        if self.oldPasswordField.text! == ""{
        self.createAlert(title: "", body: "Please enter your old password")
            return
        }
        if newPasswordTextField.text! == ""{
        self.createAlert(title: "", body: "Please enter your new password")
             return
        }
        if confirmTextField.text! == ""{
            self.createAlert(title: "", body: "Please enter your new password")
             return
        }
        if newPasswordTextField.text! != confirmTextField.text!{
            self.createAlert(title: "Alert!", body: "Your new password and confirm password does not match")
             return
        }
        else
        {
            
        changePassword.old_password = oldPasswordField.text!;
        changePassword.new_password = newPasswordTextField.text!;
        changePassword.confirm_password = confirmTextField.text!;
        AccountService.changePassword(view: self, changePassword: changePassword) {
            (completion: ForgotPasswordUserDTO) in
            print(completion)
            self.createAlert(title: "Success", body: "Account created successfully")
            Service.logout(view: self)
            (UIApplication.shared.delegate as! AppDelegate).manageView()
            }
       }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
