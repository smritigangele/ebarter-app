//
//  LoginTabBarViewController.swift
//
//  Created by Hocrox Infotech Pvt Ltd1 on 21/03/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import UIKit
import Material

class LoginTabBarViewController: PageTabBarController {
    
    static let darkblue = UIColor(red: 29/255,green: 104/255, blue: 201/255, alpha: 1)
    static let lightblue = UIColor(red: 33/255,green: 124/255, blue: 242/255, alpha: 1)
    static let blue = UIColor(red: 33/255,green: 119/255, blue: 232/255, alpha: 1)
    
    open override func prepare() {
        super.prepare()
        preparePageTabBar()
        //pageTabBar
    }
    
    private func preparePageTabBar() {
        pageTabBar.lineColor = .white  //LoginTabBarViewController.darkblue
        pageTabBar.backgroundColor = LoginTabBarViewController.lightblue
       // pageTabBar.tintColor = LoginTabBarViewController.blue
        self.pageTabBarAlignment = PageTabBarAlignment.top
        pageTabBar.lineAlignment = .bottom
        pageTabBar.borderWidth = 0
        pageTabBar.borderColor = .clear
        pageTabBar.dividerColor = LoginTabBarViewController.lightblue
        
            }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}

extension LoginTabBarViewController: PageTabBarControllerDelegate {
    func pageTabBarController(pageTabBarController: PageTabBarController, didTransitionTo viewController: UIViewController) {
     print("pageTabBarController", pageTabBarController, "didTransitionTo viewController:", viewController)
    }
    
}

