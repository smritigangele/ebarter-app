//
//  NegotiateViewController.swift
//  
//
//  Created by Hocrox Infotech Pvt Ltd1 on 27/03/17.
//
//

import UIKit
//import Kingfisher
import UserNotifications
var openNewWindow: UIWindow!
import XCGLogger

class NegotiateViewController: UIViewController, UNUserNotificationCenterDelegate{
   var bck: Bool = false
   static let blue = UIColor(red: 33/255,green: 119/255, blue: 232/255, alpha: 1)
    @IBOutlet weak var bckBtn: UIButton!
    
    
    @IBOutlet var negotiatedPriceLbl: UILabel!
    
    @IBOutlet var negotiatedPrice: UILabel!
    
    
    @IBOutlet var acceptBtn: UIButton!
    
    @IBOutlet var declineBtn: UIButton!
    
    @IBOutlet weak var imageOfProduct: UIImageView!
    @IBOutlet weak var labelForProductName: UILabel!
    @IBOutlet weak var labelForCategoryName: UILabel!
    @IBOutlet weak var labelForPoints: UILabel!
    @IBOutlet weak var labelForOldPrice: UILabel!
    @IBOutlet weak var textFieldForMessage: UITextField!
  
    @IBOutlet weak var update_price_height: NSLayoutConstraint!
    @IBOutlet weak var update_price_top: NSLayoutConstraint!
    
    @IBOutlet weak var update_price: UIButton!
    static let yellow = UIColor(red: 236/255, green: 171/255, blue: 38/255, alpha: 1)
    var changeInHeight: CGFloat = 0.0
    var catId: String!
    var items = "text"
    var isBuyer: String = ""
    var pageNo: Int = 2
    var yOffset: CGFloat = 0.0
    var NoOfContent: Array<ChatListDTO> = Array<ChatListDTO>()
        //var buyer: String = ""
    var chatid: Int!
    var sendMsgFromBuyer = false
    var isFromNotification = false
    var products: BuyerProductDTO = BuyerProductDTO() //OrdersProductDTO
    var buyerDTO : BuyerDTO = BuyerDTO()
    var buyer_name : String = ""
    var id: Int = 0
    var isFromBuyer = false
    var isFromSeller = false
    var isFormProducDetails = false
    var NEG_Type: String!
    var ChatID: Int!
    var buyername: String!
    var pre_transaction_accepted_by_buyer: Int=0
    var pre_transaction_accepted_by_seller: Int=0
    var negotiationStatus: Int!
    var sold_price: Int!
    @IBOutlet weak var viewForProductDetails: UIView!
    
    var chatListPaginateDTO : ChatListPaginateDTO = ChatListPaginateDTO()
    var productPaginateDTO: ProductPaginateDTO = ProductPaginateDTO()
    var productDTO: ProductDTO = ProductDTO()
    var chatInitiate: ChatInitiateVM = ChatInitiateVM();
    let chatNew: ChatNewMessageVM = ChatNewMessageVM()
    let chatList: ChatMessageListVM = ChatMessageListVM()
    var chatPaginateDTO: ChatInitiatePaginate = ChatInitiatePaginate()
     var newMessage: ChatListDTO = ChatListDTO()
    var startNegotiationDTO: StartNegotiationDTO = StartNegotiationDTO()
    var acceptNegotiationDTO: AcceptNegotiationDTO = AcceptNegotiationDTO()
    var newPrice = UILabel()
    var newPriceLbl = UILabel()
    let viewforBuyNow = UIView()
    let viewToNegotiatePrice = UIView()
     let blurrEffect = UIBlurEffect(style: UIBlurEffectStyle.light)
     let blurrEffectView = UIVisualEffectView()
    let gesture = UITapGestureRecognizer()
    var userData = UserProfileDTO()

    @IBOutlet weak var tableViewForChat: UITableView!
    let textFieldForPrice = UITextField()
    let myNotification = Notification.Name(rawValue:"MyNotification")
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelForOldPrice.isHidden = true
        textFieldForPrice.keyboardType = .numberPad
      tableViewForChat.delegate = self
        tableViewForChat.dataSource = self
        bckBtn.clipsToBounds = true
        textFieldForMessage.delegate = self
        negotiatedPriceLbl.isHidden = true
        negotiatedPrice.isHidden = true
        acceptBtn.isHidden = true
        declineBtn.isHidden = true
        self.tableViewForChat.estimatedRowHeight = 100.0;
        self.tableViewForChat.rowHeight = UITableViewAutomaticDimension
        self.tableViewForChat.setNeedsLayout()
        self.tableViewForChat.layoutIfNeeded()
        
//        Decline_Btn.setTitleColor(UIColor.red, for: .normal)
        let buyNow_Btn = UIButton(frame: CGRect(x: 4, y: 8, width: 118, height: 24))
        buyNow_Btn.setTitle("Buy Now", for: .normal)
        buyNow_Btn.setTitleColor(UIColor.white, for: .normal)
        buyNow_Btn.backgroundColor = NegotiateViewController.yellow
        buyNow_Btn.addTarget(self, action: #selector(buy_now), for: .touchUpInside)
        viewforBuyNow.frame = CGRect(x: 138 , y: 72, width: viewForProductDetails.width - 16, height: 40)
        tableViewForChat.backgroundView = UIImageView(image: UIImage(named: "top_bg"))
    if isFormProducDetails {
        self.chatList.chat_id = self.chatid
           }
    else{
        self.chatList.chat_id = self.buyerDTO.chat_id
            }
        self.NoOfContent.append(contentsOf: chatListPaginateDTO.data)
        UserService.getUserProfile(view: self){
            (completion: UserProfileDTO) in
            print(completion)
            self.userData = completion
            
            AccountService.chatMessageList(view: self, chatMessage: self.chatList){
            (completion: ChatListPaginateDTO) in
             print(completion)
            self.chatListPaginateDTO = completion
                self.getReloadData()
           //self.tableViewForChat.reloadData()
        }
    }
        UNUserNotificationCenter.current().requestAuthorization(options: [.badge,.alert,.sound], completionHandler: {didAllow, error in})
   
  if self.isFromNotification == true{
    
    self.viewForProductDetails.isHidden = false
         print("no one is present")
           UserService.getChatData(view: self, chatId: "\(ChatID!)", paginate: HomeScreenVM()){
            (completion: ChatInitiatePaginate) in
            print(completion)
            self.chatPaginateDTO = completion
    let url = NSURL(string: self.chatPaginateDTO.product.image)
    let request1: NSURLRequest = NSURLRequest(url: url as! URL)
            let mainQueue1 = OperationQueue.main
    NSURLConnection.sendAsynchronousRequest(request1 as URLRequest, queue: mainQueue1, completionHandler: { (response, data, error) -> Void in
       if error == nil {
              DispatchQueue.main.async(execute: {
        self.imageOfProduct.image = UIImage(data: data! as Data)})}
            })
        self.labelForProductName.text! = self.chatPaginateDTO.product.product_name
        self.labelForCategoryName.text! = self.chatPaginateDTO.product.category_name
        self.labelForPoints.text! = "\(self.chatPaginateDTO.product.product_points)"
        self.update_price_top.constant = 0
        self.update_price_height.constant = 0
            
    if self.NEG_Type == "CHAT_M" {
        
    if self.chatPaginateDTO.preTrans.pre_transaction_accepted_by_seller==0 && self.chatPaginateDTO.preTrans.pre_transaction_accepted_by_buyer == 1{
            self.viewForProductDetails.isHidden = false
            self.update_price.isHidden = true
            self.negotiatedPriceLbl.isHidden = false
            self.labelForOldPrice.isHidden = false
            self.labelForOldPrice.backgroundColor = UIColor.red
            self.negotiatedPrice.isHidden = false
           // self.negotiatedPriceLbl.text = "New Price"
            //self.viewForProductDetails.addSubview(self.newPriceLbl)
        self.negotiatedPriceLbl.isHidden = false
            self.negotiatedPrice.text = "\(self.buyerDTO.preTrans.pre_transaction_sold_price)"
            self.negotiatedPrice.isHidden = false
        //self.viewForProductDetails.addSubview(self.negotiatedPrice)
        
        }
   if self.chatPaginateDTO.preTrans.pre_transaction_accepted_by_buyer == 1 && self.chatPaginateDTO.preTrans.pre_transaction_accepted_by_seller == 1{
            self.viewForProductDetails.isHidden = false
            self.update_price_top.constant = 0
            self.update_price_height.constant = 0
            self.viewForProductDetails.addSubview(self.viewforBuyNow)
            self.viewforBuyNow.addSubview(buyNow_Btn)
        }
        self.viewForProductDetails.isHidden = false
       self.labelForProductName.text! = self.chatPaginateDTO.product.product_name
       self.labelForCategoryName.text! = self.chatPaginateDTO.product.category_name
       self.labelForPoints.text! = "\(self.chatPaginateDTO.product.product_points)"
        self.update_price_top.constant = 0
        self.update_price_height.constant = 0
    }
    else if self.NEG_Type == "NEG_A"{
             self.negotiatedPriceLbl.isHidden = true
             self.negotiatedPrice.isHidden = true
            self.viewForProductDetails.isHidden = false
            self.update_price_top.constant = 0
            self.update_price_height.constant = 0
            self.viewForProductDetails.addSubview(self.viewforBuyNow)
            self.viewforBuyNow.addSubview(buyNow_Btn)

    }
    else if self.NEG_Type == "NEG_R"{
        
        self.labelForProductName.text! = self.chatPaginateDTO.product.product_name
        self.labelForCategoryName.text! = self.chatPaginateDTO.product.category_name
        self.labelForPoints.text! = "\(self.chatPaginateDTO.product.product_points)"
        
    }
    else if self.NEG_Type == "NEG_S"{
        
        self.viewForProductDetails.isHidden = false
        self.update_price.isHidden = true
        self.negotiatedPriceLbl.isHidden = false
        self.labelForOldPrice.isHidden = false
        self.labelForOldPrice.backgroundColor = UIColor.red
        self.negotiatedPrice.isHidden = false
       // self.viewForProductDetails.addSubview(self.newPriceLbl)
        self.negotiatedPriceLbl.isHidden = false
        self.negotiatedPrice.font = UIFont.systemFont(ofSize: 14)
        self.negotiatedPrice.text = "\(self.chatPaginateDTO.preTrans.pre_transaction_sold_price)"
       // self.viewForProductDetails.addSubview(self.negotiatedPrice)
        self.negotiatedPrice.isHidden = false
        
    }
        }
   }
    
else{
    
    if isFromBuyer == true {
    
        print("it is buyer")
        print(self.presentingViewController ?? "sdfgh",self.parent ?? "sdfgh")
        
    if isFormProducDetails
       {
        
        self.labelForProductName.text! = self.chatPaginateDTO.product.product_name
        self.labelForCategoryName.text! = self.chatPaginateDTO.product.category_name
        self.labelForPoints.text! = "\(self.chatPaginateDTO.product.product_points)"
        viewForProductDetails.isHidden = false
    let url = NSURL(string: self.productDTO.image)
    let request1: NSURLRequest = NSURLRequest(url: url as! URL)
    let mainQueue1 = OperationQueue.main
    NSURLConnection.sendAsynchronousRequest(request1 as URLRequest, queue: mainQueue1, completionHandler: { (response, data, error) -> Void in
    if error == nil {
                    DispatchQueue.main.async(execute: {
                        self.imageOfProduct.image = UIImage(data: data! as Data)})}
            })
        print("all list from chat is here...\(chatPaginateDTO)")
        
            self.pre_transaction_accepted_by_buyer = self.chatPaginateDTO.preTrans.pre_transaction_accepted_by_buyer
            self.pre_transaction_accepted_by_seller = self.chatPaginateDTO.preTrans.pre_transaction_accepted_by_seller
            if (UserDefaults.standard.object(forKey: AppDelegate.NegotiationType))  != nil {
                update_price_top.constant = 0
                update_price_height.constant = 0
            }
            var chatByNotificationDTO: ChatByNotificationDTO = ChatByNotificationDTO()
    if (UserDefaults.standard.object(forKey: AppDelegate.messagingId)) != nil{
                UserService.getChatMessageIdByNotification(view: self, messageId: UserDefaults.standard.object(forKey: AppDelegate.messagingId) as! String, paginate: HomeScreenVM()){
                    (completion: ChatByNotificationDTO) in
                    chatByNotificationDTO = completion
                    print(chatByNotificationDTO)
                }
            }
        
    if self.chatPaginateDTO.preTrans.pre_transaction_accepted_by_buyer==1 && self.chatPaginateDTO.preTrans.pre_transaction_accepted_by_seller==1{
                print("there seller is 1 and buyer is also 1")
        viewForProductDetails.isHidden = false
                update_price_top.constant = 0
                update_price_height.constant = 0
                viewForProductDetails.addSubview(viewforBuyNow)
                viewforBuyNow.addSubview(buyNow_Btn)
        }
     if self.chatPaginateDTO.preTrans.pre_transaction_accepted_by_buyer == 1&&self.chatPaginateDTO.preTrans.pre_transaction_accepted_by_seller == 0{
            print("pre transac is 1")
            viewForProductDetails.isHidden = false
            self.update_price.isHidden = true
           self.negotiatedPriceLbl.isHidden = false
            labelForOldPrice.isHidden = false
            labelForOldPrice.backgroundColor = UIColor.red
             self.negotiatedPrice.isHidden = false
//            negotiatedPriceLbl.text = "New Price"
//            negotiatedPriceLbl.font = UIFont.systemFont(ofSize: 14)
           viewForProductDetails.addSubview(negotiatedPriceLbl)
//            negotiatedPrice.font = UIFont.systemFont(ofSize: 14)
           negotiatedPrice.text = "\(self.buyerDTO.preTrans.pre_transaction_sold_price)"
//            viewForProductDetails.addSubview(negotiatedPrice)
               }
         }
   // }
    else {
        
        self.labelForProductName.text! = self.products.product_name
        self.labelForCategoryName.text! = self.products.category_name
        self.labelForPoints.text! = "\(self.products.product_points)"
       viewForProductDetails.isHidden = false
        let url = NSURL(string: self.buyerDTO.product.image)
        let request1: NSURLRequest = NSURLRequest(url: url as! URL)
        let mainQueue1 = OperationQueue.main
        NSURLConnection.sendAsynchronousRequest(request1 as URLRequest, queue: mainQueue1, completionHandler: { (response, data, error) -> Void in
            if error == nil {
                DispatchQueue.main.async(execute: {
                    self.imageOfProduct.image = UIImage(data: data! as Data)
                }
                )}
        })
               if self.buyerDTO.preTrans.pre_transaction_accepted_by_buyer==1 && self.buyerDTO.preTrans.pre_transaction_accepted_by_seller==1{
            print("there seller is 1 and buyer is also 1")
            update_price_top.constant = 0
            update_price_height.constant = 0
            
            viewForProductDetails.addSubview(viewforBuyNow)
            viewforBuyNow.addSubview(buyNow_Btn)
        }
        
        if self.buyerDTO.preTrans.pre_transaction_accepted_by_buyer == 1&&self.buyerDTO.preTrans.pre_transaction_accepted_by_seller == 0{
            print("pre transac is 1")
           // viewForProductDetails.isHidden = false
            //########
        self.update_price.isHidden = true
            self.negotiatedPriceLbl.isHidden = false
            labelForOldPrice.isHidden = false
            labelForOldPrice.backgroundColor = UIColor.red
            self.negotiatedPrice.isHidden = false
//        //*****
//            update_price_top.constant = 0
//            update_price_height.constant = 0
//            labelForOldPrice.isHidden = false
//
//            acceptBtn.isHidden = false
//            declineBtn.isHidden = false
            declineBtn.addTarget(self, action: #selector(priceRejected), for: .touchUpInside)
            acceptBtn.addTarget(self, action: #selector(priceAccepted), for: .touchUpInside)

//            negotiatedPriceLbl.text = "New Price"
//            negotiatedPriceLbl.font = UIFont.systemFont(ofSize: 14)
           // viewForProductDetails.addSubview(negotiatedPriceLbl)
           // negotiatedPrice.font = UIFont.systemFont(ofSize: 14)
            negotiatedPrice.text = "\(self.buyerDTO.preTrans.pre_transaction_sold_price)"
          //  viewForProductDetails.addSubview(negotiatedPrice)
        }
       
        }
    }
 else if isFromSeller == true {
        
    //viewForProductDetails.isHidden = false
    self.labelForProductName.text! = self.products.product_name
    self.labelForCategoryName.text! = self.products.category_name
      self.labelForPoints.text! = "\(self.products.product_points)"
       self.negotiatedPriceLbl.isHidden = false
     // viewForProductDetails.addSubview(negotiatedPriceLbl)
        negotiatedPrice.text = "\(self.buyerDTO.preTrans.pre_transaction_sold_price)"
        self.negotiatedPrice.isHidden = false
     // viewForProductDetails.addSubview(negotiatedPrice)
  if self.buyerDTO.preTrans.pre_transaction_accepted_by_seller==0&&self.buyerDTO.preTrans.pre_transaction_accepted_by_buyer==1{
    print("seller is present")
    update_price_top.constant = 0
    update_price_height.constant = 0
    labelForOldPrice.isHidden = false
    
    acceptBtn.isHidden = false
    declineBtn.isHidden = false
   // viewForProductDetails.addSubview(viewforBuyNow)
    
//    viewforBuyNow.addSubview(acceptBtn)
//            viewforBuyNow.addSubview(declineBtn)
            declineBtn.addTarget(self, action: #selector(priceRejected), for: .touchUpInside)
            acceptBtn.addTarget(self, action: #selector(priceAccepted), for: .touchUpInside)
        }
    if self.buyerDTO.preTrans.pre_transaction_accepted_by_seller==1&&self.buyerDTO.preTrans.pre_transaction_accepted_by_buyer==1{
        update_price_top.constant = 0
        update_price_height.constant = 0
            self.acceptBtn.isHidden = true
           self.declineBtn.isHidden = true
              }
        var chatByNotificationDTO: ChatByNotificationDTO = ChatByNotificationDTO()
        if (UserDefaults.standard.object(forKey: AppDelegate.messagingId)) != nil{
            UserService.getChatMessageIdByNotification(view: self, messageId: UserDefaults.standard.object(forKey: AppDelegate.messagingId) as! String, paginate: HomeScreenVM()){
                (completion: ChatByNotificationDTO) in
                chatByNotificationDTO = completion
                print(chatByNotificationDTO)
            }
        }
     }
  }
}
    func buy_now(){
    let vc = storyboard?.instantiateViewController(withIdentifier: "ShippingViewController") as! ShippingViewController
     vc.chatList = self.chatList
    present(vc, animated: true, completion: nil)
                    }

 func priceAccepted(){
    
    let acceptNegotiation: AcceptNegotiationVM = AcceptNegotiationVM()
        if isFormProducDetails{
        acceptNegotiation.chatId = self.chatPaginateDTO.chat_id
        acceptNegotiation.negotiationStatus = self.chatPaginateDTO.negotiationStatus
        }
        else{
            acceptNegotiation.chatId = self.buyerDTO.chat_id
            acceptNegotiation.negotiationStatus = self.buyerDTO.negotiationStatus
        }
    AccountService.accept_Negotiation(view: self, address: acceptNegotiation){ (completion: AcceptNegotiationDTO) in
                    self.acceptNegotiationDTO = completion
                    print(completion)
                }
    self.acceptBtn.isHidden = true
    self.declineBtn.isHidden = true
        
   }
    
    func priceRejected()
        {
            acceptBtn.isHidden = true
            declineBtn.isHidden = true
    }
        @IBAction func actionOnBckBtn(_ sender: Any) {
        
        self.dismiss(animated: true,completion: nil)
        
        if  bck == true {
            (UIApplication.shared.delegate as! AppDelegate).window?.windowLevel = UIWindowLevelAlert + 1
            (UIApplication.shared.delegate as! AppDelegate).window?.makeKeyAndVisible()
            openNewWindow = nil
        }else{
            self.dismiss(animated: true)
            }
    }
    
    @IBAction func Update_Price_Btn(_ sender: Any) {
        
        var logLevel: XCGLogger.Level = .verbose
        logLevel = .error
        log.outputLevel = logLevel
        print("there is some error:- \(log.outputLevel)")
        
        blurrEffectView.effect = blurrEffect
        self.blurrEffectView.alpha = 1
        blurrEffectView.frame = view.bounds
        blurrEffectView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        gesture.addTarget(self, action: #selector(dissmissB(_:)))
        // gesture.delegate = self
        blurrEffectView.isUserInteractionEnabled = true
        blurrEffectView.addGestureRecognizer(gesture)
        self.view.addSubview(blurrEffectView)//insertSubview(blurrEffectView, at: 2)
       // tableViewForChat.isHidden = true
        //addSubview(blurrEffectView)
        viewToNegotiatePrice.frame = CGRect(x: 16, y: 156, width: Int(viewForProductDetails.frame.width) - 32, height: 90)
        viewToNegotiatePrice.backgroundColor = UIColor.white
        viewToNegotiatePrice.borderWidth = 1
        viewToNegotiatePrice.borderColor = UIColor.white
        let labelForNegotiation = UILabel()
        labelForNegotiation.frame = CGRect(x: 0, y: 4, width: Int(viewToNegotiatePrice.width), height: 15)
        labelForNegotiation.text = " Add your Negotiated Price"
        labelForNegotiation.textColor = UIColor.black
        textFieldForPrice.frame = CGRect(x: 8, y: 24, width: viewToNegotiatePrice.width - 24, height: 15)
        textFieldForPrice.placeholder = "Enter Negotiate Price"
        //tableViewForChat.addSubview(viewToNegotiatePrice)
         self.blurrEffectView.contentView.insertSubview(labelForNegotiation, at: 3)
       // blurrEffectView.insertSubview(labelForNegotiation, at: 3)
        //addSubview(viewToNegotiatePrice)
        self.blurrEffectView.contentView.insertSubview(viewToNegotiatePrice, at: 3)//bringSubview(toFront: viewToNegotiatePrice)
        viewToNegotiatePrice.insertSubview(labelForNegotiation, at: 5)//addSubview(labelForNegotiation)
        viewToNegotiatePrice.insertSubview(textFieldForPrice, at: 5)//addSubview(textFieldForPrice)
        let dismissBtn = UIButton()
        let proceedBtn = UIButton()
        dismissBtn.frame = CGRect(x: viewToNegotiatePrice.width - 200, y: 48, width: 64, height: 15)
        proceedBtn.frame = CGRect(x: Int(viewToNegotiatePrice.width - 120), y: 48, width: 72, height: 15)
        dismissBtn.setTitle("Dismiss", for: .normal)
        proceedBtn.setTitle("Proceed", for: .normal)
        dismissBtn.setTitleColor(UIColor.red, for: .normal)
        proceedBtn.setTitleColor(NegotiateViewController.blue, for: .normal)
        viewToNegotiatePrice.insertSubview(dismissBtn, at: 6)//addSubview(dismissBtn)
        viewToNegotiatePrice.insertSubview(proceedBtn, at: 6)
      //  viewToNegotiatePrice.addSubview(proceedBtn)
        //dismissBtn.addTarget(self, action: #selector(getAction), for: .touchUpInside)
        proceedBtn.addTarget(self, action: #selector(proceedFurther), for: .touchUpInside)
    }
    
    func proceedFurther(){
        update_price_height.constant=0
        update_price_top.constant=0
        labelForOldPrice.isHidden = false
        labelForOldPrice.backgroundColor = UIColor.red
        viewToNegotiatePrice.removeFromSuperview()
        blurrEffectView.removeFromSuperview()
        negotiatedPriceLbl.isHidden = false
        viewForProductDetails.addSubview(negotiatedPriceLbl)
       negotiatedPrice.isHidden = false
        viewForProductDetails.addSubview(negotiatedPrice)
        negotiatedPrice.text = self.textFieldForPrice.text!
        let startNegotiation: StartNegotiationVM = StartNegotiationVM()
        if isFormProducDetails{
                startNegotiation.chat_id = self.chatid
             startNegotiation.sold_price = Int(self.textFieldForPrice.text!)!
        }
        else{
            startNegotiation.chat_id = self.buyerDTO.chat_id
            print(" price of negotiation.....\(self.textFieldForPrice.text!)" )
            startNegotiation.sold_price = Int(self.textFieldForPrice.text!)!
        }
            AccountService.start_Negotiation(view: self, address: startNegotiation){
                (completion: StartNegotiationDTO) in
                    self.startNegotiationDTO = completion
                    print(completion)
        }
        textFieldForPrice.text = ""
  }

    func dissmissB(_ tab:UITapGestureRecognizer){
        textFieldForPrice.text = ""
        tab.view?.removeFromSuperview()
    }
    
    @IBAction func sendActionBtn(_ sender: Any) {
        
        UserService.getUserProfile(view: self){
            (completion: UserProfileDTO) in
            print(completion)
        
        let chat = ChatListDTO()
         chat.message = self.textFieldForMessage.text!
         chat.senderPic = completion.thumbnail
            
        if self.sendMsgFromBuyer == true{
            self.chatNew.chat_id = self.buyerDTO.chat_id
            self.chatNew.message = chat.message
            
        }
        else{
            self.chatNew.chat_id = self.chatid
            self.chatNew.message = chat.message
                        
        }
            AccountService.chatNewMessage(view: self, chatNew: self.chatNew)
               {
                (completion: ChatListDTO) in
                self.newMessage = completion
                print(self.newMessage)
                self.chatListPaginateDTO.data.append(chat)
                self.getReloadData()
          }
        self.textFieldForMessage.text=""
        }
    }
    
    @IBAction func actionOnTextField(_ sender: Any) {
       
    }
    
    func getReloadData(){
        
        DispatchQueue.main.async {
            self.tableViewForChat.reloadData()
            
            self.moveToLastComment()
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension NegotiateViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        //return NoOfContent.count
        return chatListPaginateDTO.data.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
       // var cell: NegotiateViewCell!
        if  self.chatListPaginateDTO.data[indexPath.row].senderName !=  self.userData.name
        {
        
            print(self.chatListPaginateDTO.data[indexPath.row].senderName)
            print(self.userData.name)
         let cell = tableView.dequeueReusableCell(withIdentifier: "cellForBuyer", for: indexPath) as! NegotiateViewCell
            
        cell.backgroundView?.backgroundColor = .clear
        cell.selectionStyle = .none
          cell.styling()
            cell.updateView(text: self.chatListPaginateDTO.data[indexPath.row])
             return cell

        }
        else
        {
            print("buyer is available.....")
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellForSeller", for: indexPath) as! IncommingMsgViewCell
            cell.backgroundView?.backgroundColor = .clear
            cell.selectionStyle = .none
            cell.styling()
            cell.updateView(text: self.chatListPaginateDTO.data[indexPath.row])
            return cell
        }
  }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("selected row.....")
    }
    func moveToLastComment() {
        if self.tableViewForChat.contentSize.height > self.tableViewForChat.frame.height {
            
            let lastSectionIndex = self.tableViewForChat!.numberOfSections - 1
            
            let lastRowIndex = self.tableViewForChat!.numberOfRows(inSection: lastSectionIndex) - 1
            
            let pathToLastRow = NSIndexPath(row: lastRowIndex, section: lastSectionIndex)
            
            self.tableViewForChat?.scrollToRow(at: pathToLastRow as IndexPath, at: UITableViewScrollPosition.bottom, animated: true)
        }
    }
    
    func scrollViewShouldScrollToTop(_ scrollView: UIScrollView) -> Bool {
        if scrollView == self.tableViewForChat {
            return true
        }
        else {
            return false
        }
    }

//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        if scrollView.contentOffset.y == (scrollView.contentSize.height - scrollView.frame.size.height) {
//            pageNo = pageNo + 1
//            loadMoreData(pageNo: pageNo)
//            print("scroll to bottom")
//        }
//        if scrollView.contentOffset.y == yOffset {
//            print("scroll to top ")
//            pageNo = pageNo - 1
//            print("page no.....\(pageNo)")
//            loadMoreData(pageNo: pageNo)
//        }
//
//    }
//
//    func loadMoreData(pageNo: Int){
//        if isFormProducDetails{
//
//        self.chatList.chat_id = self.chatid
//        }
//        else{
//            self.chatList.chat_id = self.buyerDTO.chat_id
//        }
//        AccountService.getChatByPagination(view: self, pageNo: "\(pageNo)", address: chatList){
//            (completion: ChatListPaginateDTO) in
//            self.chatListPaginateDTO = completion
//            print("pagination.....::::\(self.chatListPaginateDTO)")
////            self.chatListPaginateDTO.data.append(contentsOf: self.chatListPaginateDTO.data)
//            self.tableViewForChat.reloadData()
//         }
//    }
}
extension NegotiateViewController: UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
   func textFieldShouldClear(_ textField: UITextField) -> Bool {
    
    textField.resignFirstResponder()
       return true
    }
}
