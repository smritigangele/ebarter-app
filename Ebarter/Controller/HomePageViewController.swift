//
//  HomePageViewController.swift
//  Ebarter
//
//  Created by Hocrox Infotech Pvt Ltd1 on 22/03/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import UIKit
import Material

class HomePageViewController: UIViewController {

    var homeScreenPaginateDTO: HomeScreenPaginateDTO = HomeScreenPaginateDTO()
    var deviceRegisterDTO: DeviceRegisterDTO = DeviceRegisterDTO()
    @IBOutlet weak var collectionView: UICollectionView!
    var username: LoginVM = LoginVM()
    var username1: SignupVM = SignupVM()
    var username2 :String!
    //var barController: ToolbarController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareToolbar()
        
        let delegate = (UIApplication.shared.delegate as! AppDelegate)
        let appNavigation = delegate.appNavigationDrawerController
         self.navigationDrawerController?.isLeftViewEnabled = true
        //appNavigation!.isLeftViewEnabled = true
        
       // username2 = username
        print("print username::\(username.username)")
       // print(UserDefaults.standard.string(forKey: "username"))
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        let width = UIScreen.main.bounds.width
        let height = UIScreen.main.bounds.height
        layout.sectionInset = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        layout.itemSize = CGSize(width: (width / 3) - 14 , height: height / 5)
        collectionView.collectionViewLayout = layout
        collectionView.delegate=self
        collectionView.dataSource=self
        
     get_URL()
        
      // registerDevice()
    }
    
    func registerDevice(){
        
    let uuid = UIDevice.current.identifierForVendor?.uuidString
            print(uuid ?? "device type::::::")
            let  deviceName = UIDevice.current.name
            let deviceRegister: DeviceRegisterVM = DeviceRegisterVM()
            deviceRegister.device_name = deviceName
            deviceRegister.device_type = "IOS"
            deviceRegister.messaging_id = deviceTokenApp
    AccountService.deviceRegister(view: self, address: deviceRegister){
            (completion: DeviceRegisterDTO) in
            print(completion)
    self.deviceRegisterDTO = completion
        
            }

    }
     func get_URL()
    {
         Service.loadTokenOnAppLoad()
    UserService.getAllImages(view: self, paginate: HomeScreenVM()){
        (completion: HomeScreenPaginateDTO) in
                    print(completion)
        self.homeScreenPaginateDTO = completion
          DispatchQueue.main.async {
               self.collectionView.reloadData()
                    }
            }
    }
    fileprivate func prepareToolbar() {
        
        guard let tc = toolbarController as! AppToolbarController! else {
            return
        }
        tc.prepareMenuButton()
        tc.toolbar.title = "Ebarter"
        tc.toolbar.titleLabel.textColor = .white
      tc.toggleMenus(left: true, right: false)
       
    }
    @IBAction func Btn(_ sender: Any) {
        dismiss(animated: true,completion: nil)
        print("will the system hangs....")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
extension HomePageViewController: UICollectionViewDelegate,UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print("..........")
      return homeScreenPaginateDTO.data.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        print("cell for row.....")
        let cell: HomePageViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! HomePageViewCell
        
        
        let url = NSURL(string: homeScreenPaginateDTO.data[indexPath.row].category_image)
        
        let request: NSURLRequest = NSURLRequest(url: url as! URL)
        let mainQueue = OperationQueue.main
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: mainQueue, completionHandler: { (response, data, error) -> Void in
            if error == nil {
                DispatchQueue.main.async(execute: {
                        cell.imageForCell?.image = UIImage(data: data! as Data)
                    })
                }
            })
        cell.imageNameLabel.text = homeScreenPaginateDTO.data[indexPath.row].category_name

        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        print("did select.....")
        let vc = storyboard?.instantiateViewController(withIdentifier: "ProductsViewController") as! ProductsViewController
       vc.catId = String(self.homeScreenPaginateDTO.data[indexPath.row].id)
        //vc.username = self.username
        //(self.navigationDrawerController?.rootViewController as? ToolbarController)?.transition(to: vc)
        present(vc, animated: true, completion: nil)
    }
}




