//
//  ProductsViewController.swift
//  Ebarter
//
//  Created by Hocrox Infotech Pvt Ltd1 on 27/03/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import UIKit

class ProductsViewController: UIViewController {
    

    @IBOutlet var viewForLocality: UIView!
    
    @IBOutlet weak var textFieldForSearch: UITextField!
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var BckBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    var blurrEffect = UIBlurEffect()
    var blurrEffectView = UIVisualEffectView()
    let gesture = UITapGestureRecognizer()
     var fields = ["Search around my locality","Search by city"]
    var username: String!
    var count: Int!
    var productPaginateDTO: ProductPaginateDTO = ProductPaginateDTO()
    var productDetailsDTO: ProductDetailsDTO = ProductDetailsDTO()
    var catId:String!
    var selectedData = [String]()
    var arry = [String]()
    var screenSize: CGRect!
    var width: CGFloat!
    var height: CGFloat!
    var ButtonReleased: Bool = false 
    var tap = false
    var textFromLocality = ""
    var result = SearchByNameResDTO()
    
    
     //let productPaginateDTO: ProductPaginateDTO = ProductPaginateDTO()
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        collectionView.delegate=self
        collectionView.dataSource=self
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        screenSize = UIScreen.main.bounds
        width = screenSize.width
        height = screenSize.height
        layout.sectionInset = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 10)
        layout.itemSize = CGSize(width: (width / 3) - 22, height: height / 5)
        collectionView.collectionViewLayout = layout
        textFieldForSearch.delegate = self
        //searchData = [homeScreenPaginateDTO.data[0].categoryName]
        self.collectionView.reloadData()
                self.viewForLocality.frame = CGRect(x: 8 , y: UIScreen.main.bounds.height - 0, width: self.viewForLocality.frame.width, height: 156)
                view.addSubview(viewForLocality)
          self.view.bringSubview(toFront: viewForLocality)
        //textFieldForSearch.addTarget(self, action: #selector(textFieldDidChange), for: .touchUpInside)
      //  viewForLocality.height = 1
               get_URL()
     //   blurrEffectView.addSubview(viewForLocality)
    }
    func get_URL()
    {
        //Service.loadTokenOnAppLoad()
        UserService.getProductsList(view: self, id:catId!,paginate: HomeScreenVM()){
            (completion: ProductPaginateDTO) in
            //print(completion)
            self.productPaginateDTO = completion
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
        
    }
    
    @IBAction func bckAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func searchBtn(_ sender: Any) {
        
        UserService.searchByName(view: self, name: textFieldForSearch.text!){
            (completion: SearchByNameResDTO) in
            print(completion)
            self.result = completion
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
        //textFieldForSearch.text! = "\(productPaginateDTO.data[0].points)"
        
    }
    
    @IBAction func reloadBtn(_ sender: Any) {
        
        UserService.getProductsList(view: self, id:catId!,paginate: HomeScreenVM()){
            (completion: ProductPaginateDTO) in
            //print(completion)
            self.productPaginateDTO = completion
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
            UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseOut, animations: {()-> Void in
                
                self.viewForLocality.frame = CGRect(x: 8 , y: UIScreen.main.bounds.height - 0, width: self.view.frame.width - 16, height: 172)
            }, completion: {(_ finished:Bool)-> Void in })

        }
    }
    
    @IBAction func searchField(_ sender: Any) {
        
        selectedData = ["\(productPaginateDTO.data[0].points)"]
        arry = selectedData.filter({$0.lowercased().range(of: (sender as! UITextField).text!) != nil})
        print(arry)
        //collectionView.reloadData()

    }
    
    @IBAction func filter_Location_Btn(_ sender: Any) {
        
//        self.viewForLocality.frame = CGRect(x: 8 , y: UIScreen.main.bounds.height - 0, width: self.viewForLocality.frame.width, height: 156)
//        view.addSubview(viewForLocality)
      //  self.view.bringSubview(toFront: viewForLocality)
        count = nil
        tableView.reloadData()
        if(ButtonReleased == false)
        {
           ButtonReleased = true
           // count = 0
            UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseOut, animations: {()-> Void in
                
                self.viewForLocality.frame = CGRect(x: 8 , y: UIScreen.main.bounds.height - 120, width: self.view.frame.width - 16, height: 120)
                
                
            }, completion: {(_ finished:Bool)-> Void in })
           
            view.addSubview(viewForLocality)
            self.view.bringSubview(toFront: viewForLocality)
        }
        else {
            
            ButtonReleased = false
            UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseOut, animations: {()-> Void in
            
                self.viewForLocality.frame = CGRect(x: 8 , y: UIScreen.main.bounds.height - 0, width: self.view.frame.width - 16, height: 120)
            }, completion: {(_ finished:Bool)-> Void in })
                   }
        //view.removeFromSuperview()
       // self.view.bringSubview(toFront: viewForLocality)

    }
    }
extension ProductsViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if result.data.count != 0{
            return result.data.count
        }
        else{
        return productPaginateDTO.data.count
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
         let cell: HomePageViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! HomePageViewCell
        
        if result.data.count != 0{
            cell.layer.borderWidth = 0.5
            cell.frame.size.width = (width / 3) - 10
            cell.frame.size.height = (height / 5)
            let url = NSURL(string: result.data[indexPath.row].image)
            let request: NSURLRequest = NSURLRequest(url: url as! URL)
            let mainQueue = OperationQueue.main
            NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: mainQueue, completionHandler: { (response, data, error) -> Void in
                if error == nil {
                    DispatchQueue.main.async(execute: {
                        cell.imageForProducts?.image = UIImage(data: data! as Data)
                    })
                }
            })
            cell.imageNameForProducts.text = "\(result.data[indexPath.row].product_points)"
        }
        else{
        
        var textname: String!
        if textFieldForSearch.isFirstResponder{


        }
        cell.layer.borderWidth = 0.5
        cell.frame.size.width = (width / 3) - 10
        cell.frame.size.height = (height / 5) 
        let url = NSURL(string: productPaginateDTO.data[indexPath.row].image)
        let request: NSURLRequest = NSURLRequest(url: url as! URL)
        let mainQueue = OperationQueue.main
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: mainQueue, completionHandler: { (response, data, error) -> Void in
            if error == nil {
                DispatchQueue.main.async(execute: {
                    cell.imageForProducts?.image = UIImage(data: data! as Data)
                })
            }
        })
        cell.imageNameForProducts.text = productPaginateDTO.data[indexPath.row].points
        }
        return cell

    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "Product_DetailViewController") as! Product_DetailViewController
        vc.productDTO = productPaginateDTO.data[indexPath.row]
        //(self.navigationDrawerController?.rootViewController as? ToolbarController)?.transition(to: vc)
        vc.username = self.username
        present(vc, animated: true, completion: nil)
    }
    func dissmissB(_ tab:UITapGestureRecognizer){
    tab.view?.removeFromSuperview()
    
    
    }
}
extension ProductsViewController: UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textFieldForSearch.isFirstResponder{
            arry = [productPaginateDTO.data[0].categoryName]
            print(arry)
            selectedData = arry.filter({$0.range(of: textFieldForSearch.text!) != nil})
            print(selectedData)
            self.collectionView.reloadData()
         }
    return true
   }
    
}
extension ProductsViewController: UITableViewDelegate,UITableViewDataSource{
func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
    if count != nil {
        return fields.count+1
    }
    else{
        return fields.count
    }
}
func  tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    
    if count != nil{
        
        if indexPath.row == count+1{
            
           let cell = tableView.dequeueReusableCell(withIdentifier: "cellForSearch", for: indexPath) as! LocationViewCell
            textFromLocality = cell.textFieldForLocality.text!
            print(textFromLocality)
            //return cell
            //  cell.textLabel?.text = fields[indexPath.row]
            cell.apply_Btn.addTarget(self, action: #selector(apply_Btn(_ :)), for: .touchUpInside)
            return cell

        } else{
            let cell: LocationViewCell! = tableView.dequeueReusableCell(withIdentifier: "cellForLocality", for: indexPath) as? LocationViewCell
//             textFromLocality = cell.textFieldForLocality.text!
//            print(textFromLocality)
            //    cell.textLabel?.text = fields[indexPath.row]
            if indexPath.row == count {
            cell.pointImage.image = #imageLiteral(resourceName: "checked")
            //textFromLocality = cell.textFieldForLocality.text!
                //cell.apply_Btn.addTarget(self, action: #selector(apply_Btn(_ :)), for: .touchUpInside)
                
        }
            else{
               cell.pointImage.image = #imageLiteral(resourceName: "unchecked")
            }
            return cell

    }
    
    }
    else{
        let cell: LocationViewCell! = tableView.dequeueReusableCell(withIdentifier: "cellForLocality", for: indexPath) as? LocationViewCell
        cell.labelForSearch?.text = fields[indexPath.row]
         cell.pointImage.image = #imageLiteral(resourceName: "unchecked")
         //textFromLocality = cell.textFieldForLocality.text!
        //cell.textFieldForSearch.text! =
        return cell
    }
    
    }
    
func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
    var selectedIndex: IndexPath = IndexPath(row: 1, section: 0)
    

        if count != nil {
            
            if indexPath.row == count {
            count = nil
            
            }else{
                if count < indexPath.row {
                    
                    count = indexPath.row - 1
                }else{
                  count = indexPath.row
                }
            }
        }
        else{
        count = indexPath.row
        }
    tableView.reloadData()
}
    
    
        func apply_Btn(_ sender: UIButton)
        {
            let index = tableView.indexPath(for: (sender.superview?.superview as! LocationViewCell))!.row
            
            switch index{
               
            case 1:
                print("entered in locality")
               
                //textFieldForSearch.isEditing = true
                print(textFromLocality)
                UserService.searchByDistance(view: self, parameter: textFromLocality){
                    (completion: ProductPaginateDTO) in
                    print(completion)
                    self.productPaginateDTO = completion
                    print(self.productPaginateDTO)
                    //self.textFieldForSearch.text! = ""
                }
            case 2:
                print("in current location")
//                textFieldForSearch.placeholder = "Enter the city"
                 UserService.searchByCityState(view: self, parameter: textFromLocality){
                    (completion: ProductDetailsDTO) in
                    print(completion)
                    self.productDetailsDTO = completion
                    //self.textFieldForSearch.text! = ""
                }
//            case 2:
//                print("in city")
//                UserService.searchByLatLong(view: self, parameter: UserDefaults.standard.string(forKey: AppDelegate.latitude)! as String, state: "&longitude=", newParameter: UserDefaults.standard.string(forKey: AppDelegate.longitude)! as String, distance: "&distance=",searchDist: textFieldForSearch.text!){
//                    (completion: ProductDetailsDTO)in
//                    self.productDetailsDTO = completion
//                    //self.textFieldForSearch.text! = ""
//                }
            default:
                print("nothing is there.....")
            }
            self.tableView.reloadData()
        }
    
}


