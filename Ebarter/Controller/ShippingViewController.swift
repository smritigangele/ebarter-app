//
//  az cA
//  Ebarter
//
//  Created by Hocrox Infotech Pvt Ltd1 on 30/03/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import UIKit

class ShippingViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var product_id: [String] = []
    var addressPaginateDTO: AddressPaginateDTO = AddressPaginateDTO()
    var productDTO: ProductDTO = ProductDTO()
    var chatList: ChatMessageListVM = ChatMessageListVM()
    var chatid: Int!
    var isFromNegotiation: Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        get_URL()
    }
    
    func get_URL(){
        Service.loadTokenOnAppLoad()
        let address: AddressVM = AddressVM()
        UserService.getAllAddress(view: self, paginate: address){
            (completion: AddressPaginateDTO) in
            print(completion)
            self.addressPaginateDTO = completion
            print("product_id.....  \(self.product_id)")
          DispatchQueue.main.async {
                self.tableView.reloadData()
          }
       }
    }
    @IBAction func reloadBtn(_ sender: Any) {
        Service.loadTokenOnAppLoad()
        UserService.getAllAddress(view: self, paginate: AddressVM()){
            (completion: AddressPaginateDTO) in
            self.addressPaginateDTO = completion
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    @IBAction func bckBtn(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
extension ShippingViewController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return addressPaginateDTO.data.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ShippingAddressCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ShippingAddressCell
        cell.styling()
        cell.updateView(data: self.addressPaginateDTO.data[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "SingleAddressViewController") as! SingleAddressViewController
           vc.address_id = String(self.addressPaginateDTO.data[indexPath.row].address_id)
           vc.productDTO = self.productDTO
            vc.isFromNegotiation = isFromNegotiation
            vc.product_id = product_id
            vc.chatList = self.chatList
             vc.chatId = self.chatid
            present(vc, animated: true, completion: nil)
        }
    }
