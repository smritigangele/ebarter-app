//
//  EditProfileViewController.swift
//  Ebarter
//
//  Created by Hocrox Infotech Pvt Ltd1 on 23/03/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import UIKit
import Material
import GooglePlaces
import Alamofire
import Kingfisher

class EditProfileViewController: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    static let latitude = "user_latitude"
    static let longitude = "user_longitude"
    static let latitude1 = "latitude1"
    static let longitude1 = "longitude1"
    static let id  = "id"
    static let city = "city"
    static let image = "image"
    static let state = "state"
    static let country = "country"
    static let country1 = "country1"
    static let sentOn = "sentOn"
    var address: AddressVM = AddressVM()
    let imagePicker = UIImagePickerController()
    var userProfileDTO: UserProfileDTO = UserProfileDTO()
    var imageUrl: String!
    var deviceRegisterDTO = DeviceRegisterDTO()
    var profileImage = ""
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var descriptionTextField: UITextField!
    
    @IBOutlet weak var uploadImage: UIImageView!
    
    
    @IBOutlet weak var houseNoField: UITextField!
    
    @IBOutlet weak var mobileNoField: UITextField!
    
    @IBOutlet weak var colonyTextField: UITextField!
    @IBOutlet weak var pinTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var stateTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        imagePicker.delegate = self
        let tapImage = UITapGestureRecognizer(target: self, action: #selector(handleImage(_:)))
        uploadImage.addGestureRecognizer(tapImage)
        uploadImage.isUserInteractionEnabled = true
        uploadImage.layer.cornerRadius = 95/2
        uploadImage.clipsToBounds = true
        mobileNoField.delegate = self
        mobileNoField.keyboardType = UIKeyboardType.numberPad
        
       //uploadImage.setBackgroundImage(#imageLiteral(resourceName: "placeholderr"), for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        
        
    }
    
    func handleImage(_ gesture:UITapGestureRecognizer){
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
        
    }

    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            //            picker!.allowsEditing = false
            //            picker!.sourceType = UIImagePickerControllerSourceType.Camera
            //            picker!.cameraCaptureMode = .Photo
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = false
            imagePicker.cameraCaptureMode = .photo
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        
//        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
//            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
//            //imagePicker.allowsEditing = true
//            present(imagePicker, animated: true, completion: nil)
//        }
        
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    
        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage
                uploadImage.contentMode = .scaleToFill
                uploadImage.image = pickedImage
                print("picked image....\(uploadImage.image)")
//                 uploadImageBtn.setBackgroundImage(pickedImage, for: .normal)

            dismiss(animated: true, completion: nil)
    }
    
 func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func actionOnBckBtn(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func localityTextField(_ sender: Any) {
        
        let autoCompleteController = GMSAutocompleteViewController()
        autoCompleteController.delegate = self
        self.present(autoCompleteController, animated: true, completion: nil)
    }
    
    func updloadImage(_ image: String){
        
        if self.uploadImage.image == #imageLiteral(resourceName: "placeholder") {
            self.createAlert(title: "", body: "please upload image")
            return
        }
        
        else{
        let editProfile: EditProfileVM = EditProfileVM();
            
//            if houseNoField.text! == ""{
//                self.createAlert(title: "", body: "Please enter your house no.")
//                return
//            }
            if mobileNoField.text! == ""{
                
                self.createAlert(title: "", body: "Please enter your mobile no.")
                return
                
            }
//       if stateTextField.text! == ""{
//        
//        self.createAlert(title: "", body: "Please enter your state ")
//        return
//        
//        }
        editProfile.user_image = image
            
        editProfile.user_house_number = self.houseNoField.text!
        editProfile.user_mobile = self.mobileNoField.text!
        editProfile.user_pincode = self.pinTextField.text!
        editProfile.user_city = self.cityTextField.text!
        editProfile.user_state = self.stateTextField.text!
        editProfile.user_locality = self.colonyTextField.text!
        editProfile.user_description = self.descriptionTextField.text!
        editProfile.user_latitude = "\(UserDefaults.standard.double(forKey: EditProfileViewController.latitude1) as Double)"
        editProfile.user_longitude = "\(UserDefaults.standard.double(forKey: EditProfileViewController.longitude1)as Double)"
        editProfile.user_country = UserDefaults.standard.string(forKey: EditProfileViewController.country1)! as String
            
        AccountService.editProfile(view: self, editProfile: editProfile) {
            (completion: UserProfileDTO) in
            self.userProfileDTO = completion
            print(completion)
          
      self.register_Device()
            
    UserDefaults.standard.setValue(self.userProfileDTO.id, forKey: EditProfileViewController.id)
    UserDefaults.standard.synchronize()
    UserDefaults.standard.setValue(self.userProfileDTO.user_city, forKey: EditProfileViewController.city)
    UserDefaults.standard.synchronize()
    let x = UserDefaults.standard.object(forKey: EditProfileViewController.city) as! String
    print("city is \(x)")
            
    UserDefaults.standard.setValue(self.userProfileDTO.user_state, forKey: EditProfileViewController.state)
            UserDefaults.standard.synchronize()
            //            UserDefaults.standard.setValue(self.userProfileDTO.user_country, forKey: EditProfileViewController.country)
            //            UserDefaults.standard.synchronize()
    UserDefaults.standard.setValue(self.userProfileDTO.sentOn, forKey: EditProfileViewController.sentOn)
    UserDefaults.standard.synchronize()
    (UIApplication.shared.delegate as! AppDelegate).manageView()
            
        }
        }
    }
    
    func register_Device(){
        let uuid = UIDevice.current.identifierForVendor?.uuidString
        print(uuid ?? "device type::::::")
        let  deviceName = UIDevice.current.name
        let deviceRegister: DeviceRegisterVM = DeviceRegisterVM()
        deviceRegister.device_name = deviceName
        deviceRegister.messaging_id = deviceTokenApp
        deviceRegister.device_type = "IOS"
        AccountService.deviceRegister(view: self, address: deviceRegister){
            (completion: DeviceRegisterDTO) in
            print(completion)
            self.deviceRegisterDTO = completion
            
        }
    }

    
    
    @IBAction func editProfileBtn(_ sender: Any) {
        
        if self.uploadImage.image == #imageLiteral(resourceName: "placeholder"){
           
          updloadImage("")
        }
        
        else{
         Loader.startLoading()
        
    let imgData = UIImageJPEGRepresentation(uploadImage.image!, 0.2)!
        
                let parameters = ["image": "file.jpeg"]
                Alamofire.upload(multipartFormData: { multipartFormData in
                    multipartFormData.append(imgData, withName: "image",fileName: "file.jpg", mimeType: "image/jpg")
                    for (key, value) in parameters {
                        multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                    }
                },
                  to: Constants.uploadImage, method: .post, headers: Service.authHeader , encodingCompletion: {
                                    encodingResult in
                switch encodingResult {
        
                  case .success(let upload, _, _):
                    print("dxfcgvhbj")
                  upload.responseJSON { response in
                    print(response.request ?? "original url")  // original URL request
                    print(response.response ?? "url response ") // URL response
                    print(response.data ?? "server data")     // server data
                    print("uploaded successfully\(response.result)")   // result of response serialization
                     Loader.stopLoading()
                    
                    if String(describing: response.result) == "SUCCESS" {
                        print("this is token")
        
                        if let JSON = response.result.value {
        
                            print("JSON: \(JSON)")
                            let response = JSON as! NSDictionary
                            if ((response["image_name"] as? String) != nil) {
        
                                self.imageUrl = response["image_name"] as! String
                                print("image url.....\((self.imageUrl)! as String)")
//                                UserDefaults.standard.setValue((self.imageUrl), forKey: EditProfileViewController.image)
//                                UserDefaults.standard.synchronize()
                                self.updloadImage(self.imageUrl)
                            }else{
                                if (response["description"] as? String) == nil {
                                    self.createAlert(title: "Alert", body: ((response["fieldErrors"] as! NSArray)[0] as! NSDictionary)["message"] as! String)
        
                                }else{
                                    self.createAlert(title: "Alert", body: response["description"] as! String)
        
                                                        }
                                                    }
                                                }
                                            }
                    else{
                        
                        self.createAlert(title: "Alert", body: (response.error?.localizedDescription)!)
                        
                    }
                    }
                    
                case .failure(let encodingError):
                    print(encodingError)
                    }
                })
        }
            }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
extension EditProfileViewController: GMSAutocompleteViewControllerDelegate {
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        self.address = AddressVM()
        self.address.address = place.formattedAddress!
        self.address.latitude = place.coordinate.latitude
        self.address.longitude = place.coordinate.longitude
        self.address.placeId = place.placeID
        self.colonyTextField.text = place.formattedAddress!
        print("latitude.....\(self.address.latitude)")
        UserDefaults.standard.setValue(self.address.latitude, forKey: EditProfileViewController.latitude1)
        UserDefaults.standard.setValue(address.longitude, forKey: EditProfileViewController.longitude1)
        print("longitude.....\(self.address.longitude)")
        print("placeIID.....\(self.address.placeId)")
        for address: GMSAddressComponent in (place.addressComponents)! {
            if(address.type == "administrative_area_level_2") {
                self.address.city = address.name
                self.cityTextField.text = address.name
                
            }
            if(address.type == "administrative_area_level_1") {
                self.address.state = address.name
                self.stateTextField.text = address.name
            }
            if(address.type == "country"){
                    self.address.country = address.name
                    print("this is country name....\(address.name)")
                UserDefaults.standard.setValue(address.name, forKey: EditProfileViewController.country1)
                }
            if(address.type == "postal_code") {
                self.pinTextField.text = address.name
               // self.address.zipcode = address.name
            }
        print(address.type + " - " + address.name)
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
extension EditProfileViewController: UITextFieldDelegate{
    // Or in Swift 3:
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let invalidCharacters = CharacterSet(charactersIn: "0123456789").inverted
        return string.rangeOfCharacter(from: invalidCharacters, options: [], range: string.startIndex ..< string.endIndex) == nil
    }
    
}



