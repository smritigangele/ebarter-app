//
//  NegotiateViewCell.swift
//  
//
//  Created by Hocrox Infotech Pvt Ltd1 on 27/03/17.
//
//

import Foundation
import UIKit
import Material

class NegotiateViewCell: HTableViewCell
{
        
    @IBOutlet weak var imageOfSeller: UIImageView!
    
    @IBOutlet weak var messagFromSeller: UILabel!
    
//    @IBOutlet weak var imageOfBuyer: UIImageView!
//
//    @IBOutlet weak var messageFromBuyer: UILabel!
//

    @IBOutlet weak var viewForInfo: View!
    
    @IBOutlet weak var sendingMsgTime: UILabel!
    

    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        layoutMargins = UIEdgeInsets.zero
        separatorInset = UIEdgeInsets.zero
    }
    
    public func styling() {
        messagFromSeller.font = UIFont.appTextFont
      //  sendingMsg.textColor = .white
        
    }
    
    func updateView(text: ChatListDTO ) {
        messagFromSeller.text! = text.message
        viewForInfo.layer.cornerRadius = 3.0
        let url = NSURL(string: text.senderPic)
        let request: NSURLRequest = NSURLRequest(url: url as! URL)
        let mainQueue = OperationQueue.main
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: mainQueue, completionHandler: { (response, data, error) -> Void in
                        if error == nil {
                            DispatchQueue.main.async(execute: {
                            self.imageOfSeller.image = UIImage(data: data! as Data)
                            })
                        }
                    })
        let timeIntervalToday: TimeInterval = NSDate().timeIntervalSince1970
        let timeIntervalLastYear: TimeInterval = Double(text.sentOn)
        let now = NSDate(timeIntervalSince1970: timeIntervalToday)
        let then = NSDate(timeIntervalSince1970: timeIntervalLastYear)
        sendingMsgTime.text! = displayTimestamp(ts: timeIntervalToday)
       
    }
    
    func displayTimestamp(ts: Double) -> String {
        let date = NSDate(timeIntervalSince1970: ts)
        let formatter = DateFormatter()
        formatter.timeZone = NSTimeZone.system
        
        if NSCalendar.current.isDateInToday(date as Date) {
            formatter.dateStyle = .none
            formatter.timeStyle = .short
        } else {
            formatter.dateStyle = .short
            formatter.timeStyle = .none
        }
        
        return formatter.string(from: date as Date)
    }
    
}
