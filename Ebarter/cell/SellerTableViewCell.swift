//
//  SellerTableViewCell.swift
//  Ebarterr
//
//  Created by Hocrox Infotech Pvt Ltd1 on 06/04/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import UIKit
import Material

class SellerTableViewCell: HTableViewCell{
    
    @IBOutlet weak var action_Btn: UIButton!
    
    @IBOutlet weak var Product_name: UILabel!
    @IBOutlet weak var category_name: UILabel!
    
    @IBOutlet weak var buyer_name: UILabel!
    
    @IBOutlet weak var imageOfProduct: UIImageView!
    
    @IBOutlet weak var points_Lbl: UILabel!
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        layoutMargins = UIEdgeInsets.zero
        separatorInset = UIEdgeInsets.zero
    }
    
    public func styling() {
        Product_name.font = UIFont.appTextFont
         category_name.font = UIFont.appTextFont
         buyer_name.font = UIFont.appTextFont
         Product_name.font = UIFont.appTextFont
         //points_Lbl.font = UIFont.appTextFont
    }
    
 //BuyerDTO
    
    func updateView(text: BuyerDTO) {
        
        Product_name.text! = text.product.product_name
        category_name.text! = text.product.category_name
         buyer_name.text! = text.buyerName
        
        let dateTimeStamp = NSDate(timeIntervalSince1970:Double(text.product.sentOn)/1000)
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.local //Edit
        dateFormatter.dateFormat = "dd-MM-yyyy"
        dateFormatter.dateStyle = DateFormatter.Style.medium
        let strDateSelect = dateFormatter.string(from: dateTimeStamp as Date)
        print(strDateSelect)
        points_Lbl.text! = strDateSelect
        
        let url = NSURL(string: text.product.image)
        let request: NSURLRequest = NSURLRequest(url: url as! URL)
        let mainQueue = OperationQueue.main
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: mainQueue, completionHandler: { (response, data, error) -> Void in
            if error == nil {
                DispatchQueue.main.async(execute: {
                    self.imageOfProduct.image = UIImage(data: data! as Data)
                })
            }
        })
   
    }

//    func displayTimestamp(ts: Double) -> String {
//        let date = NSDate(timeIntervalSince1970: ts)
//        let formatter = DateFormatter()
//        formatter.timeZone = NSTimeZone.system
//
//        if NSCalendar.current.isDateInToday(date as Date) {
//            formatter.dateStyle = .none
//            formatter.timeStyle = .short
//        } else {
//            formatter.dateStyle = .short
//            formatter.timeStyle = .none
//        }
//
//        return formatter.string(from: date as Date)
//    }
//


}
