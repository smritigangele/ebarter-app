//
//  IncommingMsgViewCell.swift
//  Ebarter
//
//  Created by Hocrox Infotech Pvt Ltd1 on 10/10/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import UIKit
import Material

class IncommingMsgViewCell: HTableViewCell
{
    @IBOutlet weak var imageOfBuyer: UIImageView!

    @IBOutlet weak var messageFromBuyer: UILabel!


    @IBOutlet weak var viewForInfo: View!

   @IBOutlet weak var IncomingMsgTime: UILabel!



required init(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    layoutMargins = UIEdgeInsets.zero
    separatorInset = UIEdgeInsets.zero
}

public func styling() {
    messageFromBuyer.font = UIFont.appTextFont
    //  sendingMsg.textColor = .white
    
}

func updateView(text: ChatListDTO ) {
    messageFromBuyer.text! = text.message
    viewForInfo.layer.cornerRadius = 3.0
    if text.senderPic == nil{
        imageOfBuyer.image = #imageLiteral(resourceName: "placeholder")
    }
    else{
        let url = URL(string: text.senderPic)
        imageOfBuyer.kf.setImage(with: url)
    }
    let timeIntervalToday: TimeInterval = NSDate().timeIntervalSince1970
    let timeIntervalLastYear: TimeInterval = Double(text.sentOn)
    let now = NSDate(timeIntervalSince1970: timeIntervalToday)
    let then = NSDate(timeIntervalSince1970: timeIntervalLastYear)
    IncomingMsgTime.text! = displayTimestamp(ts: timeIntervalToday)
    
}

func displayTimestamp(ts: Double) -> String {
    let date = NSDate(timeIntervalSince1970: ts)
    let formatter = DateFormatter()
    formatter.timeZone = NSTimeZone.system
    
    if NSCalendar.current.isDateInToday(date as Date) {
        formatter.dateStyle = .none
        formatter.timeStyle = .short
    } else {
        formatter.dateStyle = .short
        formatter.timeStyle = .none
    }
    
    return formatter.string(from: date as Date)
}

}
