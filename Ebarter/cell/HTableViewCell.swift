//
// Created by Bhawani Singh on 29/01/17.
// Copyright (c) 2017 Hocrox Infotech Pvt. Ltd. All rights reserved.
//

import Foundation
import Material

class HTableViewCell: TableViewCell {

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        layoutMargins = UIEdgeInsets.zero
        separatorInset = UIEdgeInsets.zero

    }


}
