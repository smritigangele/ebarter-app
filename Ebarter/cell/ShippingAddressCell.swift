//
//  ShippingAddressCell.swift
//  Ebarter
//
//  Created by Hocrox Infotech Pvt Ltd1 on 30/03/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import UIKit
import Material

class ShippingAddressCell: HTableViewCell
{
    
    @IBOutlet weak var addressDetailView: UIView!
    
    @IBOutlet weak var nameField: UILabel!
    
    
    @IBOutlet weak var mobileNoField: UILabel!
  
    @IBOutlet weak var addressField: UILabel!
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        layoutMargins = UIEdgeInsets.zero
        separatorInset = UIEdgeInsets.zero
    }
    
    public func styling() {
        
        
    }
    
    func updateView(data: AddressDTO) {
        
      nameField.text! = data.address_name
      mobileNoField.text! = data.address_mobile
      addressField.text! = data.address_permanent_address
    
    }
    
    
}
