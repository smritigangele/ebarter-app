//
//  MyProductViewCell.swift
//  Ebarterr
//
//  Created by Hocrox Infotech Pvt Ltd1 on 23/04/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import UIKit
import Material

class MyProductViewCell: HTableViewCell{
    
    @IBOutlet weak var imageOfProduct: UIImageView!
    
    @IBOutlet weak var product_name: UILabel!
    
    @IBOutlet weak var category_name: UILabel!
    @IBOutlet weak var points_Lbl: UILabel!
    
    @IBOutlet weak var Quantity_Lbl: UILabel!
    @IBOutlet weak var product_date: UILabel!
    
    @IBOutlet weak var edit_Btn: UIButton!
    
    @IBOutlet weak var delete_Btn: UIButton!
    
    
//    override func awakeFromNib() {
//        super.awakeFromNib()
//
//        sizeToFit()
//        layoutIfNeeded()
//    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        layoutMargins = UIEdgeInsets.zero
        separatorInset = UIEdgeInsets.zero
    }
//    public func styling() {
//
//
//    }
//    
    func updateView(data: GetAllProductsDTO) {
      
    let url = NSURL(string: data.image)
    
    let request: NSURLRequest = NSURLRequest(url: url as! URL)
    let mainQueue = OperationQueue.main
    NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: mainQueue, completionHandler: { (response, data, error) -> Void in
    if error == nil {
    DispatchQueue.main.async(execute: {
       self.imageOfProduct?.image = UIImage(data: data! as Data)
    })
    }
    })
    
    product_name.text = data.product_name
    category_name.text = data.category_name
    points_Lbl.text = data.product_points
    Quantity_Lbl.text = data.product_quantity
    let resultOfTimeStamp = data.sentOn
    
    let date = NSDate(timeIntervalSince1970: TimeInterval(resultOfTimeStamp))
    let dateFormatter = DateFormatter()
    dateFormatter.timeStyle = .short
    dateFormatter.doesRelativeDateFormatting = true
    let stringTimestampResponse = dateFormatter.string(from: date as Date)
    product_date.text = stringTimestampResponse
    
    
    }
    
    
}
