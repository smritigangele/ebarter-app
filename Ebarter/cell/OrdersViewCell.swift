//
//  OrdersViewCell.swift
//  Ebarterr
//
//  Created by Hocrox Infotech Pvt Ltd1 on 15/04/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import UIKit
import Material

class OrdersViewCell: HTableViewCell{
    
    @IBOutlet weak var imageOfProduct: UIImageView!
    @IBOutlet weak var points_Lbl: UILabel!
    @IBOutlet weak var product_name: UILabel!
    @IBOutlet weak var category_name: UILabel!
    
    @IBOutlet weak var quantityOfProduct: UILabel!
    
    // As a seller view controller:-
    
    @IBOutlet weak var imageOfSellerProduct: UIImageView!
    @IBOutlet weak var product_Of_Seller: UILabel!
    
    @IBOutlet weak var categoryOfProduct: UILabel!
    
    @IBOutlet weak var productQuantity: UILabel!
  
    @IBOutlet weak var product_Points: UILabel!
    
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        layoutMargins = UIEdgeInsets.zero
        separatorInset = UIEdgeInsets.zero
    }
    public func styling() {

        
    }
    //
    func updateView(data: TransactionDTO) {
        let url = NSURL(string: data.product.thumbnail)
        let request: NSURLRequest = NSURLRequest(url: url as! URL)
        let mainQueue = OperationQueue.main
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: mainQueue, completionHandler: { (response, data, error) -> Void in
            if error == nil {
                DispatchQueue.main.async(execute: {
                    self.imageOfProduct.image = UIImage(data: data! as Data)
                })
            }
        })
       
        product_name.text! = data.product.product_name
        
        let dateTimeStamp = NSDate(timeIntervalSince1970:Double(data.product.sentOn)/1000)  //UTC time
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.local //Edit
        dateFormatter.dateFormat = "dd-MM-yyyy"
        dateFormatter.dateStyle = DateFormatter.Style.medium
        let strDateSelect = dateFormatter.string(from: dateTimeStamp as Date)
        print(strDateSelect) //Local time
        
        points_Lbl.text! = strDateSelect
        category_name.text! = data.product.category_name
        quantityOfProduct.text! = "\(data.product.product_points)"
    }
    //TransactionDTO
    func updateSellerView(data: TransactionDTO) {
                let url = NSURL(string: data.product.thumbnail)
                let request: NSURLRequest = NSURLRequest(url: url as! URL)
                let mainQueue = OperationQueue.main
                NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: mainQueue, completionHandler: { (response, data, error) -> Void in
                    if error == nil {
                        DispatchQueue.main.async(execute: {
                         self.imageOfSellerProduct.image = UIImage(data: data! as Data)
                        })
                    }
                })
        
        product_Of_Seller.text! = data.product.product_name
       
        categoryOfProduct.text! = data.product.category_name
        
        let dateTimeStamp = NSDate(timeIntervalSince1970:Double(data.product.sentOn)/1000)  //UTC time
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.local //Edit
        dateFormatter.dateFormat = "dd-MM-yyyy"
        dateFormatter.dateStyle = DateFormatter.Style.medium
        let strDateSelect = dateFormatter.string(from: dateTimeStamp as Date)
        print(strDateSelect)
        
        product_Points.text! = strDateSelect
        productQuantity.text! = "\(data.product.product_points)"
        
    }
}

