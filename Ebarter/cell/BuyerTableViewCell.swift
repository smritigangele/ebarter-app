//
//  BuyerTableViewCell.swift
//  Ebarterr
//
//  Created by Hocrox Infotech Pvt Ltd1 on 06/04/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import UIKit
import Material

class BuyerTableViewCell: HTableViewCell {

    @IBOutlet weak var productDetailView: UIView!
    
    @IBOutlet weak var product_image: UIImageView!
   
    @IBOutlet weak var product_name: UILabel!
    @IBOutlet weak var date_label: UILabel!
    
    @IBOutlet weak var category_name: UILabel!
    
    @IBOutlet weak var buyer_name: UILabel!
    @IBOutlet weak var actionBtn: UIButton!
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        layoutMargins = UIEdgeInsets.zero
        separatorInset = UIEdgeInsets.zero
    }
    
    public func styling() {
        product_name.font = UIFont.appTextFont
        category_name.font = UIFont.appTextFont
        buyer_name.font = UIFont.appTextFont
        //date_label.font = UIFont.appTextFont
    }
    
    //BuyerDTO
    //TransactionDTO
    func updateView(text: BuyerDTO)
    {
        product_name.text! = text.product.product_name
        category_name.text! = text.product.category_name
        buyer_name.text! = text.product.seller_name
        //let resultOfTimeStamp = "\(text.product.sentOn)"
        
        let dateTimeStamp = NSDate(timeIntervalSince1970:Double(text.product.sentOn)/1000)  //UTC time
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.local //Edit
        dateFormatter.dateFormat = "dd-MM-yyyy"
        dateFormatter.dateStyle = DateFormatter.Style.medium
        let strDateSelect = dateFormatter.string(from: dateTimeStamp as Date)
        print(strDateSelect)
        date_label.text! = strDateSelect
        let url = NSURL(string: text.product.image)
        
        let request: NSURLRequest = NSURLRequest(url: url as! URL)
        let mainQueue = OperationQueue.main
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: mainQueue, completionHandler: { (response, data, error) -> Void in
            if error == nil {
                DispatchQueue.main.async(execute: {
                    self.product_image.image = UIImage(data: data! as Data)
                })
            }
        })
    }
    
//    func displayTimestamp(ts: Double) -> String {
//        let date = NSDate(timeIntervalSince1970: ts)
//        let formatter = DateFormatter()
//        formatter.timeZone = NSTimeZone.system
//
//        if NSCalendar.current.isDateInToday(date as Date) {
//            formatter.dateStyle = .none
//            formatter.timeStyle = .short
//        }
//        else {
//            formatter.dateStyle = .short
//            formatter.timeStyle = .none
//        }
//        return formatter.string(from: date as Date)
//    }
//
    
}
