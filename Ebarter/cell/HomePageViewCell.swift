//
//  HomePageViewCell.swift
//  Ebarter
//
//  Created by Hocrox Infotech Pvt Ltd1 on 22/03/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import UIKit

class HomePageViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var imageForCell: UIImageView!
    
    @IBOutlet weak var imageNameLabel: UILabel!
    
    
    @IBOutlet weak var imageForProducts: UIImageView!
    
    @IBOutlet weak var imageNameForProducts: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        sizeToFit()
        layoutIfNeeded()
    }
    
}
