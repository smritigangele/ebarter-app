//
//  AddPointsCell.swift
//  Ebarterr
//
//  Created by Hocrox Infotech Pvt Ltd1 on 13/04/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import UIKit
class AddPointsCell: HTableViewCell{
    
    @IBOutlet weak var imageOfProduct: UIImageView!
    
    @IBOutlet weak var leadingOfQuantityTag: NSLayoutConstraint!
    @IBOutlet weak var TagNameOfPoints: UILabel!
    @IBOutlet weak var product_name: UILabel!
    @IBOutlet weak var purchase_date: UILabel!
    @IBOutlet weak var points_lbl: UILabel!
    @IBOutlet weak var category_name: UILabel!
    @IBOutlet weak var quantityOfProduct: UILabel!
    @IBOutlet weak var details_Btn: UIButton!
    
    @IBOutlet weak var productQuantLeading: NSLayoutConstraint!
    @IBOutlet weak var productQuantityWidth: NSLayoutConstraint!
    @IBOutlet weak var QuantityTagWidth: NSLayoutConstraint!
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        layoutMargins = UIEdgeInsets.zero
        separatorInset = UIEdgeInsets.zero
    }
    
    public func styling() {
         product_name.font = UIFont.appTextFont
         purchase_date.font = UIFont.appTextFont
         points_lbl.font = UIFont.appTextFont
         category_name.font = UIFont.appTextFont
         quantityOfProduct.font = UIFont.appTextFont
        
        
    }
    
    func updateView(text: PointTransactionDTO) {
        let url = NSURL(string: text.product.thumbnail)
        
        if text.buyer.name == "Ebarter"{
        TagNameOfPoints.alpha = 0
        points_lbl.alpha = 0
        QuantityTagWidth.constant = 0
        productQuantityWidth.constant = 0
        productQuantLeading.constant = 0
            
        }
        else{
         points_lbl.text! = text.product.product_points
        }
        let request: NSURLRequest = NSURLRequest(url: url as! URL)
        let mainQueue = OperationQueue.main
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: mainQueue, completionHandler: { (response, data, error) -> Void in
            if error == nil {
                DispatchQueue.main.async(execute: {
               self.imageOfProduct.image = UIImage(data: data! as Data)
                })
            }
        })
    
        product_name.text! = text.product.product_name

        points_lbl.text! = text.product.product_points
        category_name.text! = text.product.category_name
        quantityOfProduct.text! = text.product.product_quantity

//        let string = text.dateOfTransaction.date
//        let dateFormatter = DateFormatter()
////        let tempLocale = dateFormatter.locale // save locale temporarily
////        dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
//        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
//        let date = dateFormatter.date(from: string)!
//        dateFormatter.dateFormat = "MMM d, yyyy" ; //"dd-MM-yyyy HH:mm:ss"
////        dateFormatter.locale = tempLocale // reset the locale --> but no need here
//        let dateString = dateFormatter.string(from: date)
//text.dateOfTransaction.date
//        let inFormatter = DateFormatter()
//        inFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
//        let date = inFormatter.date(from: text.dateOfTransaction.date)!
//        inFormatter.dateFormat = "dd-MMM HH:mm"
//        let outStr = inFormatter.string(from: date)
//        print(outStr) // -> 16-Aug 05:08
//        print("EXACT_DATE : \(outStr)")
//        purchase_date.text! = "\(outStr)"
        let serverDateFormatter: DateFormatter = {
            let result = DateFormatter()
            result.dateFormat = "yyyy-MM-dd HH:mm:ss.SSSSSS"
            result.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone!
            return result
        }()
        let s = text.dateOfTransaction.date//"2016-06-06 23:37:32.000000"//2017-11-10 06:51:50.000000
        let d = serverDateFormatter.date(from: s)!
        let localDateFormatter: DateFormatter = {
            let result = DateFormatter()
            result.dateStyle = .medium
//            result.timeStyle = .medium
            return result
        }()
        
        print(localDateFormatter.string(from: d))
         purchase_date.text! = "\(localDateFormatter.string(from: d))"
    }
    
    func displayTimestamp(ts: Double) -> String {
        let date = NSDate(timeIntervalSince1970: ts)
        let formatter = DateFormatter()
        formatter.timeZone = NSTimeZone.system
        
        if NSCalendar.current.isDateInToday(date as Date) {
            formatter.dateStyle = .none
            formatter.timeStyle = .short
        } else {
            formatter.dateStyle = .short
            formatter.timeStyle = .none
        }
        
        return formatter.string(from: date as Date)
    }
    

}
