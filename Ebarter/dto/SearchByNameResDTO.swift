//
//  SearchByNameResDTO.swift
//  Ebarter
//
//  Created by Hocrox Infotech Pvt Ltd1 on 09/01/18.
//  Copyright © 2018 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import EVReflection

class SearchByNameResDTO: EVObject{
    
    public var total: Int = 0
    public var perPage: Int = 0
    public var currentPage: Int = 0
    public var lastPage: Int = 0
    public var hasNext: Bool = false
    public var data: Array<SearchByNameData> = Array<SearchByNameData>()
    
}

