//
//  AcceptNegotiationDTO.swift
//  Ebarterr
//
//  Created by Hocrox Infotech Pvt Ltd1 on 09/05/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import EVReflection

class AcceptNegotiationDTO: EVObject{
    public var pre_transaction_id: Int = 0
    public var chat_id: Int = 0;
    public var buyer_id: Int = 0;
     public var buyer_name: String = ""
    public var pre_transaction_accepted_by_buyer: Int = 0
    public var pre_transaction_accepted_by_seller: Int = 0;
    public var pre_transaction_actual_price: Int = 0
    public var pre_transaction_sold_price: Int = 0
    public var product_id: Int = 0
    public var product_name: String = ""
    public var seller_id: Int = 0
    public var seller_name: String = ""
}
