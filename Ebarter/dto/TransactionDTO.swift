//
//  TransactionDTO.swift
//  Ebarterr
//
//  Created by Hocrox Infotech Pvt Ltd1 on 10/05/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import EVReflection

class TransactionDTO: EVObject{
    
    public var transaction_id: String = "";
    public var seller_id: String = "";
    public var sellerName:String = "";
    public var buyer_id: String = "";
    public var buyerName: String = "";
    public var product_id: String = "";
    public var productName: String = "";
    public var productPoints: String = "";
    public var status: String = "";
    public var sentOn: String = "";
    public var product = OrdersProductDTO()
}
