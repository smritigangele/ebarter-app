//
// Created by Bhawani Singh on 17/01/17.
// Copyright (c) 2017 Hocrox Infotech Pvt. Ltd. All rights reserved.
//

import Foundation

enum UserType {
    case CHEF
    case NORMAL
    case ADMIN
}
