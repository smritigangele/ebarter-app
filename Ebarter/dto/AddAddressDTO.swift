//
//  AddAddressDTO.swift
//  Ebarterr
//
//  Created by Hocrox Infotech Pvt Ltd1 on 11/05/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import EVReflection

class AddAddressDTO: EVObject{
    public var address_id: String = ""
    public var address_name: String = ""
    public var address_city: String = ""
    public var address_state: String = ""
    public var address_country: String = ""
    public var address_postalcode: UInt64 = 0
    public var address_mobile: UInt64 = 0
    public var address_permanent_address: String = ""
    public var userid: UInt64 = 0
    public var address_latitude: Double = 0.0
    public var address_longitude: Double = 0.0
    public var address_place_id: UInt64 = 0
    public var id: Int64 = 0
    public var name: String = ""
    public var username: String = ""
    public var email: String = ""
    public var sentOn: UInt64 = 0
}

