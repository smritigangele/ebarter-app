//
//  Buy_Without_NegotiationVM.swift
//  Ebarterr
//
//  Created by Hocrox Infotech Pvt Ltd1 on 11/05/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import UIKit
class Buy_Without_NegotiationVM{
    
    var product_id: Int = 0
    var address_id: Int = 0
    
    public func toJSON() -> Dictionary<String, String> {
        return [
            
            "product_id": "\(self.product_id)",
            "address_id": "\(self.address_id)"
            
        ]
    }
}
