//
//  SearchByNameData.swift
//  Ebarter
//
//  Created by Hocrox Infotech Pvt Ltd1 on 09/01/18.
//  Copyright © 2018 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import EVReflection

class SearchByNameData: EVObject{
    
    public var product_id = 0;
    public var product_name = "";
    public var image = "";
    public var thumbnail = "";
    public var product_points = 0;
    public var product_quantity = 0;
    public var product_description = "";
    public var seller_name = "";
    public var seller_id = 0;
    public var category_name = "";
    public var category_id = 0;
    public var sentOn = 0;
   
}
