//
//  ForgotPasswordUserDTO.swift
//  Ebarterr
//
//  Created by Hocrox Infotech Pvt Ltd1 on 10/05/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import EVReflection

class ForgotPasswordUserDTO: EVObject{
    
    public var id: Int = 0
    public var name: String = ""
    public var username: String = ""
    public var email: String = ""
    public var reset_tokens: String = ""
    public var created_at: String = ""
    public var updated_at: String = ""
    public var user_active: Int = 0
    
}
