//
//  DeleteProductResult.swift
//  Ebarterr
//
//  Created by Hocrox Infotech Pvt Ltd1 on 23/04/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import EVReflection
class DeleteProductResult: EVObject{
    public var data: DeleteProductDTO = DeleteProductDTO()
}
