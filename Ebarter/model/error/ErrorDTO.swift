//
// Created by Bhawani Singh on 11/01/17.
// Copyright (c) 2017 Hocrox Infotech Pvt. Ltd. All rights reserved.
//

import Foundation
import EVReflection


class ErrorDTO: EVObject {
    var message: String = "";
    var fieldErrors = [FieldErrorDTO]();
}
