//
//  BuyerProductDTO.swift
//  Ebarterr
//
//  Created by Hocrox Infotech Pvt Ltd1 on 06/04/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import EVReflection

class BuyerProductDTO: EVObject {
    public var product_id: Int = 0
    public var product_name: String = ""
    public var image: String = ""
    public var thumbnail: String = ""
    public var product_points: Int = 0
    public var product_description: String = ""
    public var seller_id: Int = 0
    public var seller_name: String = ""
    public var category_name: String = ""
    public var category_id: Int = 0
    public var sentOn: UInt64 = 0
}
