//
//  SellerDetailsDTO.swift
//  Ebarterr
//
//  Created by Hocrox Infotech Pvt Ltd1 on 14/04/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import EVReflection
class SellerDetailsDTO: EVObject{
    public var id: Int = 0
    public var name: String = ""
    public var username: String = ""
    public var email: String = ""
    public var image: String = ""
    public var thumbnail: String = ""
    public var description1: String = ""
    public var longitude: Double = 0.0
    public var latitude: Double = 0.0
    public var sentOn: UInt64 = 0
    
}
