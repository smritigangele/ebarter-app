//
//  ProfilePaginateDTO.swift
//  Ebarterr
//
//  Created by Hocrox Infotech Pvt Ltd1 on 05/04/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import EVReflection

class UserProfileDTO: EVObject
{
    public var id: Int = 0
    public var name: String = ""
    public var username: String = ""
    public var user_image: String = ""
    public var thumbnail: String = ""
    public var email: String = ""
    public var user_mobile: UInt64 = 0
    public var user_house_number: String = ""
    public var user_locality: String = ""
    public var user_city: String = ""
    public var user_state: String = ""
    public var user_country: String = ""
    public var user_pincode: Int = 0
    public var user_longitude: Double = 0
    public var user_latitude: Double = 0
    public var user_description: String = ""
    public var profileCreated: Bool = false
    public var sentOn: UInt64 = 0
    
}
