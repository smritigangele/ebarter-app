//
//  Buy_Via_NegotiationDTO.swift
//  Ebarterr
//
//  Created by Hocrox Infotech Pvt Ltd1 on 21/04/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import EVReflection

class Buy_Via_NegotiationDTO: EVObject{
    public var seller_id: String = ""
    public var buyer_id: String = ""
    public var product_id: String = ""
    public var product_price: String = ""
    public var buy_via_negotiation: String = "";
    public var transaction_status: String = "";
    public var transaction_payment_id: String = "";
    public var transaction_shipment_address_id: String = "" 
    public var updated_at: String = "";
    public var created_at: String = "";
    public var transaction_id: String = "";
    
}
