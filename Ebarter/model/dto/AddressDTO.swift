//
//  AddressDTO.swift
//  Ebarter
//
//  Created by Hocrox Infotech Pvt Ltd1 on 01/04/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import EVReflection

class AddressDTO: EVObject{
    public var address_id: Int = 0
    public var address_name: String = ""
    public var address_city: String = ""
    public var address_state: String = ""
    public var address_country: String = ""
    public var address_postalcode: String = ""
    public var address_mobile: String = ""
    public var address_permanent_address: String = ""
    public var userid: Int = 0
    public var address_latitude: UInt64 = 0
    public var address_longitude: UInt64 = 0
    public var address_place_id: String = ""
    public var id: Int = 0
    public var name: String = ""
    public var username: String = ""
    public var email: String = ""
    public var sentOn: UInt64 = 0
}
