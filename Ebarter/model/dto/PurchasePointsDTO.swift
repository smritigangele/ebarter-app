//
//  PurchasePointsDTO.swift
//  Ebarterr
//
//  Created by Hocrox Infotech Pvt Ltd1 on 13/04/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import EVReflection

class PurchasePointsDTO: EVObject{
    public var status: String = ""
    public var stripeToken: String = ""
    public var amount: String = ""
    public var points_id: String = ""
    public var points: String = ""
    public var user_id: String = ""
}
