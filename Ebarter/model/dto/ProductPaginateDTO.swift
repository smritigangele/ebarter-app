//
//  ProductPaginateDTO.swift
//  Ebarter
//
//  Created by Hocrox Infotech Pvt Ltd1 on 24/03/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import EVReflection
class ProductPaginateDTO: EVObject{
    public var data: Array<ProductDTO> = Array<ProductDTO>()
    public var total: Int = 0
    public var perPage: Int = 0
    public var currentPage: Int = 0
    public var lastPage: Int = 0
    public var hasNext: Bool = false
}
