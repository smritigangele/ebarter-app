//
//  ChatInitiatePaginate.swift
//  Ebarterr
//
//  Created by Hocrox Infotech Pvt Ltd1 on 12/04/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import EVReflection

class ChatInitiatePaginate: EVObject{
    public var chat_id: Int = 0
    public var seller: Int = 0
    public var sellerName: String = ""
    public var sellerPic: Int = 0
    public var buyer: Int = 0
    public var buyerName: String = ""
    public var buyerPic: String = ""
    public var product: ChatProductDTO = ChatProductDTO()
    public var preTrans: StartNegotiationDTO = StartNegotiationDTO()
    public var negotiationStatus: Int = 0
    public var negotiationRequested: Int = 0
    public var productBought: Int = 0
}
