//
//  PointTransactionDTO.swift
//  Ebarterr
//
//  Created by Hocrox Infotech Pvt Ltd1 on 14/04/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import EVReflection
class PointTransactionDTO: EVObject{
    public var points_transaction_id: Int = 0
    public var points: Int = 0
    public var totalPoints: UInt64 = 0
    public var seller: SellerDetailsDTO = SellerDetailsDTO()
    public var buyer: SellerDetailsDTO = SellerDetailsDTO()
    public var product: ProductDetailsDTO = ProductDetailsDTO()
    public var dateOfTransaction: DateTransactionDTO = DateTransactionDTO()
}
