//
//  DeviceRegisterDTO.swift
//  Ebarterr
//
//  Created by Hocrox Infotech Pvt Ltd1 on 18/04/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import EVReflection
class DeviceRegisterDTO: EVObject{
    
    public var id: Int = 0
    public var messaging_id: String = ""
    public var device_type: String = ""
    public var device_name: String = ""
    public var user_id: Int = 0
    public var username: String = ""
    public var sentOn: UInt64 = 0
}
