//
//  ChatByNotificationDTO.swift
//  Ebarterr
//
//  Created by Hocrox Infotech Pvt Ltd1 on 21/04/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import EVReflection

class ChatByNotificationDTO: EVObject{
    
    public var id: Int = 0
    public var chatId: Int = 0
    public var message: String = ""
    public var sender: String = ""
    public var senderName: String = ""
    public var senderPic: String = ""
    public var reciever: String = ""
    public var recieverName: String = ""
    public var recieverPic: String = ""
    public var sentOn: UInt64 = 0
    
    
}
