//
//  FrogotPasswordDTO.swift
//  Ebarterr
//
//  Created by Hocrox Infotech Pvt Ltd1 on 02/05/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import EVReflection

class ForgotPasswordDTO: EVObject{
    public var user: ForgotPasswordUserDTO = ForgotPasswordUserDTO()
}

