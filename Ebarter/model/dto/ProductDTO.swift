//
//  ProductDTO.swift
//  Ebarter
//
//  Created by Hocrox Infotech Pvt Ltd1 on 24/03/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import EVReflection

class ProductDTO: EVObject {
    
    public var product_id: Int = 0
    public var name: String = ""
    public var image: String = ""
    public var thumbnail: String = ""
    public var points: String = ""
    public var description1: String = ""
    public var sellerId: Int = 0
    public var sellerName: String = ""
    public var sellerEmail: String = ""
    public var sellerImage: String = ""
    public var sellerMobile: String = ""
    public var categoryName: String = ""
    public var sentOn: UInt64 = 0
}

