//
//  DateTransactionDTO.swift
//  Ebarterr
//
//  Created by Hocrox Infotech Pvt Ltd1 on 14/04/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import EVReflection

class DateTransactionDTO: EVObject{
    
    public var date : String = ""
    public var timezone_type: String = ""
    public var timezone : String = ""
    
}

