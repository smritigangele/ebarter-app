//
//  ChatListPaginateDTO.swift
//  Ebarterr
//
//  Created by Hocrox Infotech Pvt Ltd1 on 11/04/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import EVReflection

class ChatListPaginateDTO: EVObject{
    public var total: Int = 0
    public var perPage: Int = 0
    public var currentPage: Int = 0
    public var lastPage: Int = 0
    public var hasNext: Bool = false
    public var data: Array<ChatListDTO> = Array<ChatListDTO>()
}
