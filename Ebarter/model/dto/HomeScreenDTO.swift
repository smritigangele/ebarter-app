//
//  homeScreenDTO.swift
//  Ebarter
//
//  Created by Hocrox Infotech Pvt Ltd1 on 22/03/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import EVReflection

class HomeScreenDTO: EVObject{
    public var id: Int = 0
    public var category_name: String = ""
    public var category_image: String = ""
    public var category_thumbnail: String = ""
    public var slug: String = ""
    public var category_Parent_id: String = ""
    public var sentOn: UInt64 = 0
    
}
