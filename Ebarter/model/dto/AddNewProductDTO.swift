//
//  AddNewProductDTO.swift
//  Ebarterr
//
//  Created by Hocrox Infotech Pvt Ltd1 on 23/04/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import EVReflection
class AddNewProductDTO:EVObject{
   
    public var product_id: Int = 0
    public var product_name: String = ""
    public var image: String = ""
    public var product_points: Int64 = 0
    public var category_id: Int = 0
    public var thumbnail: String = ""
    public var product_quantity: Int = 0
    public var product_description: String = ""
    public var seller_name: String = ""
    public var seller_id: String = ""
    public var category_name: String = ""
    public var sentOn: UInt64 = 0
}
