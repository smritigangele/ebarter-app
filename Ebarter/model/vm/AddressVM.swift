//
//  AddressVM.swift
//  Plenty of Freight
//
//  Created by Bhawani Singh on 04/11/16.
//  Copyright © 2016 Hocrox Infotech Pvt. Ltd. All rights reserved.
//

import Foundation

class AddressVM {
    var address: String = ""
    var address_name: String = ""
    var city: String = ""
    var state: String = ""
    var country: String = ""
    var postalcode: String = ""
    var mobile: String = ""
    var permanentAddress: String = ""
    var userId: Int = 0
    var latitude: Double = 0.0
    var longitude: Double = 0.0
    var placeId: String = ""
    var name: String = ""
    var username: String = ""
    var email: String = ""
    var sentOn: String = ""
    
    public func toJSON() -> Dictionary<String, String> {
        return [
            "address_name": self.name,
            "address_city": self.city,
            "address_state": self.state,
            "address_country": self.country,
            "address_postalcode": self.postalcode,
            "address_mobile": self.mobile,
            "address_permanent_address": self.permanentAddress,
            "userid": "\(self.userId)",
            "address_latitude": String(self.latitude),
            "address_longitude": String(self.longitude),
            "address_place_id": self.placeId
//            "name": self.name,
//            "username": self.username,
//            "email": self.email,
//            "sentOn": self.sentOn
        ]
    }
}
