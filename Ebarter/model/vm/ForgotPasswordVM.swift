//
//  ForgotPasswordVM.swift
//  vgo-ios
//
//  Created by Hocrox Infotech Pvt Ltd1 on 08/03/17.
//  Copyright © 2017 Hocrox Infotech Pvt. Ltd. All rights reserved.
//

import Foundation

class ForgotPasswordVM {
    
    //var forgotPasswordType = "NORMAL";
    var email: String = "";
    //var newPassword: String = "";
    
    public func toJSON() -> Dictionary<String, String> {
        return [
            "email": self.email,
            //"newPassword": self.newPassword
        ]
    }
}
