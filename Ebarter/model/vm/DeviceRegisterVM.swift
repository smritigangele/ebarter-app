//
//  DeviceRegisterVM.swift
//  Ebarterr
//
//  Created by Hocrox Infotech Pvt Ltd1 on 17/04/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import UIKit
class DeviceRegisterVM{
    
        var messaging_id: String = ""
        var device_name: String = ""
        var device_type: String = "IOS"
        var user_id: String = ""
    
    public func toJSON() -> Dictionary<String, String> {
        return [
    "messaging_id": self.messaging_id,
    "device_name": self.device_name,
    "device_type": self.device_type,
    "user_id": self.user_id
      ]
   }
}
