//
//  SignupVM.swift
//  Plenty of Freight
//
//  Created by Bhawani Singh on 04/11/16.
//  Copyright © 2016 Hocrox Infotech Pvt. Ltd. All rights reserved.
//

import Foundation

class SignupVM {
    
    var accountType =  "NORMAL";
    var email: String = "";
    var password: String = "";
    var username: String = "";
    var fullname: String = "";
    
    public func toJSON() -> Dictionary<String, String> {
        return [
            "name": self.fullname,
            "email": self.email,
            "password": self.password,
            "username": self.username,
        ]
    }
}

