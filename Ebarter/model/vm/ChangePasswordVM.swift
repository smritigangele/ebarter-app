//
//  ChangePasswordVM.swift
//  Ebarter
//
//  Created by Hocrox Infotech Pvt Ltd1 on 23/03/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
class ChangePasswordVM
{
    var old_password: String = "";
    var new_password: String = "";
    var confirm_password: String = "";
    
    public func toJSON() -> Dictionary<String, String> {
        return [
            "old_password": self.old_password,
            "new_password": self.new_password,
            "confirm_password": self.confirm_password
        
        ]
    }
    
}
