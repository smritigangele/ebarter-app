//
//  homeScreenVM.swift
//  Ebarter
//
//  Created by Hocrox Infotech Pvt Ltd1 on 22/03/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation

class HomeScreenVM{
    var page: UInt64 = 0;
    var size: UInt64 = 20;
    
    public func toJSON() -> Dictionary<String, String> {
        return [
            "page": "\(self.page)",
            "size": "\(self.size)"
        ]
    }
}
