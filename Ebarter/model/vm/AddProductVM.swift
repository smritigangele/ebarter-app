//
//  AddProductVM.swift
//  Ebarterr
//
//  Created by Hocrox Infotech Pvt Ltd1 on 23/04/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import UIKit
class AddProductVM {
    
    var author_id: Int = 0
    var title: String = ""
    var product_image: String = ""
    var category_id: Int = 0
    var product_points: String = ""
    var product_quantity: String = ""
    var body: String = ""
    var product_latitude: Double = 0
    var product_longitude: Double = 0
    var product_city: String = ""
    var product_state: String = ""
    var product_country: String = ""
    
    public func toJSON() -> Dictionary<String, String> {
        return [
            "author_id": "\(self.author_id)",
            "title": self.title,
            "product_image": self.product_image,
            "category_id": "\(self.category_id)",
            "product_points": self.product_points,
            "product_quantity": self.product_quantity,
            "body": self.body,
            "product_longitude": "\(self.product_longitude)",
            "product_latitude": "\(self.product_latitude)",
            "product_city": self.product_city,
            "product_state": self.product_state,
            "product_country": self.product_country
        ]
    }

    
}
