//
//  ChatMessageListVM.swift
//  Ebarterr
//
//  Created by Hocrox Infotech Pvt Ltd1 on 11/04/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation

class ChatMessageListVM {
    var chat_id: Int = 0
    
    public func toJSON() -> Dictionary<String, String> {
        return [
                "chat_id": "\(self.chat_id)"
        ]
    }
}
