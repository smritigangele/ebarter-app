//
//  File.swift
//  Ebarterr
//
//  Created by Hocrox Infotech Pvt Ltd1 on 10/05/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import UIKit

class ResetPasswordVM {
    
    var reset_token: String = "";
   var new_password: String = "";
    
    public func toJSON() -> Dictionary<String, String> {
        return [
            "reset_token": self.reset_token,
            "new_password": self.new_password
        ]
    }

}

