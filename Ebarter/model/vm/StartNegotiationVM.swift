//
//  StartNegotiationVM.swift
//  Ebarterr
//
//  Created by Hocrox Infotech Pvt Ltd1 on 17/04/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import UIKit

class StartNegotiationVM {
    var chat_id: Int = 0
    var sold_price: Int = 0
    public func toJSON() -> Dictionary<String, String> {
        return [
            "chat_id": "\(self.chat_id)",
            "sold_price": "\(self.sold_price)"
        ]
}
}
