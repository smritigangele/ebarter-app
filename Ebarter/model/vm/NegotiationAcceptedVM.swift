//
//  NegotiationAcceptedVM.swift
//  Ebarterr
//
//  Created by Hocrox Infotech Pvt Ltd1 on 17/04/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import UIKit
class NegotiationAccepted{
    var chatId: Int = 0
    var negotiationStatus: Int = 0
    public func toJSON() -> Dictionary<String, String> {
        return [
            "chatId": "\(self.chatId)",
            "negotiationStatus": "\(self.negotiationStatus)"
        ]

}
}
