//
//  LoginVM.swift
//  Plenty of Freight
//
//  Created by Bhawani Singh on 01/11/16.
//  Copyright © 2016 Hocrox Infotech Pvt. Ltd. All rights reserved.
//

import Foundation

class LoginVM {
    
    var loginType = "NORMAL";
    var password: String = "";
    var username: String = "";
    
    public func toJSON() -> Dictionary<String, String> {
        return [
            "email": self.username,
            "password": self.password
        ]
    }
}

