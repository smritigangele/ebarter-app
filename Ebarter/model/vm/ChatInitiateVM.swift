//
//  ChatInitiateVM.swift
//  Ebarter
//
//  Created by Hocrox Infotech Pvt Ltd1 on 29/03/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
class ChatInitiateVM {
    var product_id: Int = 0
    
    public func toJSON() -> Dictionary<String, String> {
        return [
            "product_id": "\(self.product_id)"
        ]
    }
}
