//
//  Buy_Via_NegotiationVM.swift
//  Ebarterr
//
//  Created by Hocrox Infotech Pvt Ltd1 on 21/04/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import UIKit
class Buy_Via_NegotiationVM{
    
    var chat_id: Int = 0
    var address_id: Int = 0
    
    public func toJSON() -> Dictionary<String, String> {
        return [
            
            "chat_id": "\(self.chat_id)",
            "address_id": "\(self.address_id)"

        ]
    }
}
