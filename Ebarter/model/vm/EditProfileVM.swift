//
//  EditProfileVM.swift
//  Ebarterr
//
//  Created by Hocrox Infotech Pvt Ltd1 on 05/04/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation

class EditProfileVM {
    
    var user_image: String = ""
    var user_description:String = ""
    var user_mobile: String = ""
    var userid: String = ""
    var user_house_number: String = ""
    var user_locality: String = ""
    var user_city: String = ""
    var user_state: String = ""
    var user_country: String = ""
    var user_pincode: String = ""
    var user_longitude: String = ""
    var user_latitude: String = ""
    
    public func toJSON() -> Dictionary<String, String> {
        return [
            "user_image": self.user_image,
            "user_description": self.user_description,
            "user_mobile": self.user_mobile,
            "userid": self.userid,
            "user_house_number": self.user_house_number,
            "user_locality": self.user_locality,
            "user_city": self.user_city,
            "user_state": self.user_state,
            "user_country": self.user_country,
            "user_pincode": self.user_pincode,
            "user_longitude": self.user_longitude,
            "user_latitude": self.user_latitude
        ]
    }
}
