//
//  PurchasePointsDTO.swift
//  Ebarterr
//
//  Created by Hocrox Infotech Pvt Ltd1 on 13/04/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation

class PurchasePointsVM{
    var stripeToken: String = ""
    var amount: String = ""
    
    public func toJSON() -> Dictionary<String, String> {
        return [
        "stripeToken": self.stripeToken,
        "amount": self.amount
        ]
}
}
