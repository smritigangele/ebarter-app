//
//  HomeScreenPaginateResult.swift
//  Ebarter
//
//  Created by Hocrox Infotech Pvt Ltd1 on 22/03/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import EVReflection

class HomeScreenPaginateResult: EVObject {
    public var result: HomeScreenPaginateDTO = HomeScreenPaginateDTO()
    public var response : Int = 0
    
}
