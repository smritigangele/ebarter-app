//
//  ChatPaginateResult.swift
//  Ebarter
//
//  Created by Hocrox Infotech Pvt Ltd1 on 28/03/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
class ChatPaginateResult{
    public var data: Array<ChatDTO> = Array<ChatDTO>()
    public var total: Int = 0
}
