//
//  UserService.swift
//  Ebarter
//
//  Created by Hocrox Infotech Pvt Ltd1 on 22/03/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import EVReflection

class UserService {
    
   static func getAllImages<T:EVObject>(view: UIViewController, paginate: HomeScreenVM, completion: @escaping (T) -> Void) {
    Service.get(url: Constants.homeScreenURL, view: view, authRequired: true, completion: completion)
    }
    
    static func getProductsList<T:EVObject>(view: UIViewController,id:String ,paginate: HomeScreenVM, completion: @escaping (T) -> Void) {
        Service.get(url: Constants.productDetailURL+id, view: view, authRequired: true, completion: completion)
    }
    static func getAllProducts<T:EVObject>(view: UIViewController, paginate: HomeScreenVM, completion: @escaping (T) -> Void) {
        Service.get(url: Constants.getAllProduct, view: view, authRequired: true, completion: completion)
   }
    
    static func getAllAddress<T:EVObject>(view: UIViewController, paginate: AddressVM, completion: @escaping (T) -> Void) {
        Service.get(url: Constants.addAddresslistURL, view: view, authRequired: true, completion: completion)
    }
    static func getAddressById<T:EVObject>(view: UIViewController,id: String, paginate: AddressVM , completion: @escaping (T) -> Void) {
        Service.get(url: Constants.address_IdURL+id, view: view, authRequired: true, completion: completion)
        
    }
    static func getTransactionsByBuyer<T:EVObject>(view: UIViewController, completion: @escaping (T) -> Void) {
        Service.get(url: Constants.transactionBuyer, view: view, authRequired: true, completion: completion)
    }
    static func getUserProfile<T:EVObject>(view: UIViewController, completion: @escaping (T) -> Void) {
        Service.get(url: Constants.userProfile, view: view, authRequired: true, completion: completion)
    }
    static func getChatListOfBuyerSeller<T:EVObject>(view: UIViewController,type: String, paginate: HomeScreenVM, completion: @escaping (T) -> Void) {
        Service.get(url: Constants.chatListAsBuyerSeller+type, view: view, authRequired: true, completion: completion)
    }
    static func getPointsTransaction<T:EVObject>(view: UIViewController, paginate: HomeScreenVM, completion: @escaping (T) -> Void) {
        Service.get(url: Constants.pointsTransaction, view: view, authRequired: true, completion: completion)
    }
   
    static func getTransactionsBySeller<T:EVObject>(view: UIViewController, completion: @escaping (T) -> Void) {
        Service.get(url: Constants.transactionSeller, view: view, authRequired: true, completion: completion)
    }

    static func getSingleTransaction<T:EVObject>(view: UIViewController,id:String, paginate: HomeScreenVM, completion: @escaping (T) -> Void) {
        Service.get(url: Constants.singleTransaction+id, view: view, authRequired: true, completion: completion)
    }
    static func getChatData<T:EVObject>(view: UIViewController,chatId:String, paginate: HomeScreenVM, completion: @escaping (T) -> Void) {
        Service.get(url: Constants.chatDataByChatId+chatId, view: view, authRequired: true, completion: completion)
    }

    static func getChatMessageIdByNotification<T:EVObject>(view: UIViewController,messageId:String, paginate: HomeScreenVM, completion: @escaping (T) -> Void) {
        Service.get(url: Constants.chatMessageIdByNotification+messageId, view: view, authRequired: true, completion: completion)
    }

    static func getCurrentUser<T:EVObject>(view: UIViewController, paginate: HomeScreenVM, completion: @escaping (T) -> Void) {
        Service.get(url: Constants.userProfile, view: view, authRequired: true, completion: completion)
    }
   
    static func deleteProduct<T:EVObject>(view: UIViewController,id: String,  completion: @escaping (T) -> Void) {
        Service.get(url: Constants.deleteProduct+id, view: view, authRequired: true, completion: completion)
    }
   
    static func editProduct<T:EVObject>(view: UIViewController,id: String,  completion: @escaping (T) -> Void) {
        Service.get(url: Constants.editProduct+id, view: view, authRequired: true, completion: completion)
    }
    
    static func searchByDistance<T:EVObject>(view: UIViewController, parameter: String,completion: @escaping (T) -> Void) {
        Service.get(url: Constants.searchByDistance+parameter, view: view, authRequired: true, completion: completion)
    }
    
    static func searchByCityState<T:EVObject>(view: UIViewController, parameter: String, completion: @escaping (T) -> Void) {
        Service.get(url: Constants.searchByCity+parameter, view: view, authRequired: true, completion: completion)
    }
    
    static func searchByLatLong<T:EVObject>(view: UIViewController, parameter: String, state: String, newParameter: String, distance: String,searchDist: String, completion: @escaping (T) -> Void) {
        Service.get(url: Constants.searchByLatLong+parameter+state+newParameter+distance+searchDist, view: view, authRequired: true, completion: completion)
    }

    static func searchByName<T:EVObject>(view: UIViewController, name:String, completion: @escaping (T) -> Void) {
        Service.get(url: Constants.searchByName+name, view: view, authRequired: true, completion: completion)
    }
    
    
}
