//
//  Service.swift
//  Hunger Hub
//
//  Created by Bhawani Singh on 01/11/16.
//  Copyright © 2016 Hocrox Infotech Pvt. Ltd. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

import EVReflection

class Service {
    static var ebarterToken: String = "ebarterToken"
      static let name = "name"
      static let user_image = "user_image"
      static var isFromChat = false
    
    static var userToken: String? = nil;
    
    static var authHeader: HTTPHeaders = HTTPHeaders();

    private static let sessionManager = Alamofire.SessionManager.default

    static func loadTokenOnAppLoad() {

        if let token = UserDefaults.standard.string(forKey: ebarterToken) as String! {
          userToken = token
            //print("User Token 2 : \(userToken)")
            authHeader = [
                    "Authorization": "Bearer " + userToken!
            ];
        }
       print("User Token : \(userToken)")
    
        
   }
    
    static func get<T:EVObject>(url: String, parameters: Dictionary<String, Any> = [String: String](), view: UIViewController, authRequired: Bool, completion: @escaping (T) -> Void) {
        baseNetworkOperation(method: .get, url: url, parameters: parameters, view: view, authRequired: authRequired, encoding: URLEncoding.default, completion: completion)
    }

    static func post<T:EVObject>(url: String, parameters: Dictionary<String, Any>, view: UIViewController, authRequired: Bool, completion: @escaping (T) -> Void) {
        baseNetworkOperation(method: .post, url: url, parameters: parameters, view: view, authRequired: authRequired, completion: completion)

    }

    static func put<T:EVObject>(url: String, parameters: Dictionary<String, Any>, view: UIViewController, authRequired: Bool, completion: @escaping (T) -> Void) {
        baseNetworkOperation(method: .put, url: url, parameters: parameters, view: view, authRequired: authRequired, completion: completion)

    }

    static func delete<T:EVObject>(url: String, parameters: Dictionary<String, Any> = [String: String](), view: UIViewController, authRequired: Bool, completion: @escaping (T) -> Void) {
        baseNetworkOperation(method: .delete, url: url, parameters: parameters, view: view, authRequired: authRequired, completion: completion)

    }

    static private func baseNetworkOperation<T:EVObject>(method: HTTPMethod, url: String, parameters: Dictionary<String, Any>?, view: UIViewController, authRequired: Bool,encoding: ParameterEncoding = JSONEncoding.default, completion: @escaping (T) -> Void) {
        Loader.startLoading();
        
      print("URL" + url)
        
     //   print(parameters!)
        
        var request: DataRequest;
        if (authRequired) {
            print(authHeader)
            request = Alamofire.request(url, method: method, parameters: parameters, encoding: encoding, headers: authHeader);
        } else {
            request = Alamofire.request(url, method: method, parameters: parameters, encoding: encoding);
        }
//        request.responseJSON { response in
//            print(response.result.value)
//            
//        }
//        print(authHeader)
//        print(parameters!)
//        print(method)
//        print(url)
        
        request.validate()
                .responseObject { (response: DataResponse<T>) in
                    //                if #available(iOS 10.0, *) {
                    //                    debugPrint(response.metrics ?? "0")
                    //                }
                    
                    
                    if let result = response.result.value {
                        if (result is TokenDTO) {
                            saveToken(view: view, token: result as! TokenDTO)
                            print("Result...")
                        }
                        print("Lol Result")
                        print(result)
//                        if (result is AddNewProductDTO) {
//                            if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
//                                let json = try? JSONSerialization.jsonObject(with: data)
//                                print(json!)
//                                
//                            }
//                        }
                                                
                        
                        completion(result)
                        
                    } else {
                        handleError(view: view, response: response, string: url)
                    }
                    Loader.stopLoading()
                }

    }
    
    
    static  func uploadFile<T:EVObject>(method: HTTPMethod, url: String, parameters: Dictionary<String, Any>?, view: UIViewController, authRequired: Bool,encoding: ParameterEncoding = JSONEncoding.default, completion: @escaping (T) -> Void) {
        Loader.startLoading();
        
        print("URL" + url)
        
        //   print(parameters!)
        
        var request: DataRequest;
        if (authRequired) {
            print(authHeader)
            let imageData = UIImagePNGRepresentation(UIImage())!
            
            Alamofire.upload(imageData, to: "https://httpbin.org/post")
                .responseJSON { response in
                debugPrint(response)
            }
            request = Alamofire.request(url, method: method, parameters: parameters, encoding: encoding, headers: authHeader);
        } else {
            request = Alamofire.request(url, method: method, parameters: parameters, encoding: encoding);
        }
//                request.responseJSON { response in
//                    print(response.result.value ?? "sdfghjk")
//
//                }
        //        print(authHeader)
        //        print(parameters!)
        //        print(method)
        //        print(url)
        
        request.validate()
            .responseObject { (response: DataResponse<T>) in
                //                if #available(iOS 10.0, *) {
                //                    debugPrint(response.metrics ?? "0")
                //                }
                if let result = response.result.value {
                    if (result is TokenDTO) {
                        saveToken(view: view, token: result as! TokenDTO)
                        print("Result...")
                    }
                    print("Lol Result")
                    print(result)
                
                    completion(result)
                    
                } else {
                    handleError(view: view, response: response , string: url)
                }
                Loader.stopLoading()
        }
        
    }


    static private func handleError<T>(view: UIViewController, response: DataResponse<T>, string: String) {

        var statusCode = response.response?.statusCode
        if let error = response.result.error as? AFError {
            statusCode = error._code // statusCode private
            switch error {
            case .invalidURL(let url):
                print("Invalid URL: \(url) - \(error.localizedDescription)")
                view.createAlert(title: "Error", body: "Invalid URL")
            case .parameterEncodingFailed(let reason):
                print("Parameter encoding failed: \(error.localizedDescription)")
                print("Failure Reason: \(reason)")
                view.createAlert(title: "Error", body: "Parameter Encoding Failed")
            case .multipartEncodingFailed(let reason):
                print("Multipart encoding failed: \(error.localizedDescription)")
                print("Failure Reason: \(reason)")
                view.createAlert(title: "Error", body: "Multipart encoding failed")
            case .responseValidationFailed(let reason):
//                if let data = response.data {
//                    let responseJSON = JSON(data: data)
//                    
//                    if let message: String = responseJSON["message"].stringValue {
//                        if !message.isEmpty {
//                            errorMessage = message
//                        }
//                    }
//                }
    
//                response.responseJSON { response in
//                    print("this is response\(response.result.value)")}
                print("Response validation failed: \(error.localizedDescription)")
                print("Failure Reason: \(reason)")
               //view.createAlert(title: "Error", body: "Response validation failed")

                switch reason {
                case .dataFileNil, .dataFileReadFailed:
                    print("Downloaded file could not be read")
                    view.createAlert(title: "Error", body: "Data file could not be read")
                case .missingContentType(let acceptableContentTypes):
                    print("Content Type Missing: \(acceptableContentTypes)")
                    view.createAlert(title: "Error", body: "Content Type Missing")
                case .unacceptableContentType(let acceptableContentTypes, let responseContentType):
                    print("Response content type: \(responseContentType) was unacceptable: \(acceptableContentTypes)")
                    view.createAlert(title: "Error", body: "Response content type")
                case .unacceptableStatusCode(let code):
                    print("Response status code was unacceptable: \(code)")
                    statusCode = code
                    //view.createAlert(title: "Error", body: "\(statusCode)")
                }
                if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                    let json = try? JSONSerialization.jsonObject(with: data)
                    print(json!)
                    
                    if let jsonResponse = json as? NSDictionary {
                      
//                    if let jsonResponse1 = json as? NSDictionary{
                        if let errorFields = jsonResponse["fieldErrors"] as? NSArray {
                            if errorFields.count == 0{
                                print("error fields are zero")
                                
                                if string == Constants.chatInitiate{
                                    
                               // view.createAlert(title: "",body: "")
                                    
                                }
                       else{
                    view.createAlert(title: "",body: jsonResponse["description"] as! String)
                                }
                            }
                            else{
                                let message = ((errorFields[0] as! NSDictionary)["message"] as! NSArray)[0] as! String
                                view.createAlert(title: "", body: "\(message)" )
                            }
                            
                        }
                        
                    }
                   print("Utf : " + utf8Text)
//                    //json.errorLabel.text = "something went wrong"
//                    //view.createAlert(title: "Error", body: utf8Text)
//                    view.createAlert(title: "Error", body: "something went wrong")
               // }
            
//                if let strongSelf = self {
//                    let data = data as? NSData
//
//                if data == nil {
//                    print("Why didn't I get any data back?")
                
//                    return
//                } else if let error = error {
//                    let resultText = NSString(data: data!, encoding: NSUTF8StringEncoding)
//                    println(resultText)
//                    strongSelf.errorLabel.text = "something went wrong"
//                    return
//                }

                }
            case .responseSerializationFailed(let reason):
                print("Response serialization failed: \(error.localizedDescription)")
                print("Failure Reason: \(reason)")
                view.createAlert(title: "Failed!", body: "Serialization failed")
                    // statusCode = 3840 ???? maybe..
            }

            print("Underlying error: \(error.underlyingError)")
        } else if let error = response.result.error as? URLError {
            print("URLError occurred: \(error)")
            view.createAlert(title: "Error", body: "URLError Occured")
        } else {
            print("Unknown error: \(response.result.error)")
            view.createAlert(title: "Error", body: "Unknown error")
        }
        print(statusCode ?? 200) // the status code
    }

    static private func saveToken(view: UIViewController, token: TokenDTO) {
        let preferences = UserDefaults.standard

        preferences.setValue(token.token, forKey: Service.ebarterToken)
        let didSave = preferences.synchronize()

        if !didSave {
            view.createAlert(title: "Error", body: "Unable to save token")
        }
        loadTokenOnAppLoad()
    }
    
    static func logout(view: UIViewController) {
        let preferences = UserDefaults.standard
        
        preferences.setValue(nil, forKey: Service.ebarterToken)
        preferences.synchronize()
        userToken = nil
        authHeader = HTTPHeaders()
    }

}


