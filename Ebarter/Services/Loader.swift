//
//  Loader.swift
//  Plenty of Freight
//
//  Created by Bhawani Singh on 01/11/16.
//  Copyright © 2016 Hocrox Infotech Pvt. Ltd. All rights reserved.
//

import Foundation
import NVActivityIndicatorView


class Loader {

    static func startLoading(message: String? = nil) {
        NVActivityIndicatorView.DEFAULT_TYPE = .ballSpinFadeLoader
        NVActivityIndicatorView.DEFAULT_COLOR = UIColor.white
        let activityData = ActivityData(message: message)
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
    }

    static func stopLoading() {
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
    }
}
