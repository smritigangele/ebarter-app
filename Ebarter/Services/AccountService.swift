//
// Created by Bhawani Singh on 11/01/17.
// Copyright (c) 2017 Hocrox Infotech Pvt. Ltd. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import EVReflection

class AccountService {

//    static func isLoggedIn(view: UIViewController, completion: @escaping (TokenDTO) -> Void) {
//        Service.get(url: Constants.loginURL, view: view, authRequired: true, completion: completion)
//    }

    static func login(view: UIViewController, login: LoginVM, completion: @escaping (TokenDTO) -> Void) {
        Service.post(url: Constants.loginURL, parameters: login.toJSON(), view: view, authRequired: false, completion: completion)
    }

    static func signup(view: UIViewController, signup: SignupVM, completion: @escaping (TokenDTO) -> Void) {
        Service.post(url: Constants.registerURL, parameters: signup.toJSON(), view: view, authRequired: false, completion: completion)
    }
    static func changePassword<T:EVObject>(view: UIViewController, changePassword: ChangePasswordVM, completion: @escaping (T) -> Void) {
        Service.post(url: Constants.changePasswordURL, parameters: changePassword.toJSON(), view: view, authRequired: true, completion: completion)
    }
    static func editProfile<T:EVObject>(view: UIViewController, editProfile: EditProfileVM, completion: @escaping (T) -> Void) {
        Service.post(url: Constants.updateProfileURL, parameters: editProfile.toJSON(), view: view, authRequired: true, completion: completion)
    }
    static func chatInitiate<T: EVObject>(view: UIViewController, chatInitiate: ChatInitiateVM, completion: @escaping (T) -> Void) {
        Service.post(url: Constants.chatInitiate, parameters: chatInitiate.toJSON(), view: view, authRequired: true, completion: completion)
    }
    static func chatMessageList<T: EVObject>(view: UIViewController, chatMessage: ChatMessageListVM, completion: @escaping (T) -> Void) {
        Service.post(url: Constants.chatMessageList, parameters: chatMessage.toJSON(), view: view, authRequired: true, completion: completion)
    }
    static func chatNewMessage<T:EVObject>(view: UIViewController, chatNew: ChatNewMessageVM, completion: @escaping (T) -> Void) {
        Service.post(url: Constants.chatNew, parameters: chatNew.toJSON(), view: view, authRequired: true, completion: completion)
    }
    static func addPoints<T:EVObject>(view: UIViewController, points: BuyPointsVM, completion: @escaping (T) -> Void) {
        Service.post(url: Constants.addPoints, parameters: points.toJSON(), view: view, authRequired: true, completion: completion)
    }
    static func purchasePoints<T:EVObject>(view: UIViewController, points: PurchasePointsVM, completion: @escaping (T) -> Void) {
        Service.post(url: Constants.purchasePoints, parameters: points.toJSON(), view: view, authRequired: true, completion: completion)
    }


    static func addAddress<T:EVObject>(view: UIViewController, address: AddressVM, completion: @escaping (T) -> Void) {
        Service.post(url: Constants.addAddressURL, parameters: address.toJSON(), view: view, authRequired: true, completion: completion)
    }
    static func buy_Via_Negotiation<T:EVObject>(view: UIViewController, address: Buy_Via_NegotiationVM, completion: @escaping (T) -> Void) {
        Service.post(url: Constants.buy_Via_Notification, parameters: address.toJSON(), view: view, authRequired: true, completion: completion)
    }
    static func buy_Without_Negotiation<T:EVObject>(view: UIViewController, address: Buy_Without_NegotiationVM, completion: @escaping (T) -> Void) {
        Service.post(url: Constants.buy_Without_Negotiation, parameters: address.toJSON(), view: view, authRequired: true, completion: completion)
    }

    static func deviceRegister<T:EVObject>(view: UIViewController, address: DeviceRegisterVM, completion: @escaping (T) -> Void) {
        Service.post(url: Constants.device_Register, parameters: address.toJSON(), view: view, authRequired: true, completion: completion)
    }
    static func start_Negotiation<T: EVObject>(view: UIViewController, address: StartNegotiationVM, completion: @escaping (T) -> Void) {
            Service.post(url: Constants.startNegotiation, parameters: address.toJSON(), view: view, authRequired: true, completion: completion)
        
    }
    
    static func accept_Negotiation<T: EVObject>(view: UIViewController, address: AcceptNegotiationVM, completion: @escaping (T) -> Void) {
        Service.post(url: Constants.AcceptNegotiation, parameters: address.toJSON(), view: view, authRequired: true, completion: completion)
    }
    static func addNewProduct<T: EVObject>(view: UIViewController, address: AddProductVM, completion: @escaping (T) -> Void) {
        Service.post(url: Constants.addNewProduct, parameters: address.toJSON(), view: view, authRequired: true, completion: completion)
    }
    static func getChatByPagination<T:EVObject>(view: UIViewController,pageNo: String, address: ChatMessageListVM, completion: @escaping (T) -> Void) {
        Service.post(url: Constants.chatMessageUsingPagination+pageNo, parameters: address.toJSON(), view: view, authRequired: true, completion: completion)
    }
    static func forgotPassword<T:EVObject>(view: UIViewController, address: ForgotPasswordVM, completion: @escaping (T) -> Void) {
        Service.post(url: Constants.forgotPassword, parameters: address.toJSON(), view: view, authRequired: false, completion: completion)
    }
    static func resetPassword<T:EVObject>(view: UIViewController, address: ResetPasswordVM, completion: @escaping (T) -> Void) {
        Service.post(url: Constants.resetPassword, parameters: address.toJSON(), view: view, authRequired: false, completion: completion)
    }

}
