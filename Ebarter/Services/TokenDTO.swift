//
//  TokenDTO.swift
//  Plenty of Freight
//
//  Created by Bhawani Singh on 01/11/16.
//  Copyright © 2016 Hocrox Infotech Pvt. Ltd. All rights reserved.
//

import Foundation
import EVReflection

class TokenDTO: EVObject {
    var token: String = "";
    var timestamp: String = "";
}
