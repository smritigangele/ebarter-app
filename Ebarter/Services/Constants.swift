//
//  Constants.swift
//
//
//  Created by Bhawani Singh on 01/11/16.
//  Copyright © 2016 Hocrox Infotech Pvt. Ltd. All rights reserved.
//

import Foundation

class Constants {
     
    static let googleMapsApiKey: String = "AIzaSyAnIPCIrT3X4CUB4gccZOJtB0_lNyeWrNQ"
    
    static let baseURL: String = "http://api.ebarternow.com/api/v1/";
    //"http://192.168.0.12:8080/api/v1/" //"http://192.168.0.22:8080/api/v1/" //
    static let loginURL: String = Constants.baseURL + "auth/login";
    static let registerURL: String = Constants.baseURL + "auth/register";
    static let changePasswordURL: String = Constants.baseURL + "password/change";
    static let userProfile: String = Constants.baseURL + "profile/user"
    static let updateProfileURL: String = Constants.baseURL + "user/profile"
    static let homeScreenURL: String = Constants.baseURL + "home/";
    static let productDetailURL: String = Constants.baseURL + "home/category/";
    //static let listProductURL: String = Constants.baseURL + "products/all"
    static let uploadImage: String = Constants.baseURL + "image/upload";
    static let addAddressURL: String = Constants.baseURL + "address/new";
    static let addAddresslistURL: String = Constants.baseURL + "address/all"
    static let address_IdURL: String = Constants.baseURL + "address/edit/"
    static let chatInitiate: String = Constants.baseURL + "chat/initiate";
    static let chatNew: String = Constants.baseURL + "chat/new";
    static let chatMessageList: String = Constants.baseURL + "chat/message";
    static let chatListAsBuyerSeller: String = Constants.baseURL + "chat/list?type="
    static let addPoints: String = Constants.baseURL + "pointstousd"
    static let purchasePoints: String = Constants.baseURL + "payment"
    static let transactionBuyer: String = Constants.baseURL + "transactions/buyer"
    static let transactionSeller: String = Constants.baseURL + "transactions/seller"
    static let pointsTransaction: String = Constants.baseURL + "pointstransaction/all"
    static let singleTransaction: String = Constants.baseURL + "pointstransaction/single?points_transaction_id="
    static let startNegotiation: String = Constants.baseURL + "negotiation/start"
    static let AcceptNegotiation: String = Constants.baseURL + "negotiation/accept"
    static let buy_Via_Notification: String = Constants.baseURL + "buy-via-negotiation"
    static let buy_Without_Negotiation: String = Constants.baseURL + "buy-without-negotiation"
    static let device_Register: String = Constants.baseURL + "device/register"
    static let chatDataByChatId: String = Constants.baseURL + "getchat/data?chat_id="
    static let chatMessageIdByNotification: String = Constants.baseURL + "chat/message/single?message_id="
    static let addNewProduct: String = Constants.baseURL + "product/new"
    static let deleteProduct: String = Constants.baseURL + "product/delete/"
     static let editProduct: String = Constants.baseURL + "product/edit?product_id="
    static let productByCity:String = Constants.baseURL + "search?city="
    static let productByDistance:String = Constants.baseURL + "search/distance?value="
    static let productByLatLong:String = Constants.baseURL + "search/latlong?latitude=30.744654&longitude=76.761065&distance=3"
    static let chatMessageUsingPagination: String = Constants.baseURL + "chat/message?page="
    static let searchByDistance: String = Constants.baseURL + "search/distance?value="
    static let searchByCity: String = Constants.baseURL + "search?city="
    static let searchByState: String = "&state="
    static let searchByLatLong: String = Constants.baseURL + "search/latlong?latitude="
    static let getAllProduct:String = Constants.baseURL + "products/all"
    static let forgotPassword:String = Constants.baseURL + "password/forgot"
    static let resetPassword:String = Constants.baseURL + "password/reset"
    static let searchByName: String = Constants.baseURL + "search/product?product_name=";
    
  }
