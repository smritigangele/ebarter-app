//
//  EditProfileViewController.swift
//  Ebarter
//
//  Created by Hocrox Infotech Pvt Ltd1 on 23/03/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import UIKit
import Material
import GooglePlaces

class EditProfileViewController: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    var address: AddressVM = AddressVM()
    let imagePicker = UIImagePickerController()
    
       
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var uploadImageBtn: UIButton!
    @IBOutlet weak var colonyTextField: UITextField!
    @IBOutlet weak var pinTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var stateTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        imagePicker.delegate = self
        uploadImageBtn.layer.cornerRadius = 95/2
        uploadImageBtn.clipsToBounds = true
        uploadImageBtn.setBackgroundImage(#imageLiteral(resourceName: "placeholderr"), for: .normal)
    }
    
    @IBAction func openLibraryBtn(_ sender: Any) {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
            if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
                 uploadImageBtn.setBackgroundImage(pickedImage, for: .normal)
               
            }
            
            dismiss(animated: true, completion: nil)
    }
    
    @IBAction func LocalityTextField(_ sender: Any) {
        let autoCompleteController = GMSAutocompleteViewController()
        autoCompleteController.delegate = self
        self.present(autoCompleteController, animated: true, completion: nil)
    }
    
    @IBAction func editProfileBtn(_ sender: Any) {
        
//        let vc = storyboard?.instantiateViewController(withIdentifier: "HomePageViewController")as! HomePageViewController
       dismiss(animated: true, completion: nil)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
extension EditProfileViewController: GMSAutocompleteViewControllerDelegate {
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        self.address = AddressVM()
        self.address.address = place.formattedAddress!
        self.address.latitude = place.coordinate.latitude
        self.address.longitude = place.coordinate.longitude
        self.address.placeId = place.placeID
        self.colonyTextField.text = place.formattedAddress!
        
        for address: GMSAddressComponent in (place.addressComponents)! {
            if(address.type == "administrative_area_level_2") {
                self.address.city = address.name
                self.cityTextField.text = address.name
            }
            if(address.type == "administrative_area_level_1") {
                self.address.state = address.name
                self.stateTextField.text = address.name
            }
            if(address.type == "postal_code") {
                self.pinTextField.text = address.name
               // self.address.zipcode = address.name
            }
        print(address.type + " - " + address.name)
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
////extension EditProfileViewController:

